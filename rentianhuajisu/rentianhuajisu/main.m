//
//  main.m
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/19.
//

#import <UIKit/UIKit.h>
#import "TIANAppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([TIANAppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
