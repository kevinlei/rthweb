//
//  MEBaseTabBarController.h
//  mechanic
//
//  Created by takung on 2021/9/7.
//

#import "SUITabbarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SUIBaseTabBarController : SUITabbarController

@end

NS_ASSUME_NONNULL_END
