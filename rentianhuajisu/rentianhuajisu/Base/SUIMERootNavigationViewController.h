//
//  MERootNavigationViewController.h
//  mechanic
//
//  Created by 宗丽康 on 2021/9/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SUIMERootNavigationViewController : UINavigationController

- (BOOL)popToAppointViewController:(NSString *)className animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
