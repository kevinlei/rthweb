//
//  METabbarController.m
//  mechanic
//
//  Created by 宗丽康 on 2021/9/3.
//

#import "SUITabbarController.h"

@interface SUITabbarController ()<UITabBarControllerDelegate>

@end

@implementation SUITabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _meTabbar = [[METabBar alloc] init];
    [_meTabbar.centerBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    //利用KVC 将自己的tabbar赋给系统tabBar
    [self setValue:_meTabbar forKeyPath:@"tabBar"];
    self.delegate = self;
}

//// 重写选中事件， 处理中间按钮选中问题
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    _meTabbar.centerBtn.selected = (tabBarController.selectedIndex == self.viewControllers.count/2);
//
    if (self.meDelegate){
        [self.meDelegate meTabBarController:tabBarController didSelectViewController:viewController];
    }
}

- (void)buttonAction:(UIButton *)button {
    NSInteger count = self.viewControllers.count;
    self.selectedIndex = count/2;//关联中间按钮
    [self tabBarController:self didSelectViewController:self.viewControllers[self.selectedIndex]];
}

@end

