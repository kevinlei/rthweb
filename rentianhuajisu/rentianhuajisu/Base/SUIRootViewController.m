//
//  MERootViewController.m
//  mechanic
//
//  Created by 宗丽康 on 2021/9/3.
//

#import "SUIRootViewController.h"

@interface SUIRootViewController ()

@end

@implementation SUIRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent animated:NO];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top"] forBarMetrics:UIBarMetricsDefault];
//       [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
   
  
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = rgb(243, 246, 254);
    // 是否显示返回按钮
    self.isShowLeftBackButton = YES;
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UIImage *bgImage = [UIImage imageWithColor:[UIColor colorWithHexString:APP_tab_Color]];
    if (@available(iOS 13.0, *)) {
        UINavigationBarAppearance *appearance = [UINavigationBarAppearance new];
        [appearance configureWithOpaqueBackground];
        appearance.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
        //设置背景图
        appearance.backgroundImage = bgImage;
        self.navigationItem.standardAppearance = appearance;
        self.navigationItem.scrollEdgeAppearance=self.navigationItem.standardAppearance;
    }
    [self.navigationController.navigationBar setBackgroundImage:bgImage forBarMetrics:UIBarMetricsDefault];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return _statusBarStyle;
}

// 动态更改状态栏颜色
- (void)setStatusBarStyle:(UIStatusBarStyle)statusBarStyle {
    _statusBarStyle = statusBarStyle;
//    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - 是否显示返回按钮

- (void)setIsShowLeftBackButton:(BOOL)isShowLeftBackButton {
    _isShowLeftBackButton = isShowLeftBackButton;
    NSInteger VCCount = self.navigationController.viewControllers.count;
    // 当VC所在的导航控制器中的VC个数大于1 或者  是present出来的VC时，才展示返回按钮，其他情况不展示
    if (isShowLeftBackButton && ( VCCount > 1 || self.navigationController.presentingViewController != nil)) {
        [self addNavgationItemWithImageNames:@[@"navBack"] isLeft:YES target:self action:@selector(backBtnClicked) tags:[NSArray array]];
    } else {
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem *nullBar = [[UIBarButtonItem alloc] initWithCustomView:[UIView new]];
        self.navigationItem.leftBarButtonItem = nullBar;
    }
}

- (void)backBtnClicked {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - 导航栏 添加图片按钮

- (void)addNavgationItemWithImageNames:(NSArray *)imageNames isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags {
    NSMutableArray *items = [[NSMutableArray alloc] init];
    NSInteger i = 0;
    for (NSString *imageName in imageNames) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        btn.frame = CGRectMake(0, 0, 30, 30);
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        
        if (isLeft) {
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
        } else {
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
        }
        if (tags.count) {
            btn.tag = [tags[i++] integerValue];
        }
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [items addObject:item];
    }
    if (isLeft) {
        self.navigationItem.leftBarButtonItems = items;
    } else {
        self.navigationItem.rightBarButtonItems = items;
    }
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],NSForegroundColorAttributeName:[UIColor blueColor]}];

}

#pragma mark - 导航栏添加文字按钮

- (void)addNavgationItemWithTitles:(NSArray *)titles isLeft:(BOOL)isLeft target:(id)target action:(SEL)action tags:(NSArray *)tags {
    NSMutableArray *items = [[NSMutableArray alloc] init];
    //调整按钮位置
    UIBarButtonItem* spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem.width = 15;
    [items addObject:spaceItem];
    NSInteger i = 0;
    
    for (NSString *title in titles) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 30, 30);
        [btn setTitle:title forState:UIControlStateNormal];
        [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        if (tags && tags.count > 0) {
            btn.tag = [tags[i++] integerValue];
        }
        
        if (isLeft) {
            
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
        } else {
            
            [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
        }
        
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [items addObject:item];
    }
    
    if (isLeft) {
        
        self.navigationItem.leftBarButtonItems = items;
    } else {
        
        self.navigationItem.rightBarButtonItems = items;
    }
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}

- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
