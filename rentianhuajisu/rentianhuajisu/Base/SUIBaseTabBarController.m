//
//  MEBaseTabBarController.m
//  mechanic
//
//  Created by takung on 2021/9/7.
//

#import "SUIBaseTabBarController.h"
#import "SUIMERootNavigationViewController.h"

#import "TIANRJSHomeViewController.h"
#import "TIANRJSMineViewController.h"
#import "TIANRJSPulishViewController.h"
@interface SUIBaseTabBarController ()<ZHUMETabBarControllerDelegate>

@end

@implementation SUIBaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //透明设置为NO，显示白色，view的高度到tabbar顶部截止，YES的话到底部
    
//    self.meTabbar.translucent = NO;
//    self.meTabbar.tintColor = [UIColor colorWithRed:251.0/255.0 green:199.0/255.0 blue:115/255.0 alpha:1];
//      //透明设置为NO，显示白色，view的高度到tabbar顶部截止，YES的话到底部
//      self.meTabbar.translucent = NO;
//      
//      self.meTabbar.position = METabBarCenterButtonPositionBulge;
//      self.meTabbar.centerImage = [UIImage imageNamed:@"消息"];
////      self.mcDelegate = self;

    

//    self.meTabbar.position = METabBarCenterButtonPositionBulge;
//    self.meTabbar.centerImage = [UIImage imageNamed:@"tab_public"];
    self.meDelegate = self;
    [self addChildViewControllers];
}

//添加子控制器
- (void)addChildViewControllers {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main" bundle: nil];
//    ViewController *vc  = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];

    [self addChildrenViewController:[[TIANRJSHomeViewController alloc] init] andTitle:@"转换" andImageName:@"转换 (1)" selectImage:@"转换"];
    
    [self addChildrenViewController:[[TIANRJSPulishViewController alloc] init] andTitle:@"计算器" andImageName:@"计算器" selectImage:@"计算器 (1)"];
//    
//    [self addChildrenViewController:[[RTHAlterViewController alloc] init] andTitle:@"提醒" andImageName:@"底部图标-03" selectImage:@"底部图标-07"];
    [self addChildrenViewController:[[TIANRJSMineViewController alloc] init] andTitle:@"我的" andImageName:@"我的" selectImage:@"我的 (1)"];

}

- (void)addChildrenViewController:(UIViewController *)childVC andTitle:(NSString *)title andImageName:(NSString *)imageName selectImage:(NSString *)selectImageName {
    childVC.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)]  ;
    // 选中的颜色由tabbar的tintColor决定
    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectImageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] ;
    
    
//    childVC.tabBarItem.image = [[UIImage imageNamed:selectImageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)]  ;
//    // 选中的颜色由tabbar的tintColor决定
//    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:imageName] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] ;
    childVC.title = title;
    
//    // S未选中字体颜色
//    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#EEEEEE"],NSFontAttributeName:[UIFont systemFontOfSize:12 weight:UIFontWeightRegular]} forState:UIControlStateNormal];
//
//    // 选中字体颜色
//    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#00D878"],NSFontAttributeName:[UIFont systemFontOfSize:12 weight:UIFontWeightRegular]} forState:UIControlStateSelected];
//
    if (@available(iOS 12.0, *)) {
        [[UITabBar appearance] setTintColor:[UIColor colorWithHexString:APP_tab_Color]];
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor darkGrayColor]];

    } else {
        // Fallback on earlier versions

    }


 
    
    
    SUIMERootNavigationViewController *baseNav = [[SUIMERootNavigationViewController alloc] initWithRootViewController:childVC];
    [self addChildViewController:baseNav];
}

// 使用MCTabBarController 自定义的 选中代理
- (void)meTabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (tabBarController.selectedIndex == 1){
//        self.tabBar.hidden = YES;
//        [[TIANAppDelegate shareAppDelegate] showChat];
//        [[AppDelegate  shareAppDelegate] showChat];
        
    }else {
        self.tabBar.hidden = NO;
//        [self.meTabbar.centerBtn.layer removeAllAnimations];
    }
}

@end
