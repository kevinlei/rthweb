//
//  METabBar.h
//  mechanic
//
//  Created by 宗丽康 on 2021/9/6.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, METabBarCenterButtonPosition){
    METabBarCenterButtonPositionCenter, // 居中
    METabBarCenterButtonPositionBulge // 凸出一半
};

@interface METabBar : UITabBar
/**
 中间按钮
 */
@property (nonatomic, strong) UIButton *centerBtn;

/**
  中间按钮图片
 */
@property (nonatomic, strong) UIImage *centerImage;
/**
 中间按钮选中图片
 */
@property (nonatomic, strong) UIImage *centerSelectedImage;

/**
 中间按钮偏移量,两种可选，也可以使用centerOffsetY 自定义
 */
@property (nonatomic, assign) METabBarCenterButtonPosition position;

/**
  中间按钮偏移量，默认是居中
 */
@property (nonatomic, assign) CGFloat centerOffsetY;

/**
  中间按钮的宽和高，默认使用图片宽高
*/
@property (nonatomic, assign) CGFloat centerWidth, centerHeight;

@end

