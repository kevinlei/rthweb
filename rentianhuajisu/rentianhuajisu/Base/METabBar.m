//
//  METabBar.m
//  mechanic
//
//  Created by 宗丽康 on 2021/9/6.
//

#import "METabBar.h"

#define MCTabBarItemHeight    49.0f

@interface METabBar()
@end
@implementation METabBar
- (instancetype)init{
    if (self = [super init]){
        [self initView];
    }
    return self;
}

- (void)initView{
    _centerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //去除选择时高亮
    _centerBtn.adjustsImageWhenHighlighted = NO;
    [self addSubview:_centerBtn];
    _centerBtn.backgroundColor = [UIColor redColor];
    
    self.backgroundColor = [UIColor whiteColor];
}

// 设置layout
- (void)layoutSubviews {
    [super layoutSubviews];
    
    switch (self.position) {
        case METabBarCenterButtonPositionCenter:
            _centerBtn.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - _centerWidth)/2.0, (MCTabBarItemHeight - _centerHeight)/2.0 + self.centerOffsetY, _centerWidth, _centerHeight);
            break;
        case METabBarCenterButtonPositionBulge:
             _centerBtn.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - _centerWidth)/2.0, -_centerHeight/2.0 + self.centerOffsetY, _centerWidth, _centerHeight);
            break;
        default:
            break;
    }
}

- (void)setCenterImage:(UIImage *)centerImage {
    _centerImage = centerImage;
    // 如果设置了宽高则使用设置的大小
    if (self.centerWidth <= 0 && self.centerHeight <= 0){
        //根据图片调整button的位置(默认居中，如果图片中心在tabbar的中间最上部，这个时候由于按钮是有一部分超出tabbar的，所以点击无效，要进行处理)
        _centerWidth = centerImage.size.width;
        _centerHeight = centerImage.size.height;
    }
    [_centerBtn setImage:centerImage forState:UIControlStateNormal];

    UIView *centerView = [[UIView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - 46)/2.0, (MCTabBarItemHeight - 46)/2.0 - 24, 46, 46)];
    centerView.layer.shadowColor = [UIColor blackColor].CGColor;
    centerView.layer.shadowOffset = CGSizeMake(0, 0);
    centerView.layer.shadowOpacity = .3;
    centerView.layer.shadowRadius = 2.0;
    centerView.layer.cornerRadius = 23;
    centerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:centerView];

    UIView *coverView = [[UIView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - 60)/2.0, (MCTabBarItemHeight - 46)/2.0, 60, 30)];
    coverView.backgroundColor = [UIColor whiteColor];
    [self addSubview:coverView];

    [self bringSubviewToFront:self.centerBtn];
}

- (void)setCenterSelectedImage:(UIImage *)centerSelectedImage {
    _centerSelectedImage = centerSelectedImage;
    [_centerBtn setImage:centerSelectedImage forState:UIControlStateSelected];
}

//处理超出区域点击无效的问题
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    if (self.hidden){
        return [super hitTest:point withEvent:event];
    }else {
        //转换坐标
        CGPoint tempPoint = [self.centerBtn convertPoint:point fromView:self];
        //判断点击的点是否在按钮区域内
        if (CGRectContainsPoint(self.centerBtn.bounds, tempPoint)){
            //返回按钮
            return _centerBtn;
        }else {
            return [super hitTest:point withEvent:event];
        }
    }
}

@end
