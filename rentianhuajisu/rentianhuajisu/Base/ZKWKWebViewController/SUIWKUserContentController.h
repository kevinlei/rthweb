//
//  ZKWKUserContentController.h
//  StudentKudou
//
//  Created by 赵坪生 on 2017/5/11.
//  Copyright © 2017年 赵坪生. All rights reserved.
//

#import <WebKit/WebKit.h>
@class SUIWKWebViewController;
@interface SUIWKUserContentController : WKUserContentController<WKScriptMessageHandler>
- (instancetype)initWithViewController:(SUIWKWebViewController *)viewController;
@end
