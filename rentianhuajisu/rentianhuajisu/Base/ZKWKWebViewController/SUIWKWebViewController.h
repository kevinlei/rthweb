//
//  ZKWKWebViewController.h
//  StudentKudou
//
//  Created by 赵坪生 on 2017/5/11.
//  Copyright © 2017年 赵坪生. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "WMDragView.h"
typedef NSDictionary * (^WebViewShareBlock)();

@interface SUIWKWebViewController : SUIRootViewController

@property(nonatomic,strong)WMDragView *dragView;

@property (nonatomic, assign)BOOL isChat;
/**
 网络加载
 */
@property (nonatomic, strong)NSString *url;

/**
 本地加载
 */
@property (nonatomic, strong)NSString *html;

@property (nonatomic, copy) WebViewShareBlock shareOptions;

- (WKWebView *)getWkwebView;

- (void)share;
@end
