//
//  METabbarController.h
//  mechanic
//
//  Created by 宗丽康 on 2021/9/3.
//

#import <UIKit/UIKit.h>
#import "METabBar.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZHUMETabBarControllerDelegate<UITabBarControllerDelegate>

// 重写了选中方法，主要处理中间item选中事件
- (void)meTabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;
@end

@interface SUITabbarController : UITabBarController
@property (nonatomic, weak) id<ZHUMETabBarControllerDelegate> meDelegate;
@property (nonatomic, strong) METabBar *meTabbar;

@end

NS_ASSUME_NONNULL_END
