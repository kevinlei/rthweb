//
//  RJSSaveViewController.m
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/20.
//

#import "TIANRJSSaveViewController.h"
#import "XWDreamHomeTableViewCell.h"

@interface TIANRJSSaveViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation TIANRJSSaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的收藏";
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStyleGrouped)];
//        tableView.layer.masksToBounds = YES;
//        tableView.layer.cornerRadius = 16;
    tableView.backgroundColor = [UIColor clearColor];
    self.tableView = tableView;
    self.tableView.rowHeight = 80;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator  = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
//        make.right.left.mas_equalTo(self.view).offset(0);
        make.top.mas_equalTo(self.view.mas_top).offset(0);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
    }];
    [tableView reloadData];
    [self showProgress];
    [self.tableView registerClass:[XWDreamHomeTableViewCell class] forCellReuseIdentifier:@"XWDreamHomeTableViewCell"];

[self loadData];
[self.tableView reloadData];
    // Do any additional setup after loading the view.
}
-(void)showProgress{
    [SVProgressHUD show];
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}
#pragma mark 返回分组数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   return self.dataArray.count;
}

#pragma mark 返回每组行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return 1;
}


#pragma mark cell视图
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    XWDreamHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"XWDreamHomeTableViewCell" forIndexPath:indexPath];
    [cell getMyNewHomeCell];
    cell.contentView.layer.masksToBounds = YES;
    cell.contentView.layer.cornerRadius = 8;
    NSDictionary *dict = self.dataArray[indexPath.section];
    cell.iconImageView.image = [UIImage imageNamed:dict[@"image"]];
    cell.titleLabel.text = dict[@"title"];
    cell.bottomLabel.text = dict[@"time"];
    NSInteger previousTime = [[NSUserDefaults standardUserDefaults] integerForKey:dict[@"title"]];
    if(!previousTime){
        previousTime = 0;
        cell.rightLabel.text = @"去阅读";
    }else{
        cell.rightLabel.text = @"继续阅读";
    }
    
    cell.studyLabel.text = [NSString stringWithFormat:@"阅读时长:%lds",(long)previousTime];
//    cell.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
//    NSArray *section =self.dataArray[indexPath.section];
//    cell.dict = [section objectAtIndex:indexPath.row];
   return cell;
}

#pragma mark 设置cell高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  90;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 15;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *vc =[UIView new];
    vc.backgroundColor = [UIColor clearColor];
    return vc;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.dataArray[indexPath.section];
            ZKWKWebViewController *vc = [ZKWKWebViewController new];
            vc.savaDict = dict;
            vc.isShowSave = YES;
            vc.myTitle = dict[@"title"];
            NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
            vc.html = str1;
            [self.navigationController pushViewController:vc animated:YES];
//    if([dict[@"urltype"] isEqualToString:@"url"]){
//        ZKWKWebViewController *vc = [ZKWKWebViewController new];
//        vc.savaDict = dict;
//        vc.isShowSave = YES;
//        vc.myTitle = dict[@"title"];
//        NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
//        vc.url = dict[@"content"];
//        [self.navigationController pushViewController:vc animated:YES];
//    }else{
//        ZKWKWebViewController *vc = [ZKWKWebViewController new];
//        vc.savaDict = dict;
//        vc.isShowSave = YES;
//        vc.myTitle = dict[@"title"];
//        NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
//        vc.html = str1;
//        [self.navigationController pushViewController:vc animated:YES];
//    }
    
   
}
-(void)loadData{
    self.dataArray = [NSMutableArray array];
    
   
    
    
    NSDictionary *dicta = @{@"title":@"东京证券交易所",
                            @"time":@"1878-5-15",
                            @"type":@"2",
                            @"image":@"2",
                            @"urltype":@"html",
                            @"content":@"它在1878年5月15日创立，同年6月1日开始交易，创立时的名称为「东京股票交易所」（日文：东京株式取引所）。二次大战时曾暂停交易，1949年5月16日重开，并更名为东京证券交易所。东京证券交易所的交易楼层於1999年4月30日关闭，全部改为电子交易，而新的TSE Arrows（东证アローズ）於2000年5月9日启用。东京证券交易所的前身是1879年5月曾成立东京证券交易株式会社。由于当时日本经济发展缓慢，证券交易不兴旺，1943年6月，日本政府合并所有证券交易所，成立了半官方的日本证券交易所，但成立不到四年就解体了。二次大战前，日本的资本主义虽有一定的发展，但由于军国主义向外侵略，重工业、兵器工业均由国家垄断经营，纺织、海运等行业也由国家控制，这是一种战争经济体制并带有浓厚的军国主义色彩。那时，即使企业发行股票，往往也被同一财阀内部的企业所消化。因此，证券业务难以发展。日本战败后，1946年在美军占领下交易所解散。1949年1月美国同意东京证券交易所重新开业。随着日本战后经济的恢复和发展，东京证券交易所也发展繁荣起来。东京证券交易所有上市公司1777家，其中外国公司 110家，市场资本总额将近45000亿美元。长期以来，大量的公众储蓄都依赖金融机构进行间接投资，使证券交易具有整批性、数量大等特点，同时也造成发行市场狭小，仅面向少数金融机构，使股票、债券的发行市场形成抽象的无形市场，与发达的流通市场相比较落后。70年代以来，日本经济实力大增，成为世界经济强国。为适应日本经济结构和经济发展的国际化需要，日本证券市场的国际化成为必然趋势。为此，日本政府自70年代以来全面放宽外汇管制，降低税率，以鼓励外国资金进入日本证券市场，使国际资本在东京证券市场的活动日益频繁。1988年，日本政府允许外国资本在东京进入场外交易；1989年，又允许外国证券公司进入东京证券交易所，使东京证券交易所在国际上的地位大大提高。"};
    [self.dataArray addObject:dicta];
    
    
    
    NSDictionary *dictb = @{@"title":@"伦敦证券交易所",
                            @"time":@"1986-10",
                            @"type":@"3",
                            @"image":@"3",
                            @"urltype":@"html",
                            @"content":@"伦敦证券交易所是世界四大证券交易所之一，作为世界上最国际化的金融中心，伦敦不仅是欧洲债券及外汇交易领域的全球领先者，还受理超过三分之二的国际股票承销业务。伦敦的规模与位置，意味着它为世界各地的公司及投资者提供了一个通往欧洲的理想门户。在保持伦敦的领先地位方面，伦敦证券交易所扮演着中心角色，它运作世界上国际最强的股票市场，其外国股票的交易超过其它任何证交所。作为世界第三大证券交易中心，伦敦证券交易所是世界上历史最悠久的证券交易所。伦敦证券交易所曾为当时英国经济的兴旺立下汗马功劳，但随着英国国内和世界经济形势的变化，其浓重的保守色彩，特别是沿袭的陈规陋习严重阻碍了英国证券市场的发展，影响厂中场竞争力，在这一形势下，伦敦证券交易所于1986年10月进行了重大改革，例如，改革固定佣金制；允许大公司直接进入交易所进行交易；放宽对会员的资格审查；允许批发商与经纪人兼营：证券交易全部实现电脑化，与纽约、东京交易所连机，实现24小时全球交易。这些改革措施使英国证券市场发生了根本性的变化，巩固了其在国际证券市场中的地位。伦敦的外国股票交易额始终高于其它市场。这反映了外国公司在伦敦证券交易所业务中的中心地位—在伦敦证券交易所交易的外国股票远远超出英国本土的股票，这种情形是独一无二的。在伦敦，外国股票的平均日交易额达到195亿美元，远远高于其它任何主要证交所。伦敦市场的规模、威望和全球操作围，意味着在这里上市和交易的外国公司可获得全球瞩目和覆盖。 伦敦证券交易所面向外国股票的交易及信息系统包括国际股票自动对盘交易系统和EAQ国际股票自动报价系统。伦敦证券交易所的国际股票自动对盘交易系统是一套由订单驱动的全自动交易设施。它有助于提高在伦敦证券交易所交易量最大的外国公司的知名度，并以其速度、透明度和效率促进这些公司股票的交易。 SEAQ国际股票自动报价系统是一套由报价驱动的基于屏幕上的交易及信息系统，已成为在伦敦上市的外国公司与其投资者之间出色的沟通渠道，并已成为该领域的衡量标准。在增进上市公司的股票在其本土市场、伦敦市场以及世界其它金融中心的流动性方面，SEAQ国际股票自动报价系统发挥着关键作用。"};
    [self.dataArray addObject:dictb];
    
    
    NSDictionary *dict1 = @{@"title":@"全球金融体系的分类及特点",
                            @"time":@"2023-01-13",
                            @"type":@"4",
                            @"image":@"4",
                            @"urltype":@"html",
                            @"content":@"首先，金融市场是指以金融资产为交易对象，以金融资产的供给方和需求方为交易主体形成的交易机制及其关系的总和。按照金融市场交易标的物划分是全球金融市场最常见的划分方法。按这一划分标准，金融市场分为货币市场、资本市场、外汇市场、金融衍生品市场、保险市场、黄金市场及其他投资品市场。1、货币市场 货币市场又称“短期金融市场”“短期资金市场”，是指融资期限在一年及一年以下的金融市场。可以看出货币市场的期限一般都比较短，流动性高。货币市场主要包括同业拆借市场、票据市场、回购市场和货币市场基金等。(1)同业拆借市场又被称为“同业拆放市场”，是指金融机构之间以货币借贷方式进行短期资金融通活动的市场。同业拆借市场最早出现于美国，其形成的根本原因在于法定存款准备金制度的实施。(2)票据市场指的是在商品交易和资金往来过程中产生的以汇票、本票和支票的发行、担保、承兑、贴现、转贴现、再贴现来实现短期资金融通的市场。(3)回购市场指通过回购协议进行短期资金融通交易的市场。回购是指资金融入方在出售证券时，与证券的购买商签订协议，约定在一定期限后按约定的价格购回所卖证券，从而获得即时可用资金的一种交易行为。在银行间债券市场上回购业务可以分为质押式回购业务、买断式回购业务和开放式回购业务三种类型。 (4)货币市场基金(简称“MMF”)是指投资于货币市场上短期(一年以内，平均期限120天)有价证券的一种投资基金。2、资本市场 资本市场亦称“长期金融市场”“长期资金市场”，是指期限在一年以上的各种资金借贷和证券交易的场所。资本市场一般偏长期。资本市场主要包括股票市场、债券市场和基金市场等。3、外汇市场 外汇市场是指经营外币和以外币计价的票据等有价证券买卖的市场。外汇市场具有高杠杆、双向交易、极高流动性、24小时交易等特点4、金融衍生品市场 金融衍生品是指以杠杆或信用交易为特征，在传统金融产品(货币、债券、股票等)的基础上派生出来的，具有新价值的金融工具，如期货、期权、互换及远期等。以上就是按照交易标的对全球金融体系分类和特点的简单介绍。"};
    [self.dataArray addObject:dict1];
    
   
    
    [self.dataArray reverse];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
