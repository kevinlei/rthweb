//
//  AboutUsViewController.m
//  AboutUsViewController
//
//  Created by HDOceandeep on 2022/3/28.
//

#import "TIANHDPeiyuAboutUsViewController.h"

@interface TIANHDPeiyuAboutUsViewController ()

@end

@implementation TIANHDPeiyuAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    
    
    UIImageView * headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mylogo"]];
    [self.view addSubview:headerImageView];
    [headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
        make.top.mas_equalTo(self.view.mas_top).offset(100);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(180);
    }];
    
    // 当前app的信息
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

//     CFShow(i(__bridge CFTypeRef)(infoDictionary));

    // app名称

    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];

    // app版本

    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];

    // app build版本

    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];

    
    UILabel * Labelfeleiname = [UILabel new];
    Labelfeleiname.text = [NSString stringWithFormat:@"%@",app_Name];
    Labelfeleiname.textColor = [UIColor blackColor];
    Labelfeleiname.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    [self.view addSubview:Labelfeleiname];
    [Labelfeleiname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headerImageView.mas_bottom).offset(25);
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
        make.height.mas_equalTo(21);
    }];

    
    UILabel * Labelfelei = [UILabel new];
    Labelfelei.text = [NSString stringWithFormat:@"版本：%@",app_Version];
    Labelfelei.textColor = [UIColor blackColor];
    Labelfelei.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    [self.view addSubview:Labelfelei];
    [Labelfelei mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(Labelfeleiname.mas_bottom).offset(25);
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
        make.height.mas_equalTo(21);
    }];
    
    UILabel * Labelfelei1 = [UILabel new];
    Labelfelei1.numberOfLines = 0;
    Labelfelei1.text = @"仁天花是一款为用户提供全方位计算服务的应用软件，银行利率计算器，在线计算银行活期、定期存款利息计算器，采用最新存款利率表，在线各种银行存款利息计算器，特别是金额小写转大写功能，备受群众喜爱和使用。";
    Labelfelei1.textColor = [UIColor blackColor];
    Labelfelei1.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    [self.view addSubview:Labelfelei1];
    [Labelfelei1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(Labelfelei.mas_bottom).offset(25);
        make.left.mas_equalTo(self.view.left).offset(20);
        make.right.mas_equalTo(self.view.right).offset(-20);
        make.height.mas_equalTo(200);
    }];
    
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
