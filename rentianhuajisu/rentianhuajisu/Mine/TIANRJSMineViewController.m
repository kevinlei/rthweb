//
//  RJSMineViewController.m
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/19.
//

#import "TIANRJSMineViewController.h"
#import "TIANHDPeiyuAboutUsViewController.h"
#import "TIANRJSSaveViewController.h"
#import "TIANAppDelegate.h"

@interface TIANRJSMineViewController ()
@property(nonatomic,strong) UIView *bgView2;
@property(nonatomic,strong)UILabel * namelabel;
@property(nonatomic,strong)UIButton *qianbaoButton1;

@end

@implementation TIANRJSMineViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    if ([USERPHONE length]==0) {
        self.namelabel.text = @"请先点击登录哦";
        self.qianbaoButton1.hidden = YES;
    }else{
        NSString *user = [userDefault objectForKey:@"USERPHONE"];
        self.namelabel.text = [NSString stringWithFormat:@"欢迎回来%@",user];
        self.qianbaoButton1.hidden = NO;
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = rgb(243, 246, 254);
    UIImageView * topBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
//    topBG.backgroundColor = [UIColor colorWithHexString:@"6699FF"];
    [self.view addSubview:topBG];
    topBG.userInteractionEnabled = YES;
    [topBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(0);
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(200);
    }];
    
    
    UILabel * Label0 = [UILabel new];
    Label0.text = @"个人中心";
//    Label0.textColor = [UIColor whiteColor];
    Label0.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [topBG addSubview:Label0];
    [Label0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topBG.mas_top).offset(50);
        make.centerX.mas_equalTo(topBG.mas_centerX).offset(0);
        make.height.mas_equalTo(21);
    }];
    
    UIButton *qianbaoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [qianbaoButton setImage:[UIImage imageNamed:@"客服"] forState:UIControlStateNormal];
    [topBG addSubview:qianbaoButton];
    [qianbaoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(Label0.mas_centerY).offset(0);
        make.right.mas_equalTo(topBG.mas_right).offset(-20);
        make.height.width.mas_equalTo(15);
    }];
    qianbaoButton.hidden  = YES;
    [qianbaoButton bk_whenTapped:^{
        NSMutableString * phoneStr=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"028-38289396"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
    }];
    
    
    UIImageView * header = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mylogo"]];
    header.layer.masksToBounds = YES;
    header.layer.cornerRadius = 65/2;
    [topBG addSubview:header];
    header.userInteractionEnabled = YES;
    [header mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(120);
        make.left.mas_equalTo(self.view.mas_left).offset(20);
        make.height.width.mas_equalTo(65);
    }];
    [header bk_whenTapped:^{
        if ([USERPHONE length]==0) {
            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
            [self.navigationController pushViewController:vc animated:YES];
            [SUIUtils makeShortToastAtCenter:@"请先登录"];
            return;
        }
    }];
    
    
    UILabel * Label = [UILabel new];
    self.namelabel = Label;
    Label.text = @"仁天花";
    Label.textAlignment = NSTextAlignmentLeft;
//    Label.textColor = [UIColor whiteColor];
    Label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [topBG addSubview:Label];
    [Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(header.mas_right).offset(15);
        make.centerY.mas_equalTo(header.mas_centerY).offset(0);
        make.height.mas_equalTo(25);
//        make.width.mas_equalTo(60);
    }];
    Label.userInteractionEnabled = YES;
    [Label bk_whenTapped:^{
        if ([USERPHONE length]==0) {
            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
            [self.navigationController pushViewController:vc animated:YES];
            [SUIUtils makeShortToastAtCenter:@"请先登录"];
            return;
        }
        
    }];
    
    
//    UIView *bgView = [UIView new];
//    bgView.backgroundColor = [UIColor whiteColor];
//    bgView.layer.masksToBounds = YES;
//    bgView.layer.cornerRadius = 16;
//    [topBG addSubview:bgView];
//    self.bgView2 = bgView;
//    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(header.mas_bottom).offset(15);
//        make.left.mas_equalTo(topBG.mas_left).offset(20);
//        make.right.mas_equalTo(topBG.mas_right).offset(-20);
//        make.height.mas_equalTo(120);
//    }];
//
//    UILabel * Label1 = [UILabel new];
//    Label1.text = @"我的借款";
//    Label1.textAlignment = NSTextAlignmentLeft;
//    Label1.textColor = [UIColor colorWithHexString:@"#333333"];
//    Label1.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
//    [bgView addSubview:Label1];
//    [Label1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(bgView.mas_left).offset(20);
//        make.top.mas_equalTo(bgView.mas_top).offset(10);
//        make.height.mas_equalTo(20);
////        make.width.mas_equalTo(60);
//    }];
//    [self createJiugongge2];
    
    
    
   
    
    
    
//    UIImageView * topBG1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"编组 5"]];
//    [self.view addSubview:topBG1];
//    topBG1.userInteractionEnabled = YES;
//    [topBG1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(bgView.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(20);
//        make.right.mas_equalTo(self.view.mas_right).offset(-20);
//        make.height.mas_equalTo(130);
//    }];
//    [topBG1 bk_whenTapped:^{
////        [self.navigationController pushViewController:[RTHGetMoneyViewController new] animated:YES];
//    }];
//
    
    
//    NSArray *titles = @[@"我的发布",@"我的收藏",@"协议中心",@"关于我们",@"联系电话",];
//    NSArray *titles = @[@"协议中心",@"反馈建议",@"关于我们",@"联系电话",@"注销账号"];
    NSArray *titles = @[@"投诉反馈",@"版本更新",@"协议中心",@"关于我们",@"联系电话",@"注销账号"];
    CGFloat H =  70 *titles.count;
    UIView *bgView1 = [UIView new];
    bgView1.backgroundColor = [UIColor clearColor];
    bgView1.layer.masksToBounds = YES;
    bgView1.layer.cornerRadius = 16;
    [self.view addSubview:bgView1];
    [bgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(header.mas_bottom).offset(40);
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(-0);
        make.height.mas_equalTo(H);
    }];

    [self createJiugonggeWithView2:bgView1 titles:titles];
    
    
    UIButton *qianbaoButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.qianbaoButton1 = qianbaoButton1;
//    qianbaoButton1.backgroundColor = [UIColor redColor];
    [qianbaoButton1 setTitle:@"退出登录" forState:UIControlStateNormal];
    [qianbaoButton1 setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIFontWeightMedium];
    qianbaoButton1.titleLabel.font =[UIFont systemFontOfSize:15 weight:UIFontWeightBold];
    qianbaoButton1.layer.masksToBounds = YES;
    qianbaoButton1.layer.cornerRadius = 9;
    qianbaoButton1.layer.borderColor = [UIColor colorWithHexString:@"#666666"].CGColor;
    qianbaoButton1.layer.borderWidth = 0.5;
    [self.view addSubview:qianbaoButton1];
    [qianbaoButton1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-130);
        make.left.mas_equalTo(self.view.mas_left).offset(20);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.height.mas_equalTo(50);
    }];
    [qianbaoButton1 bk_whenTapped:^{
        if ([USERPHONE length]==0) {
            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
            [self.navigationController pushViewController:vc animated:YES];
            [SUIUtils makeShortToastAtCenter:@"请先登录"];
            return;
        }
        
    [SVProgressHUD show];
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [userDefault setObject:@"" forKey:@"USERPHONE"];
        [userDefault setObject:@"" forKey:@"USERPASSWORD"];
        [userDefault synchronize];
        [self viewWillAppear:YES];
        [SUIUtils makeShortToastAtCenter:@"退出成功"];
    });

    }];

}
- (void) createJiugonggeWithView2:(UIView *)BGview  titles:(NSArray*)titles{
    BGview.backgroundColor = [UIColor clearColor];
    int padding = 10;
    float width = (CGRectGetWidth(self.view.bounds) - 20) / 1;
    int count = 1;
    for(int i = 0; i < titles.count; i++) {
        for(int j = 0; j < 1; j++) {
            UIView *view = [UIView new];
            view.userInteractionEnabled = YES;
            view.backgroundColor = [UIColor clearColor];
            [BGview addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(BGview).offset(0+(width)*j);
                make.top.mas_equalTo(BGview).offset(0+(0+45)*i);
                make.height.mas_equalTo(45);
                make.width.mas_equalTo(BGview.mas_width);
            }];
            
            if(i == 0){
                [view bk_whenTapped:^{
                    
//                    RegistViewController *vc = [RegistViewController new];
                    
       
                    if([USERPHONE length] == 0){
                        TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
                        [self.navigationController pushViewController:vc animated:YES];
                        [SUIUtils makeShortToastAtCenter:@"请先登录"];
                        return;
                    }
                    TIANRTHFankuiViewController *vc = [TIANRTHFankuiViewController new];
                    [self.navigationController pushViewController:vc animated:YES];
                }];

               
            }
            
            if(i == 1){
                [view bk_whenTapped:^{
                    [SVProgressHUD show];
                                              dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
                                              dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                                  [SVProgressHUD dismiss];
           
                                                  [SUIUtils makeShortToastAtCenter:@"您当前已是最新版本"];
                                              });
                }];

               
            }
            if(i == 2){
                [view bk_whenTapped:^{
                    ZKWKWebViewController *vc = [ZKWKWebViewController new];
                                        vc.url =  [NSString stringWithFormat:@"%@%@",@"http://39.107.109.205:8081",@"/yrj/frame/h5/privacypolicy"];
                                        [self.navigationController pushViewController:vc animated:YES];
//                    if([USERTOKEN length] == 0){
//                        [self.navigationController pushViewController:[YTXRegistrationVC new] animated:YES];
//                        return;
//                    }
//                    [self.navigationController pushViewController:[RTHXieyiViewController new] animated:YES];
                }];
               
            }
            if(i == 3){
                [view bk_whenTapped:^{
                    
                [self.navigationController pushViewController:[TIANHDPeiyuAboutUsViewController new] animated:YES];
                }];
               
            }
            
            
           
            
            if(i == 4){
                [view bk_whenTapped:^{
                    NSMutableString * phoneStr=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"028-38289396"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
                }];
               
            }
            
            
            if(i == 5){
                [view bk_whenTapped:^{
                    
               
                    
                    if([USERPHONE length] == 0){
                        TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
                        [self.navigationController pushViewController:vc animated:YES];
                        [SUIUtils makeShortToastAtCenter:@"请先登录"];
                        return;
                    }
                    
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认要注销的账号吗？" preferredStyle:UIAlertControllerStyleAlert];
                       
                       
                       UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                           
                           
                           [SVProgressHUD show];
                           NSString *url = [HTTPInterface PublicForWard];
                           NSDictionary *dict1 =@{@"parameter":@"logoutUserInfo"};
                           NSDictionary *param1 = @{@"json":[dict1 jsonStringEncoded],@"loginToken":USERTOKEN};
                           [HttpRequestModel request:url withParamters:param1 success:^(id responseData) {
                                           if ([responseData[@"code"] intValue]==1000) {
                                               [userDefault setObject:@"" forKey:@"USERID"];
                                               [userDefault setObject:@"" forKey:@"USERPHONE"];
                                               [userDefault setObject:@"" forKey:@"USERPASSWORD"];
                                               [userDefault setObject:@"" forKey:@"USERTOKEN"];
                                               [userDefault setObject:@"" forKey:@"USERCustomerType"];
                                               [userDefault setObject:@"" forKey:@"USERID"];
                                               [userDefault synchronize];
                                               [SUIUtils makeShortToastAtCenter:@"账号已注销"];
                                               [self viewWillAppear:YES];
//                                               SUIAppDelegate *app = [SUIAppDelegate shareAppDelegate];
//                                               [app changeLoginState:NO];
                                           }
                               [SVProgressHUD dismiss];
                           } failure:^(NSError *error) {
                                           [SUIUtils makeShortToastAtCenter:error.localizedDescription];
                                           [SVProgressHUD dismiss];
                           }];
                           
                           
//                           [SVProgressHUD show];
//                           dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
//                           dispatch_after(delayTime, dispatch_get_main_queue(), ^{
//                               [SVProgressHUD dismiss];
//                               [SUIUtils makeShortToastAtCenter:@"您的账号已经注销成功"];
//                               NSArray *deleteids = [userDefault objectForKey:@"deleteid"];
//                               if (deleteids) {
//                                   NSMutableArray *array = [NSMutableArray arrayWithArray:deleteids];
//                                   [array addObject:USERPHONE];
//                                   [userDefault setObject:array forKey:@"deleteid"];
//                               }else{
//                                   NSMutableArray *array = [NSMutableArray array];
//                                   [array addObject:USERPHONE];
//                                   [userDefault setObject:array forKey:@"deleteid"];
//                               }
//                               [self viewWillAppear:YES];
//
////                               [SVProgressHUD show];
//                               dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
//                               dispatch_after(delayTime, dispatch_get_main_queue(), ^{
////                                   [SVProgressHUD dismiss];
//                                   [userDefault setObject:@"" forKey:@"USERPHONE"];
//                                   [userDefault setObject:@"" forKey:@"USERPASSWORD"];
//                                   [userDefault synchronize];
//                                   [self viewWillAppear:YES];
//                                   [SUIUtils makeShortToastAtCenter:@"退出成功"];
//                               });
//
//                           });
                       }];
                       
                       UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                       [alertController addAction:cancelAction];
                       [alertController addAction:okAction];
                       
                       
                       [self presentViewController:alertController animated:YES completion:nil];
                    
                    
//                [SVProgressHUD show];
//                NSString *url = [HTTPInterface PublicForWard];
//                NSDictionary *dict1 =@{@"parameter":@"logoutUserInfo"};
//                NSDictionary *param1 = @{@"json":[dict1 jsonStringEncoded],@"loginToken":USERTOKEN};
//                [HttpRequestModel request:url withParamters:param1 success:^(id responseData) {
//                                if ([responseData[@"code"] intValue]==1000) {
//                                    [userDefault setObject:@"" forKey:@"USERID"];
//                                    [userDefault setObject:@"" forKey:@"USERPHONE"];
//                                    [userDefault setObject:@"" forKey:@"USERPASSWORD"];
//                                    [userDefault setObject:@"" forKey:@"USERTOKEN"];
//                                    [userDefault setObject:@"" forKey:@"USERCustomerType"];
//                                    [userDefault setObject:@"" forKey:@"USERID"];
//                                    [userDefault synchronize];
//                                    [SUIUtils makeShortToastAtCenter:@"账号已注销"];
////                                    [self viewWillAppear:YES];
////                                    SUIAppDelegate *app = [SUIAppDelegate shareAppDelegate];
////                                    [app changeLoginState:NO];
//                                }
//                    [SVProgressHUD dismiss];
//                } failure:^(NSError *error) {
//                                [SUIUtils makeShortToastAtCenter:error.localizedDescription];
//                                [SVProgressHUD dismiss];
//                }];
//
//                }];
                    
                }];
            }
            
            UIView *line = [UIView new];
            line.backgroundColor = [UIColor colorWithHexString:@"#C1C2CA"];
            [view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(view.mas_left).offset(20);
                make.right.mas_equalTo(view.mas_right).offset(-20);
                make.bottom.mas_equalTo(view.mas_bottom).offset(0);
                make.height.mas_equalTo(0.5);
            }];
//            line.hidden = YES;
            if((titles.count-1) == i){
                line.hidden = YES;
            }else{
                line.hidden = NO;
            }
            
            
            
            UIImageView * ImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"首页_填充"]];
            ImageView.userInteractionEnabled = YES;
            ImageView.hidden = YES;
            [view addSubview:ImageView];
            [ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(view.mas_left).offset(10);
                make.height.width.mas_equalTo(10);
                make.centerY.mas_equalTo(view.mas_centerY).offset(0);
            }];



            UILabel *l = [UILabel new];

            l.text = titles[i];
            
//            @[@"银行卡",@"协议中心",@"反馈建议",@"关于我们",@"联系电话",@"注销账号"];
//            if([l.text isEqualToString:@"银行卡"]){
//                ImageView.image = [UIImage imageNamed:@"添加银行卡"];
//            }
//            if([l.text isEqualToString:@"协议中心"]){
//
//                ImageView.image = [UIImage imageNamed:@"申请进度"];
//            }
//            if([l.text isEqualToString:@"反馈建议"]){
//                ImageView.image = [UIImage imageNamed:@"借款记录"];
//            }
//            if([l.text isEqualToString:@"关于我们"]){
//                ImageView.image = [UIImage imageNamed:@"关于我们"];
//            }
//            if([l.text isEqualToString:@"联系电话"]){
//                ImageView.image = [UIImage imageNamed:@"联系客服"];
//            }
//            if([l.text isEqualToString:@"注销账号"]){
//                ImageView.image = [UIImage imageNamed:@"添加银行卡"];
//            }
//

            l.textAlignment = NSTextAlignmentLeft;

            l.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];

            l.textColor = [UIColor colorWithHexString:@"#333333"];

            [view addSubview:l];
            [l mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(view);
                make.width.mas_equalTo(120);
                make.left.mas_equalTo(ImageView.mas_right).offset(kMargin);
            }];
            
            UIImageView * ImageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"返回"]];
            ImageView1.userInteractionEnabled = YES;
//            ImageView1.hidden = YES;
            [view addSubview:ImageView1];
            [ImageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(view.mas_right).offset(-20);
                make.width.mas_equalTo(16);
                make.height.mas_equalTo(16);
                make.centerY.mas_equalTo(view.mas_centerY).offset(0);
            }];
            
            UITextField * Label = [UITextField new];
            Label.textAlignment  = NSTextAlignmentCenter;
            Label.placeholder = @"请输入";
            Label.hidden = YES;
//            Label.delegate = self;
//            if (i==0) {
//                self.Label0 = Label;
//            }
//            if (i==1) {
//                self.Label1 = Label;
//                Label.keyboardType = UIKeyboardTypePhonePad;
//            }
//            if (i==2) {
//                self.Label2 = Label;
//            }
//            if (i==3) {
//                self.Label3 = Label;
//            }
//            if (i==4) {
//                self.Label4 = Label;
//            }
            
            Label.backgroundColor = [UIColor clearColor];
            Label.textColor = [UIColor colorWithHexString:@"#743FF3"];
            Label.textColor = [UIColor colorWithHexString:@"#743FF3"];
            Label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
            [view addSubview:Label];
            [Label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(view.mas_top);
                make.bottom.mas_equalTo(view.mas_bottom);
                make.left.mas_equalTo(l.mas_right).offset(10);
                make.right.mas_equalTo(ImageView1.mas_right).offset(-10);
            }];
            
            




            count++;

        }

    }

}
    // Do any additional setup after loading the view.
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
