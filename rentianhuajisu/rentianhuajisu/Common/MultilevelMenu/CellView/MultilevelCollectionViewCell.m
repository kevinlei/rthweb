//
//  MultilevelCollectionViewCell.m
//  MultilevelMenu
//
//  Created by gitBurning on 15/3/13.
//  Copyright (c) 2015年 BR. All rights reserved.
//

#import "MultilevelCollectionViewCell.h"

@implementation MultilevelCollectionViewCell

- (void)awakeFromNib {
    self.imageView.layer.borderColor = [UIColor colorWithHexString:@"#EEEEEE"].CGColor;
    self.imageView.layer.borderWidth = 0.5;
    self.imageView.layer.masksToBounds  = YES;
    self.imageView.layer.cornerRadius=  9;
    self.titile.textColor = [UIColor colorWithHexString:Navigation_And_StatusBar_Color];
    // Initialization code
}

@end
