//
//  netReachability.m
//  EDU268
//
//  Created by mac on 14/12/6.
//  Copyright (c) 2014年 北京易知路科技有限公司. All rights reserved.
//

#import "netReachability.h"


@implementation netReachability
// 是否wifi
+ (BOOL) IsEnableWIFI {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    BOOL wifi = manager.reachableViaWiFi;
    //NSLog(@"是否是WiFi%d",wifi);
    return wifi;
}

// 是否3G
+ (BOOL) IsEnable3G {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    
    BOOL net234G = manager.reachableViaWWAN;
    //NSLog(@"是否是net234G%d",net234G);
    return net234G;
}

+ (BOOL) isEnableNet
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    
    BOOL isExistNet = manager.reachable;
    //NSLog(@"是否是isExistNet%d",isExistNet);
    return isExistNet;
}

@end
