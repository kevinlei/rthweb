//
//  netReachability.h
//  EDU268
//
//  Created by mac on 14/12/6.
//  Copyright (c) 2014年 北京易知路科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface netReachability : NSObject

+ (BOOL) IsEnableWIFI;//是否是wifi
+ (BOOL) IsEnable3G;//是否是2.3.4G网络
+ (BOOL) isEnableNet;//是否有网

@end
