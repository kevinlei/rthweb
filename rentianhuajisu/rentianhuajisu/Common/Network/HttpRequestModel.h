//
//  HttpRequestModel.h
//  爱启航
//
//  Created by 爱启航mac on 2017/7/24.
//  Copyright © 2017年 爱启航mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpRequestModel : NSObject

/** 1 * post/Get 请求 有进度 */
+ (void)httpRequest:(NSString *)urlString withParamters:(id)paramters isPost:(BOOL)isPost success:(void (^)(id responseData))success failure:(void (^)(NSError *error))failure;

/** 2 * post 请求 无进度 */
+ (void)request:(NSString *)urlString withParamters:(NSDictionary *)dic success:(void (^)(id responseData))success failure:(void (^)(NSError *error))failure;
/** 3 * Get 请求 无进度 */
+ (void)requestGet:(NSString *)urlString withParamters:(NSDictionary *)dic success:(void (^)(id responseData))success failure:(void (^)(NSError *error))failure;

+ (HttpRequestModel *) sharedHttpRequestModel;

@end
