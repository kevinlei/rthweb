//
//  HDMessageSegment.h
//  SafetyTrainProject
//
//  Created by 5555 on 2018/10/16.
//  Copyright © 2018 kevinxuelei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^CurrentIndex)(NSInteger index);
@interface SUIHDMessageSegment : UIView
+ (SUIHDMessageSegment *)createSegementViews:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles;

+ (SUIHDMessageSegment *)createSegementWithFrame:(CGRect)frame views:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles buttonImage:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
