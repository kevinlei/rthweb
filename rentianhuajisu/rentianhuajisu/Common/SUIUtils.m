//
//  MEUtils.m
//  mechanic
//
//  Created by HDOceandeep on 2021/9/11.
//

#import "SUIUtils.h"
#import <SDWebImage/SDImageCache.h>
@implementation SUIUtils
+ (UIViewController *)topViewController{
  return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController *)topViewController:(UIViewController *)rootViewController{
  if (rootViewController.presentedViewController == nil) {
    return rootViewController;
  }
  
  if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
    UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
    UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
    return [self topViewController:lastViewController];
  }
  
  UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
  return [self topViewController:presentedViewController];
}

+ (void) makeShortToastAtCenter: (NSString*)toast{
  [[self topViewController].view makeToast:toast duration:1.5 position:CSToastPositionCenter];
}

+ (void) makeShortToastAtBottom: (NSString*)toast{
  [[self topViewController].view makeToast:toast duration:1.5 position:CSToastPositionBottom];
}

+ (void) makeShortToastWindowAtCenter: (NSString*)toast{
  [[self getDefaultWindow] makeToast:toast duration:1.5 position:CSToastPositionCenter];
}

+ (void) makeShortToastWindowAtBottom: (NSString*)toast{
  [[self getDefaultWindow] makeToast:toast duration:1.5 position:CSToastPositionBottom];
}


+ (UIWindow*) getDefaultWindow{
    return [UIApplication sharedApplication].keyWindow ;
}

//字典转json格式字符串：
+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
  NSError *parseError = nil;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
  return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

//json格式字符串转字典：
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
  if (jsonString == nil) {
    return nil;
  }
  NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSError *err;
  NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                      options:NSJSONReadingMutableContainers

                                                        error:&err];
  if(err) {
    NSLog(@"json解析失败：%@",err);
    return nil;
  }
  return dic;
}
+ (NSString *)jsonStringWithDict:(NSDictionary *)dict {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString;
    if (!jsonData) {
        NSLog(@"%@",error);
    }else{
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}
+ (NSString *)getCurrentDeviceUUID{
  return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+ (long long)folderSizeAtPath:(NSString *)path {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  long long folderSize = 0;
  if ([fileManager fileExistsAtPath:path]) {
    NSArray *subFiles = [fileManager subpathsAtPath:path];
    for (NSString *fileName in subFiles) {
      NSString *fileAbsolutePath = [path stringByAppendingPathComponent:fileName];
      long long size = [SUIUtils fileSizeAtPath:fileAbsolutePath];
      folderSize += size;
    }
  }
  return folderSize;
}

+ (long long)fileSizeAtPath:(NSString *)filePath {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  if ([fileManager fileExistsAtPath:filePath]) {
    long long size = [fileManager attributesOfItemAtPath:filePath error:nil].fileSize;
    return size;
  }
  return 0;
}
+ (CGFloat)sizoOfCache {
//    NSUInteger imageCache = [[SDImageCache sharedImageCache] getSize];
//    CGFloat cacheSize = ( imageCache) / (1024 * 1024);
    return 0;
}
+(NSString *)convertToJsonStr:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    return mutStr;
    
}

@end
