//
//  SelectedListView.m
//  LEEAlertDemo
//
//  Created by 李响 on 2017/6/4.
//  Copyright © 2017年 lee. All rights reserved.
//

#import "SelectedListView.h"
#import "RTHMoneyTableViewCell.h"
@interface SelectedListView ()<UITableViewDelegate , UITableViewDataSource>
@property (nonatomic ,strong) NSMutableArray * selectedArr;
@property (nonatomic , strong ) NSMutableArray *dataArray;

@end

@implementation SelectedListView

- (void)dealloc{
    
    _dataArray = nil;
}

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    
    self = [super initWithFrame:frame style:style];
    
    if (self) {
        
        //初始化数据
        
        [self initData];
    }
    
    return self;
}

#pragma mark - 初始化数据

- (void)initData{
    
    self.selectedArr = [NSMutableArray array];
    
    self.backgroundColor = [UIColor clearColor];
    
    self.delegate = self;
    
    self.dataSource = self;
    
    self.bounces = NO;
    
    self.allowsMultipleSelectionDuringEditing = YES; //支持同时选中多行
    
    self.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    self.separatorColor = [[UIColor grayColor] colorWithAlphaComponent:0.2f];
    
    self.dataArray = [NSMutableArray array];
    self.allowsMultipleSelection = YES;//多选
//    [self registerClass:[MKZProductListTableViewCell class] forCellReuseIdentifier:@"ZHUProductListTableViewCell"];
//    [self registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

- (void)setArray:(NSArray<SelectedListModel *> *)array{
    
    _array = array;
    
    [self reloadData];
    
    [self setEditing:!self.isSingle animated:NO];
    
    CGRect selfFrame = self.frame;
    
    selfFrame.size.height = array.count * 110.0f;
    
    self.frame = selfFrame;
}

- (void)setIsSingle:(BOOL)isSingle{
    
    _isSingle = isSingle;
    
    [self setEditing:!isSingle animated:NO];
}

- (void)finish{
    
    if (self.selectedBlock) self.selectedBlock(self.dataArray);
}

#pragma mark - UITableViewDelegate , UITableViewDataSource
#pragma mark 返回分组数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   return 1;
}

#pragma mark 返回每组行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return 10;
}


#pragma mark cell视图
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RTHMoneyTableViewCell *cell = [RTHMoneyTableViewCell RTHMoneyTableViewCelllWithTableView:tableView];
//    MKZProductListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZHUProductListTableViewCell" forIndexPath:indexPath];
//    cell.isCheckOut = YES;
//    cell.dict = self.array[indexPath.row];
   return cell;
}

#pragma mark 设置cell高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  55;
}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSDictionary *dict = self.array[indexPath.row];
//    [SUIUtils makeShortToastAtCenter:[NSString stringWithFormat:@"您提交的\n%@订阅申请\n正在审核中,\n请耐心等待",dict[@"name"]]];
////    XGZJiPinDetailViewController *vc = [XGZJiPinDetailViewController new];
////    vc.dict = self.dataArray[indexPath.row];
////    [[[XGZAppDelegate shareAppDelegate] getCurrentUIVC].navigationController pushViewController:vc animated:YES];
//
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
     NSLog(@"选择了第 %ld Section 第 %ldRow ",(long)indexPath.row,(long)indexPath.row);
    [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
     [self.selectedArr removeObject:indexPath];
     [self.selectedArr addObject:indexPath];
     NSLog(@"加入selectedArr=%ld",self.selectedArr.count);
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
   
     NSLog(@"取消选择了第 %ld Section 第 %ldRow ",(long)indexPath.row,(long)indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.selectedArr removeObject:indexPath];
    NSLog(@"取消selectedArr=%ld",self.selectedArr.count);
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    id model = self.array[indexPath.row];
//
//    [self.dataArray addObject:model];
//
//    if (self.isSingle) {
//
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//        [self finish];
//
//    } else {
//
//        if (self.changedBlock) self.changedBlock(self.dataArray);
//    }
//
//}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath  {
//    
//    id model = self.array[indexPath.row];
//    
//    if (!self.isSingle) {
//        
//        [self.dataArray removeObject:model];
//        
//        if (self.changedBlock) self.changedBlock(self.dataArray);
//    }
//}

@end
