//
//  HDDataManager.m
//  HDDataManager
//
//  Created by HDOceandeep on 2022/3/26.
//

#import "SUIDataManager.h"

@interface SUIDataManager()
@property (nonatomic, strong) NSArray *incomeArray;
@property (nonatomic, strong) NSArray *outcomeArray;

@property (nonatomic, copy) NSString *incomePath;
@property (nonatomic, copy) NSString *outcomePath;
@property (nonatomic, copy) NSString *caigouPath;
@property (nonatomic, copy) NSString *fabushuiguoPath;
@property (nonatomic, strong)NSDateFormatter *dateFormatter ;
@end


@implementation SUIDataManager

+ (instancetype)shareManager{
    static SUIDataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SUIDataManager alloc] init];
        [manager loadPlist];
        [manager loadData];
        [manager setupLocalData];
    });
    return manager;
}

-(void)setupLocalData{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        
        NSArray *array = @[@{@"isColloct":@(YES),
                                   @"title":@"纽约证券交易所",
                                   @"title1":@"New York Stock Exchange，NYSE",
                                   @"title2":@"美国纽约州纽约市百老汇大街18号",
                                   @"title3":@"在美国证券发行之初，尚无集中交易的证券交易所，证券交易大都在咖啡馆和拍卖行里进行，纽约证券交易所的起源可以追溯到1792年5月17日，当时24个证券经纪人在纽约华尔街68号外一棵梧桐树下签署了梧桐树协议，协议规定了经纪人的“联盟与合作”规则，通过华尔街现代老板俱乐部会员制度交易股票和高级商品，这也是纽约交易所的诞生日。1817年3月8日这个组织起草了一项章程，并把名字更改为“纽约证券交易委员会”。1863年改为现名“纽约证券交易所”。从1868年起，只有从当时老成员中买得席位方可取得成员资格。纽约证券交易所的第一个总部是1817年一间月租200美金，位于华尔街40号的房间。1865年交易所才拥有自己的大楼。坐落在纽约市华尔街11号的大楼是1903年启用的。交易所内设有主厅、蓝厅、“车房”等三个股票交易厅和一个债券交易厅，是证券经纪人聚集和互相交易的场所，共设有十六个交易亭，每个交易亭有十六至二十个交易柜台，均装备有现代化办公设备和通讯设施。交易所经营对象主要为股票，其次为各种国内外债券。除节假日外，交易时间每周五天，每天五小时。自20世纪20年代起，它一直是国际金融中心，这里股票行市的暴涨与暴跌，都会在其他资本主义国家的股票市场产生连锁反应，引起波动。它还是纽约市最受欢迎的旅游名胜之一。1934年10月1日，交易所向美国证券交易委员会注册为一家全国性证券交易所，有一位主席和33位成员的董事会，1971年2月18日，非营利法人团体正式成立，董事会成员的数量减少到25位。2006年6月1日，纽约证券交易所宣布与泛欧证券交易所合并组成纽约证交所－泛欧证交所公司。1股纽约证交所的股票换成1股新公司股票，泛欧证交所股东以1股泛欧证交所股票换取新公司的0.98股股票和21.32欧元现金。新公司总部设在纽约。1953年起，成员限定为1366名。只有盈利250万美元（税前）、最低发行售出股票100万股、给普通股东以投票权并定期公布财务的公司，其股票才有资格在交易所挂牌。至1999年2月，交易所的日均交易量达亿股，交易额约达300亿美元。截至1999年2月，在交易所上市的公司已超过3000家，其中包括来自48个国家的385家外国公司，在全球资本市场上筹措资金超过10万亿。另外，美国政府、公司和外国政府、公司及国际银行的数千种债券也在交易所上市交易。",
                                   @"title4":@"1"
                             
                      
            },
                           
                           @{@"isColloct":@(YES),
                                                      @"title":@"纳斯达克证券交易所",
                                                      @"title1":@"National Association of Securities Dealers Automated Quotation",
                                                      @"title2":@"全球第一个电子交易市场",
                                                      @"title3":@"纳斯达克在传统的交易方式上通过应用当今先进的技术和信息——计算机和电讯技术术使它与其它股票市场相比独树一帜，代表着世界上最大的几家证券公司的 519位券商被称作做市商，他们在纳斯达克上提供了6万个竞买和竞卖价格。这些大范围的活动由一个庞大的计算机网络进行处理，向遍布52个国家的投资者显示其中的最优报价。（包括70多个电脑终端），纳斯达克拥有各种各样的做市商，投资者在纳斯达克市场上任何一支挂牌的股票的交易都采取公开竞争来完成——用他们的自有资本来买卖纳斯达克股票。这种竞争活动和资本提供活动使交易活跃地进行，广泛有序的市场、指令的迅速执行为大小投资者买卖股票提供了有利条件。这一切不同于拍卖市场。它有一个单独的指定交易员，或特定的人。这个人被指定负贡一种股票在这处市场上的所有交易，并负责搓全买卖双方，在必要时为了保持交易的不断进行还要充当交易者的角色。信息和服务业的兴起催生了纳斯达克。纳斯达克始建于1971年，是一个完全采用电子交易、为新兴产业提供竞争舞台、自我监管、面向全球的股票市场。纳斯达克是全美也是世界最大的股票电子交易市场。 纳斯达克（NASDAQ）股票市场是世界上主要的股票市场中成长速度最快的市场，而且它是首家电子化的股票市场。每天在美国市场上换手的股票中有超过半数的交易在纳斯达克上进行的，将近有5400家公司的证券在这个市场上挂牌。纳斯达克增大了交易市场中的优秀因素，并增强了他的交易系统，这些改进使纳斯达克有能力把投资者的指令发送到其它的电子通讯网络中去，感觉好像进入了一个拍卖市场。",
                                                      @"title4":@"2"
                                                
                                         
                               },
                           
                           @{@"isColloct":@(YES),
                                                      @"title":@"东京证券交易所",
                                                      @"title1":@"Tokyo Stock Exchange",
                                                      @"title2":@"東京証券取引所（とうきょうしょうけんとりひきじょ）",
                                                      @"title3":@"它在1878年5月15日创立，同年6月1日开始交易，创立时的名称为「东京股票交易所」（日文：东京株式取引所）。二次大战时曾暂停交易，1949年5月16日重开，并更名为东京证券交易所。东京证券交易所的交易楼层於1999年4月30日关闭，全部改为电子交易，而新的TSE Arrows（东证アローズ）於2000年5月9日启用。东京证券交易所的前身是1879年5月曾成立东京证券交易株式会社。由于当时日本经济发展缓慢，证券交易不兴旺，1943年6月，日本政府合并所有证券交易所，成立了半官方的日本证券交易所，但成立不到四年就解体了。二次大战前，日本的资本主义虽有一定的发展，但由于军国主义向外侵略，重工业、兵器工业均由国家垄断经营，纺织、海运等行业也由国家控制，这是一种战争经济体制并带有浓厚的军国主义色彩。那时，即使企业发行股票，往往也被同一财阀内部的企业所消化。因此，证券业务难以发展。日本战败后，1946年在美军占领下交易所解散。1949年1月美国同意东京证券交易所重新开业。随着日本战后经济的恢复和发展，东京证券交易所也发展繁荣起来。东京证券交易所有上市公司1777家，其中外国公司 110家，市场资本总额将近45000亿美元。长期以来，大量的公众储蓄都依赖金融机构进行间接投资，使证券交易具有整批性、数量大等特点，同时也造成发行市场狭小，仅面向少数金融机构，使股票、债券的发行市场形成抽象的无形市场，与发达的流通市场相比较落后。70年代以来，日本经济实力大增，成为世界经济强国。为适应日本经济结构和经济发展的国际化需要，日本证券市场的国际化成为必然趋势。为此，日本政府自70年代以来全面放宽外汇管制，降低税率，以鼓励外国资金进入日本证券市场，使国际资本在东京证券市场的活动日益频繁。1988年，日本政府允许外国资本在东京进入场外交易；1989年，又允许外国证券公司进入东京证券交易所，使东京证券交易所在国际上的地位大大提高",
                                                      @"title4":@"3"
                                                
                                         
                               },
                           
                           @{@"isColloct":@(YES),
                                                      @"title":@"伦敦证券交易所",
                                                      @"title1":@"London Stock Exchange",
                                                      @"title2":@"英国伦敦 证券交易，股票市场",
                                                      @"title3":@"伦敦证券交易所是世界四大证券交易所之一，作为世界上最国际化的金融中心，伦敦不仅是欧洲债券及外汇交易领域的全球领先者，还受理超过三分之二的国际股票承销业务。伦敦的规模与位置，意味着它为世界各地的公司及投资者提供了一个通往欧洲的理想门户。在保持伦敦的领先地位方面，伦敦证券交易所扮演着中心角色，它运作世界上国际最强的股票市场，其外国股票的交易超过其它任何证交所。作为世界第三大证券交易中心，伦敦证券交易所是世界上历史最悠久的证券交易所。伦敦证券交易所曾为当时英国经济的兴旺立下汗马功劳，但随着英国国内和世界经济形势的变化，其浓重的保守色彩，特别是沿袭的陈规陋习严重阻碍了英国证券市场的发展，影响厂中场竞争力，在这一形势下，伦敦证券交易所于1986年10月进行了重大改革，例如，改革固定佣金制；允许大公司直接进入交易所进行交易；放宽对会员的资格审查；允许批发商与经纪人兼营：证券交易全部实现电脑化，与纽约、东京交易所连机，实现24小时全球交易。这些改革措施使英国证券市场发生了根本性的变化，巩固了其在国际证券市场中的地位。伦敦的外国股票交易额始终高于其它市场。这反映了外国公司在伦敦证券交易所业务中的中心地位—在伦敦证券交易所交易的外国股票远远超出英国本土的股票，这种情形是独一无二的。在伦敦，外国股票的平均日交易额达到195亿美元，远远高于其它任何主要证交所。伦敦市场的规模、威望和全球操作围，意味着在这里上市和交易的外国公司可获得全球瞩目和覆盖。 伦敦证券交易所面向外国股票的交易及信息系统包括国际股票自动对盘交易系统和EAQ国际股票自动报价系统。伦敦证券交易所的国际股票自动对盘交易系统是一套由订单驱动的全自动交易设施。它有助于提高在伦敦证券交易所交易量最大的外国公司的知名度，并以其速度、透明度和效率促进这些公司股票的交易。 SEAQ国际股票自动报价系统是一套由报价驱动的基于屏幕上的交易及信息系统，已成为在伦敦上市的外国公司与其投资者之间出色的沟通渠道，并已成为该领域的衡量标准。在增进上市公司的股票在其本土市场、伦敦市场以及世界其它金融中心的流动性方面，SEAQ国际股票自动报价系统发挥着关键作用。",
                                                      @"title4":@"4"
                                                
                                         
                               },
                           
                           @{@"isColloct":@(YES),
                                                      @"title":@"香港证券交易所",
                                                      @"title1":@"Hong Kong Exchanges and Clearing Limited",
                                                      @"title2":@"香港交易及结算所有限公司",
                                                      @"title3":@"港交所（Hong Kong Exchanges and Clearing Limited）是一个独家经营香港股票市场的机构，在未得财政司司长同意下，任何个人或机构不得持有香港交易所超过5%的股份。2006年9月11日，香港交易所成为恒生指数成份股。1999年，香港特区财政司提出对香港证券及期货市场进行全面改革，以提高香港的竞争力和迎接市场全球化所带来的挑战。根据改革方案，香港联合交易所有限公司（联交所）与香港期货交易所有限公司（期交所）实行股份化并与香港中央结算有限公司（香港结算）合并，由单一控股公司香港交易所拥有。三家公司于2000年3月6日完成合并，2000年6月27日，香港交易所以介绍形式在联交所上市。2005年7月，香港交易所为使用接近20年，位于交易广场的交易大堂进行翻新，减少交易柜位，并增设多功能场地、展览馆及图书馆等设施。装修完成后的新交易大堂在2006年1月16日重新使用，并由财政司司长唐英年主持启用仪式。交易大堂的展览馆则在2006年4月26日由特区行政长官曾荫权主持揭幕仪式。交易大堂在2006年完成翻新后，设有294张交易台，交易场地面积为12200平方呎，设有传媒采访区及新闻直播室，交易大堂中央的电子显示屏幕则改用全彩色显示屏，以圆筒形设计。另增设交易所展览馆，占地13800平方呎，对外开放，向公众介绍交易所的历史及发展，成人入场费20港元。香港交易所的所有交易以电脑进行，第一代的自动对盘及成交系统于1993年11月启用，至1996年1月，第二代自动对盘及成交系统启用，系统让证券行的终端机连接，令交易不再局限于交易大堂内。第三代自动对盘及成交系统（AMS/3）于2000年启用，证券经纪可以透过开放式的连接器，将买卖盘直接输入中央处理系统进行交易，使交易更快捷，亦提高了交易所可处理的交易量。 香港交易所随着证券市场规模的扩大和交易所未来国际化发展的需要，香港交易所于2000年10月推出了第叁代自动对盘及成交系统（AMS/3）。AMS/3将投资者、交易所参与者、其他参与者及中央市场连接起来，使交易过程变得更有效率。",
                                                      @"title4":@"5"
                                                
                                         
                               },
                           
                           @{@"isColloct":@(YES),
                                                      @"title":@"巴黎证券交易所",
                                                      @"title1":@"Paris Bourse/Paris Stock Exchange",
                                                      @"title2":@"建立时间1724年法国",
                                                      @"title3":@"巴黎证券交易所是法国最大的证券交易所，1724年正式建立。证券通过银行在交易所上市，交易所内有银行代表为客户服务，但证券买卖必须通过证券经纪人进行。经纪人由财政经济部指定，1983年3月共有99人，其中巴黎71人，外省28人。证券经纪人接受客户买卖证券的委托，掌握买卖双方供求数量和要求的价格幅度，代客户进行买卖，从中收取佣金。证券价格由供求关系决定，官方牌价由交易所业务委员会听取经纪人同业公会的意见后公布。 法国从1961年开始，所有交易所实行“单一价格”。巴黎证券交易所发行全国性、国际性和外国证券，7家外省交易所经营地区证券买卖、并划分了各自的管辖区，但有时一种证券也同时在几个交易所买卖，标价统一。交易所业务委员会，1967年成立，系国家机构，主席由部长理事会任命，另加国库司的一个成员作为政府特派员参加该委员会。其职责是对有关重大问题（如建立或撤销交易所、修改现行法律章程）提出意见和建议，对一些技术性的问题作出决定。如确定期货交易所需要的保证金数额、决定佣金费率；在征求同业公会的意见后，对准许证券列入官方牌价表或注销上市的证券作出最后决定；向共和国总统递交日报和年报。巴黎证券交易所在世界各大交易所中，次于纽约、东京和伦敦，名列第四。",
                                                      @"title4":@"6"
                                                
                                         
                               },
                           @{@"isColloct":@(YES),
                                                      @"title":@"上海证券交易所",
                                                      @"title1":@"Shanghai Stock Exchange",
                                                      @"title2":@"上海市浦东新区杨高南路388号",
                                                      @"title3":@"提供证券集中交易的场所、设施和服务；制定和修改上交所业务规则；按照国务院及中国证监会规定，审核证券公开发行上市申请；审核、安排证券上市交易，决定证券终止上市和重新上市等；提供非公开发行证券转让服务；组织和监督证券交易；组织实施交易品种和交易方式创新；对会员进行监管；对证券上市交易公司及相关信息披露义务人进行监管，提供网站供信息披露义务人发布依法披露的信息；对证券服务机构为证券发行上市、交易等提供服务的行为进行监管；设立或者参与设立证券登记结算机构；管理和公布市场信息；开展投资者教育和保护；法律、行政法规规定的及中国证监会许可、授权或者委托的其他职能。上海证券交易所已发展成为拥有股票、债券、基金、衍生品四大类证券交易品种的综合型证券交易所，建立了保障上海证券市场规范有序运作的自律监管体系。 为了持续提升市场主体的用户体验，上交所以市场需求为导向，对业务规则体系架构进行了优化，形成了以股、债、基、衍四个市场为核心模块，以跨境互联互通和交易创新业务为特色模块，以章程、会员管理、纪律处分和收费事项为通用模块的全新业务规则体系。同时，经过全面清理和整合，大力精简了业务规则数量和层级划分，形成了以基本规则为主干、以规则适用指引为枝叶、以业务操作指南为补充的轻型架构，并对业务规则、指南实行分类连续编号。1990年12月，经国务院授权，由中国人民银行批准建立的上海证券交易所正式成立，这是建国以来内地的第一家证券交易所。此前的中国人，只是从茅盾的小说《子夜》及据此改编的电影中依稀对股票交易所有一点印象。开市那天，来自上海、山东、江西、安徽、浙江、海南、辽宁等地的25家证券经营机构成为交易所会员，分专业经纪商、专业自营商、监管经纪商和自营商几种。交易所采用现贷交易方式，不搞期货交易，开业初期以债券包括国债、企业债券和金融债券交易为主，同时进行股票交易，以后逐步过渡到债券和股票交易并重。",
                                                      @"title4":@"7"
                                                
                                         
                               },
                           @{@"isColloct":@(YES),
                                                      @"title":@"深圳证券交易所",
                                                      @"title1":@"Shenzhen Stock Exchange",
                                                      @"title2":@"深圳市福田区深南大道2012号",
                                                      @"title3":@"提供证券集中交易的场所、设施和服务；制定和修改本所的业务规则；审核、安排证券上市交易，决定证券暂停上市、恢复上市、终止上市和重新上市；提供非公开发行证券转让服务；组织和监督证券交易；组织实施交易品种和交易方式创新；对会员进行监管；对证券上市交易公司及相关信息披露义务人进行监管；对证券服务机构为证券上市、交易等提供服务的行为进行监管；设立或者参与设立证券登记结算机构；管理和公布市场信息；开展投资者教育和保护；法律、行政法规规定的以及中国证监会许可、授权或者委托的其他职能。我国经济发展具有多样性特点，劳动密集型、资本密集型和技术密集型产业优势并存，但都面临着由“粗放”到“集约”转型，由产业链低端向高端跃升的压力。服务于我国经济现有格局，深交所初步建立主板、中小企业板和创业板差异化发展的多层次资本市场体系，依托实体经济现实需求，固定收益等产品创新也在取得突破。创业板历经十年筹备，于2009年10月经国务院批准设立，定位于服务自主创新企业和其他成长型创业企业。截至2014年4月底，创业板上市公司379家，77%所处子行业是“十二五”规划的重点发展方向，超过43%在国际上处于子行业领先或者同步地位。板块特色逐步清晰，在新一轮以创新驱动发展的战略中，努力打造为科技与金融结合的引领者。截止2014年4月底，创业板总股本758.8亿股，总市值1.88万亿元 [8] ，在全球服务创业创新的市场中，市值仅次于NASDAQ，如NASDAQ只计算资本市场板块，则我国创业板规模位居全球第一位。从2001开始，深交所承担代办股份转让系统技术支持等工作，2006年1月增加中关村代办系统股份报价转让试点。2012年9月全国中小企业股份转让系统公司设立，深交所参股并继续提供技术支持。同时，深交所积极稳妥支持区域性股权市场规范建设，提供规则、技术、信息披露等方面咨询服务，通过多种渠道支持区域股权市场建设。",
                                                      @"title4":@"8"
                                                
                                         
                               },
                                 
                                 
                            
        ];
        
        [array writeToFile:self.incomePath atomically:YES];
    }
    
}

-(void)setupLocalDataa{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]){
//    if(YES){
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        
        
        NSMutableArray *oneUSER = [NSMutableArray array];
        NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];

        NSString *acount = @"17019547333";
        [dic1 setObject:acount forKey:@"phone"];
        [dic1 setObject:@"@kzz123456" forKey:@"psw"];
        [oneUSER addObject:dic1];
        
        [oneUSER writeToFile:self.outcomePath atomically:YES];
        
        NSMutableArray *array = [NSMutableArray array];
        
        //浆果
        NSDictionary *dict1 = @{@"type":@"百货",@"name":@"9.5成新沙发组合",@"level":@"价格：300 元",
             @"sexing":@"成色：99新",@"guoxing":@"轻微缺点",@"guojing":@"70",
             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"100g",
            @"kougan":@"沙发组合（三件套）600购入，现300出售。茶几组合（2件套）100出售。需要自取。  请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区域：丛台 - 创鑫",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"13784268035",@"person":@"联 系 人：康女士",
                                @"img":@"WechatIMG1",@"buyCount":@"105"
                                ,@"price":@"12"
                                ,@"id":@"0"
        };
        [array addObject:dict1];
        NSDictionary *dict1_2 = @{@"type":@"百货",@"name":@"沙发床",@"level":@"价 格：684 元",
             @"sexing":@"成色：90新",@"guoxing":@"完好",@"guojing":@"120",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"有购买记录 全新未用 谁要拿走 自提 已经安装好了 想的赶紧和我联系吧 当床和当沙发都可以",@"chandi":@"区域：邯山 - 明珠广场",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：王经理",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_2];
        NSDictionary *dict1_3 = @{@"type":@"百货",@"name":@"床和床垫",@"level":@"价 格：800 元",
             @"sexing":@"成色：80新",@"guoxing":@"完好",@"guojing":@"120",
             @"tangfen":@"50",@"zhuoselv":@"0.1",@"danguozhong":@"110g",
            @"kougan":@"样式好看 连带柜子一共900元.请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区域：邯山 - 明珠广场",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"9000kg",@"phone":@"13784268035",@"person":@"联 系 人：王淼",
                                  @"img":@"WechatIMG2",@"buyCount":@"210"
                                  ,@"price":@"45"
                                  ,@"id":@"2"
        };
        [array addObject:dict1_3];
        
        NSDictionary *dict1_4 = @{@"type":@"百货",@"name":@"两门柜",@"level":@"价 格：280 元",
             @"sexing":@"成色：二手",@"guoxing":@"完好",@"guojing":@"78",
             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"900g",
            @"kougan":@"好迪沙发一组280，容声小冰箱220，一米宽沙发床100，两门柜100，自家东西诚心出售，只限自提，非诚勿扰！",@"chandi":@"区域：丛台 - 龙湖公园",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"13784268035",@"person":@"联 系 人：冀莎",
                                @"img":@"WechatIMG1",@"buyCount":@"105"
                                ,@"price":@"12"
                                ,@"id":@"0"
        };
        [array addObject:dict1_4];
        NSDictionary *dict1_5 = @{@"type":@"百货",@"name":@"员工宿舍单人床",@"level":@"价格：100 元",
             @"sexing":@"成色：二手",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"低价出售各种薄厚上下铺 高低床 单人床 折叠床 实木上下床 单人床 双人床 一件也是批发价 全天津较低价 质量保证 员工宿舍 学生宿舍 工地 均有现货！有需要的亲们欢迎随时！",@"chandi":@"区域：丛台 - 丛台公园",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：王先生",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_5];
        
        
        NSDictionary *dict1_6 = @{@"type":@"百货",@"name":@"办公室内茶艺桌",@"level":@"价格：1500 元",
             @"sexing":@"成色：二手",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"老船木茶桌椅现代茶台实木家具客厅茶几办公室内茶艺桌船木家具,船木饰品,船木特色装修材料，分为茶文化、酒文化、办公、家居、户内外装饰五大系列：",@"chandi":@"区域：丛台",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：郭经理",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_6];
        
        
        NSDictionary *dict1_7 = @{@"type":@"家电",@"name":@"海信50智能电视",@"level":@"价格：750 元",
             @"sexing":@"成色：50新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"海信50智能电视 低价转让 请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区 域：丛台 - 三广",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：张先生",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_7];
        
        
        NSDictionary *dict1_8 = @{@"type":@"家电",@"name":@"三开门冰箱",@"level":@"价 格：950 元",
             @"sexing":@"成色：40新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"要搬家了，不想要了 218升三开门冰箱 还有京东79元的四年全包修服务 买的时候1399 买了1年 当办公用的",@"chandi":@"区域：邯山 - 中华南",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：王者 ",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_8];
        
        NSDictionary *dict1_9 = @{@"type":@"家电",@"name":@"格力变频空调",@"level":@"价格：2000 元",
             @"sexing":@"成色：60新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"挂机2000柜机4000打包都要可小刀 铜管 遥控器,请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区域：邯山 - 贸易路",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：张老师 ",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_9];
        
        NSDictionary *dict1_10 = @{@"type":@"家电",@"name":@"新飞小冰箱",@"level":@"价格：300 元",
             @"sexing":@"成色：70新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"新飞小冰箱，质量没问题,请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区域：高开区 - 新科园",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：李女士",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_10];
        
        NSDictionary *dict1_11 = @{@"type":@"手机",@"name":@"正品全新手机OPPO",@"level":@"服务区域：丛台  邯山 ",
             @"sexing":@"成色：80新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"OPPO手机转让一次未用，配件齐全  6g运行128g内存，可以面谈电话联系   请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：迪迪",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_11];
        
        
        NSDictionary *dict1_12 = @{@"type":@"手机",@"name":@"华为原装二手手机",@"level":@"服务区域：丛台  邯山 ",
             @"sexing":@"成色：80新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"(●)第1台：华为mate8二手手机，4+64G，麒麟950，6寸大屏，金属机身。一口价:380元 (●)第2台：华为nova3e二手手机，4+128G内存，玻璃机身，5.8寸屏幕，一口价:460元(●)第3台：华为nova3i二手手机，4+128G大内存，6寸全面屏，玻璃机身，一口价:550元",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：陈先生",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_12];

        NSDictionary *dict1_13 = @{@"type":@"手机",@"name":@"清仓处理手机苹果",@"level":@"服务区域：丛台 ",
             @"sexing":@"成色：50新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"95后新锐言情作家，行走的少女心“种草机”，以幽默细腻、暖甜治愈的写作风格见长，笔下作品时而令人捧腹连连，时而令人泪流满面，已出版长篇小说《咬定卿卿不放松》☆所有看似漫不经心的巧合，都是他想方设法的谋划；所有他辗转不成眠的时刻，她也在想他。",@"chandi":@"商家地址：丛台 -  创鑫 ",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：丽琴 ",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_13];
        
        NSDictionary *dict1_14 = @{@"type":@"手机",@"name":@"新款高配vivo",@"level":@"服务区域：丛台 ",
             @"sexing":@"成色：100新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"今年买的vivo智能手机。5G网络。拍照超清晰。平时使用很少。外观99新。是新款的，新机买来4000多。买了不到半年。有发票。配置8+128内存，自己一手使用的，送原装配件。",@"chandi":@"商家地址：邯山 -  滏阳公园 ",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：小倩",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_14];
        
        NSDictionary *dict1_15 = @{@"type":@"电车",@"name":@"二手电车",@"level":@"价   格：500 元",
             @"sexing":@"成色：30新",@"guoxing":@"完好",@"guojing":@"20",
             @"tangfen":@"30",@"zhuoselv":@"0.7",@"danguozhong":@"150g",
            @"kougan":@"本店常年出售各种能上牌的二手电车，车况好，pq跑得远，价格低， 请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。！",@"chandi":@"区域：丛台 - 丛台",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"13000kg",@"phone":@"13784268035",@"person":@"联 系 人：刘先生",
                                  @"img":@"WechatIMG3",@"buyCount":@"80"
                                  ,@"price":@"30"
                                  ,@"id":@"1"
        };
        [array addObject:dict1_15];
        
        
        
        //瓜果
        NSDictionary *dict2 = @{@"type":@"电车",@"name":@"零售电动三轮车",@"level":@"价 格：1599 元",
             @"sexing":@"成色：40新",@"guoxing":@"轻微缺点",@"guojing":@"1100",
             @"tangfen":@"60",@"zhuoselv":@"0.7",@"danguozhong":@"60g",
            @"kougan":@"三轮车好不好看看街上路上跑、昊顺（品牌）电动三轮车靠的是品质、经过7年的质量把控专造好车、让客户放心、好产品靠的是质量靠的是品质",@"chandi":@"区域：邯郸",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"13391513629",@"person":@"联 系 人：马经理",
                                @"img":@"WechatIMG4",@"buyCount":@"76"
                                ,@"price":@"58"
                                ,@"id":@"3"
        };
        [array addObject:dict2];
        
      
        
        
        //仁果
        NSDictionary *dict3 = @{@"type":@"其他",@"name":@"快递电动三轮车",@"level":@"价 格：2500 元",
             @"sexing":@"成色：50新",@"guoxing":@"轻微缺点",@"guojing":@"70",
             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"100g",
            @"kougan":@"刚刚换的62的电瓶，跑50km没问题，应为工作原因不干了现在低价出售 请您在接受平台商家提供的服务前，注意核对商家的身份并提前确认商家提供的产品/服务是否符合相关法律规定。",@"chandi":@"区域：复兴 - 建设大街",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"15101634440",@"person":@"联 系 人：付航 ",
                                @"img":@"WechatIMG8",@"buyCount":@"15"
                                ,@"price":@"150"
                                ,@"id":@"6"
        };
        [array addObject:dict3];
        NSDictionary *dict3_2 = @{@"type":@"其他",@"name":@"电瓶车二手车",@"level":@"价格：6230 元",
             @"sexing":@"成色：70新",@"guoxing":@"缺点",@"guojing":@"70",
             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"10g",
            @"kougan":@"您好，全新电动车，所有车型价格从五六千到七八千多都有的，有详细图片视频的你可以看下，都有详细图片和视频，也有部分特价车型，可以私人订制颜色车型颜色 ，有需要的话可以留言给我，若是客服没有及时回复，你也可以电联老板",@"chandi":@"区域：邯郸",
            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"230000kg",@"phone":@"15101634440",@"person":@"联 系 人：张先生",
                                  @"img":@"WechatIMG9",@"buyCount":@"300"
                                  ,@"price":@"25"
                                  ,@"id":@"7"
        };
//        [array addObject:dict3_2];
//        NSDictionary *dict3_3 = @{@"type":@"艺术",@"name":@"将门嫡女之定乾坤",@"level":@"正文语种：简体中文",
//             @"sexing":@"页数：1088",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"1. 全书由威武大将军府上的嫡女沈妙命运、身份变化为主线，讲述了一段关于天下局势的恢弘故事，尤为吸引人。 2. 故事以天下局势为大背景，集皇权故事与爱情于一身，惊心动魄中不乏细腻的情感穿插，读来让人欲罢不能。3. 精美双封，带给书友美好的阅读享受。。",@"chandi":@"出版社： 青岛出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict3_3];
//
//        NSDictionary *dict3_4 = @{@"type":@"艺术",@"name":@"他与爱同罪",@"level":@"页数：584",
//             @"sexing":@"出版时间：2018-08-01",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"北倾人气作家，热爱旅行和美食，有点小懒，对感兴趣的事格外执着，性格软萌又温暖。擅长温馨治愈系的文字，文风暖甜而清新，细微处下笔如点睛，每一个精彩的情节，每一个重要的转折，都如精火慢炖般让人品出个中滋味。已出版作品有《摇欢》《好想和你在一起》《他站在时光深处》等。",@"chandi":@"出版社： 河北出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict3_4];
//
//
//
//        NSDictionary *dict4 = @{@"type":@"动漫",@"name":@"我失去你的那一天",@"level":@"页数：1014",
//             @"sexing":@"出版时间：2018-09-01",@"guoxing":@"轻微缺点",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"100g",
//            @"kougan":@"陪伴千万人的电台主持人、畅销书作家蕊希继《只能陪你走一程》《总要习惯一个人》之后重磅新作。 生命在跟我们讨论的问题，总会随着年龄的增长而愈发深沉而难以启齿吧。仔细想想真是这样，二十五六岁以前的人生，都在迎接，而在那之后的，却尽是挥别。不知不觉地，我们都走到了要跟生命里的那些重要的人依次说再见的年纪。我们跟彼此在某个没有计划过的时间里遇见。然后，却又在不被提醒的看似一切如常的日子里，被告知失去。我们都在来的时候说好不会离开，可后来，有人离开了我们，而我们，也离开了别人。如果明天我就要失去你，那我将用这个日子里的每一口呼吸，将你牢记。",@"chandi":@"出版社： 东北文艺出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"15101634440",@"person":@"王军",
//                                @"img":@"WechatIMG8",@"buyCount":@"15"
//                                ,@"price":@"150"
//                                ,@"id":@"6"
//        };
//        [array addObject:dict4];
//        NSDictionary *dict4_2 = @{@"type":@"动漫",@"name":@"可爱多少钱一斤",@"level":@"开本：15开",
//             @"sexing":@"用纸：轻型纸",@"guoxing":@"缺点",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"10g",
//            @"kougan":@"两个人的距离一瞬间拉近，寝室楼下昏黄暗淡的灯光给他有点寡淡冷情的五官染上了一层温柔的颜色，睫毛也泛着柔软的棕色 他俯身平直盯着她，压低嗓子，声线被刻意压得又低又磁，尾音带着柔软的气音：“小栀子，你叫一声给哥哥听听，哥哥请你吃冰激凌。初栀愣愣地看着他近在咫尺的脸，眼睛微微瞪大了一点。 陆嘉珩也不动，甚至身体又往前倾了倾，鼻音含糊：“嗯？叫啊。”距离太近，初栀甚至能够感觉到他浅浅淡淡的鼻息，还有他身上好闻的味道。 这么直白赤裸、极其具有攻击性的动作，终于让她缓慢地反应过来，三秒钟后，一张白嫩的脸全红了。陆嘉珩自始至终盯着她，突然开始笑，笑声低沉缓慢，桃花眼弯起，和他以往那种寡淡又漫不经心的假笑不太一样，这次带上了真切的愉悦。",@"chandi":@"出版社： 青岛出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"230000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG9",@"buyCount":@"300"
//                                  ,@"price":@"25"
//                                  ,@"id":@"7"
//        };
//        [array addObject:dict4_2];
//        NSDictionary *dict4_3 = @{@"type":@"动漫",@"name":@"他和她的猫",@"level":@"品牌：有容书邦",
//             @"sexing":@"页数：1088",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"解说两人你一言我一语的，红色方基地水晶炸裂，游戏画面定格在最后一瞬。馆内响起传统的恭喜收尾音乐。场中大屏幕上的画面从游戏切到现场。现场灯光闪耀，双方队员摘下耳机丢在桌上。导播的镜头扫过一个个队员，到左边队伍第二个位置，刻意地停下。1. 全书由威武大将军府上的嫡女沈妙命运、身份变化为主线，讲述了一段关于天下局势的恢弘故事，尤为吸引人。 2. 故事以天下局势为大背景，集皇权故事与爱情于一身，惊心动魄中不乏细腻的情感穿插，读来让人欲罢不能。3. 精美双封，带给书友美好的阅读享受。。",@"chandi":@"出版社： 青岛出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict4_3];
//
//        NSDictionary *dict4_4 = @{@"type":@"动漫",@"name":@"别为他折腰",@"level":@"页数：1984",
//             @"sexing":@"品牌：悦读纪",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"距离江攸宁出车祸已经过了一年，她的脚还是有些跛。 被迫去相亲的沈岁和看到了身残志坚的江攸宁，以为她也是来相亲的，问她要不要跟他结婚。她鬼使神差地答应了北倾人气作家，热爱旅行和美食，有点小懒，对感兴趣的事格外执着，性格软萌又温暖。擅长温馨治愈系的文字，文风暖甜而清新，细微处下笔如点睛，每一个精彩的情节，每一个重要的转折，都如精火慢炖般让人品出个中滋味。已出版作品有《摇欢》《好想和你在一起》《他站在时光深处》等。",@"chandi":@"出版社： 河北出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict4_4];
//
//        NSDictionary *dict5 = @{@"type":@"旅游",@"name":@"地理学与生活",@"level":@"用纸：胶版纸",
//             @"sexing":@"品牌：后浪",@"guoxing":@"轻微缺点",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"100g",
//            @"kougan":@"地理学是一门古老的学科，起初仅指地球的绘图与勘查，但发展到今天已经逐渐成为一门范围广泛的学科。《地理学与生活》共十三章，分四篇，囊括自然地理学、人口地理学、文化地理学、城市地理学等分支学科。全书以介绍地理学的发展、核心概念，以及四种系统性传统为开端，四大传统独立成篇。前三篇专门介绍地理学的分支学科，而区域分析传统作为全书第四篇，通过相互参照的方式对前三种传统和主题进行综合。 相对于其他地理学书籍，本书特别突出了地理学与生活的相关性，并涉及对人体有害的天气现象、城市土地利用模式、城市垃圾与危险废物的处理等与我们生活密切相关的问题。平实、生动的文字与丰富的实例使本书可读性强，读者通过本书亦会获得一种新的思维方式。",@"chandi":@"出版社： 北京联合出版公司",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"10000kg",@"phone":@"15101634440",@"person":@"王军",
//                                @"img":@"WechatIMG8",@"buyCount":@"15"
//                                ,@"price":@"150"
//                                ,@"id":@"6"
//        };
//        [array addObject:dict5];
//        NSDictionary *dict5_2 = @{@"type":@"旅游",@"name":@"地理可以这样学",@"level":@"页数：193",
//             @"sexing":@"出版时间：2016-05-01",@"guoxing":@"缺点",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"10g",
//            @"kougan":@"这是一本以培养学生地理思维，引领学生感悟地理学习方法，提高学生学习兴趣和学习能力，提升学生地理素养为主要目标的学习方法类和教法参考类图书。本书以“尊重自然，关心社会，经历过程，形成思维，激发情感，提升素养”为理念，将课标中的重难点内容按照知识之间的逻辑关系分成30个探究专题，遵循认知规律，尊重学生的心理需求，精心设计学习探究过程，引领学生在观察、操作、思考等过程中感知、体验和感悟基本的地理概念、原理和规律，掌握认识地理事物和规律的一般方法，体验成功，提升素养。　　假如你更关注孩子的学习能力和综合素养，那就马上体验一下吧,一定会有意想不到的收获。",@"chandi":@"出版社： 中国地图出版社",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"230000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG9",@"buyCount":@"300"
//                                  ,@"price":@"25"
//                                  ,@"id":@"7"
//        };
//        [array addObject:dict5_2];
//        NSDictionary *dict5_3 = @{@"type":@"旅游",@"name":@"极简地理学",@"level":@"品牌：中信出版",
//             @"sexing":@"出版时间：2019-04-01",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"前文的故事形象地概括了地理学在日常生活中的角色，然而同时也暴露了地理学在大众的生活中的局限性。 不去谈工作社会如何发展变化，不去想人与人之间为何愈加隔绝，就让我们看看地图吧！无论是横穿美国直抵西海岸的刘易斯（Lewis）和克拉克（Clark），还是航海征服澳大利亚全境的弗林德斯（Flinders），或者是横贯非洲的利文斯通（Livingstone），这些伟大的探险家亲手绘制出地图，可不是为了让我们后人以科技为名贬低这份宝贵的遗产的。 地图是大多数人第一次接触地理学的契机。 虽然卫星导航的出现减少了人们对地图的依赖， 地图仍有一席之地。现代地理学家们根本离不开地理信息系统(GIS), 你可能也在工作日不经意间变身为地理学家。 网络上充斥着各种地图，有储存着数据的地图、显示你的朋友们（或者他们的手机）所在位置的地图、显示你的房子何时会被水淹的地图、标明离你最近的餐馆的地图……地图实际上可以显示任何东西。 地理学一直都在，它是我们所有人生活中必不可少的一部分。",@"chandi":@"出版社： 中信出版集团",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict5_3];
//
//        NSDictionary *dict5_4 = @{@"type":@"旅游",@"name":@"有趣得让人睡不着的地理",@"level":@"字数：105000",
//             @"sexing":@"品牌：北京时代",@"guoxing":@"完整",@"guojing":@"70",
//             @"tangfen":@"12",@"zhuoselv":@"0.5",@"danguozhong":@"200g",
//            @"kougan":@"左卷健男 日本法政大学生命科学学院环境应用化学系教授。1949年出生于日本枥木县，本科毕业于千叶大学教育学院，后毕业于东京学艺大学研究生院（物理化学科学教育）。在初中、高中教书二十六年后，担任京都工艺纤维大学招生中心教授，后于2004年担任同志社女子大学教授。著有《有趣得让人睡不着的物理》《有趣得让人睡不着的化学》《奇妙的化学元素全彩图鉴》《水不知道答案》等。",@"chandi":@"出版社： 北京时代华文书局",
//            @"time":@"全年",@"baozhuang":@"食用农产品",@"stock":@"19000kg",@"phone":@"15101634440",@"person":@"徐亮",
//                                  @"img":@"WechatIMG10",@"buyCount":@"230"
//                                  ,@"price":@"35"
//                                  ,@"id":@"8"
//        };
//        [array addObject:dict5_4];

        
        
        [array writeToFile:self.incomePath atomically:YES];
        
        
        NSMutableDictionary *mydict = [NSMutableDictionary dictionary];
        [mydict setObject:@"海信50智能电视" forKey:@"name"];
        [mydict setObject:acount forKey:@"phone"];
        [mydict setObject:@"河北保定市莲花池120号" forKey:@"address"];
        [mydict setObject:@"1台" forKey:@"count"];
        [self saveCaigouWithData:mydict];
    //第一次启动
    }else{
    //不是第一次启动了
    }

}
-(NSArray*)getHomeData{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];
    if (allArray==nil) {
        allArray = [NSMutableArray array];
    }
    
    array = allArray;
//
//
//    NSMutableArray *allData = [NSMutableArray array];
//    NSMutableArray *allData_1 = [NSMutableArray array];
//    NSMutableArray *allData_2 = [NSMutableArray array];
//    NSMutableArray *allData_3 = [NSMutableArray array];
//    NSMutableArray *allData_4 = [NSMutableArray array];
//    NSMutableArray *allData_5= [NSMutableArray array];
//    NSMutableArray *allData_6= [NSMutableArray array]; //其他
//
//    for (NSDictionary*dict in allArray) {
//        [allData addObject:dict]; //全部
////        if ([dict[@"name"] containsString:@"包"] ||[dict[@"name"] containsString:@"短"]||[dict[@"name"] containsString:@"长"]) {
////            [allData_1 addObject:dict];
////        }
//
////        if ([dict[@"type"] isEqualToString:@"凳子类"]) {
////            [allData_1 addObject:dict];
////        }else if ([dict[@"type"] isEqualToString:@"桌子类"]){
////            [allData_2 addObject:dict];
////        }else if ([dict[@"type"] isEqualToString:@"家居类"]){
////            [allData_3 addObject:dict];
////        }else if ([dict[@"type"] isEqualToString:@"橘果"]){
////            [allData_4 addObject:dict];
////        }else if ([dict[@"type"] isEqualToString:@"核果"]){
////            [allData_5 addObject:dict];
////        }else{
////
////        }
//    }
//    [array addObject:allData];
//    [array addObject:allData_1];
//    [array addObject:allData_2];
//    [array addObject:allData_3];
//    [array addObject:allData_4];
//    [array addObject:allData_5];
//    [array addObject:allData_6];

    return array;
}

-(void)loadPlist{
    self.incomeArray = [NSArray array];
    self.outcomeArray = [NSArray array];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"usersListShuiguo.plist"];
    self.incomePath  = plistPath;
    if ([fileManager fileExistsAtPath:plistPath] == NO) {
        NSLog(@"不存在");
    }else{
    //存在 读取plist文件  这里的plist文件存的是数组
        self.incomeArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
    }
    
    NSString *plistPath2 = [documentsPath stringByAppendingPathComponent:@"regLoginUsersList.plist"];
    self.outcomePath  = plistPath2;
    if ([fileManager fileExistsAtPath:plistPath2] == NO) {
        NSLog(@"不存在");
    }else{
    //存在 读取plist文件  这里的plist文件存的是数组
        self.outcomeArray = [NSMutableArray arrayWithContentsOfFile:plistPath2];
    }
    NSLog(@"路径%@   -    %@",self.incomePath,self.outcomePath);
    
    
    NSString *plistPath3 = [documentsPath stringByAppendingPathComponent:@"caigouUsersList.plist"];
    self.caigouPath  = plistPath3;
    if ([fileManager fileExistsAtPath:plistPath3] == NO) {
        NSLog(@"不存在");
    }else{
    //存在 读取plist文件  这里的plist文件存的是数组
//        self.outcomeArray = [NSMutableArray arrayWithContentsOfFile:plistPath2];
    }
    
    NSString *plistPath4 = [documentsPath stringByAppendingPathComponent:@"fabushuiguoUsersList.plist"];
    self.fabushuiguoPath  = plistPath4;
    if ([fileManager fileExistsAtPath:plistPath4] == NO) {
        NSLog(@"不存在");
    }else{
    //存在 读取plist文件  这里的plist文件存的是数组
//        self.outcomeArray = [NSMutableArray arrayWithContentsOfFile:plistPath2];
    }
    
//    NSLog(@"内容%@",usersArray);
}
-(void)savefabushuiguoWithData:(NSDictionary*)dict{
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.fabushuiguoPath];
    if (allArray==nil) {
        allArray = [NSMutableArray array];
    }
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]initWithDictionary:dict];
    [dic1 setObject:[self getCurrentTimestamp] forKey:@"time"];
    [allArray addObject:dic1];
    
    [allArray writeToFile:self.fabushuiguoPath atomically:YES];
}
-(NSArray*)getFabushuiguoData{
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.fabushuiguoPath];
    return allArray;
}
-(void)saveCaigouWithData:(NSDictionary*)dict{
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.caigouPath];
    if (allArray==nil) {
        allArray = [NSMutableArray array];
    }
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:dict[@"name"] forKey:@"name"];
//    [dic1 setObject:dict[@"name"] forKey:@"name"];
    [dic1 setObject:dict[@"phone"] forKey:@"phone"];
    [dic1 setObject:dict[@"count"] forKey:@"count"];
//    [dic1 setObject:dict[@"deliverType"] forKey:@"deliverType"];
    [dic1 setObject:dict[@"address"] forKey:@"address"];
    [dic1 setObject:[self getCurrentTimestamp] forKey:@"time"];
    [allArray addObject:dic1];
    
    [allArray writeToFile:self.caigouPath atomically:YES];
}
-(NSArray*)getCaigouData{
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.caigouPath];
    return allArray;
}
-(void)registUserWithData:(NSDictionary*)dict{
    NSMutableArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.outcomePath];
    if (allArray==nil) {
        allArray = [NSMutableArray array];
    }
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];

    [dic1 setObject:dict[@"phone"] forKey:@"phone"];
    [dic1 setObject:dict[@"psw"] forKey:@"psw"];
    [allArray addObject:dic1];
    
    [allArray writeToFile:self.outcomePath atomically:YES];
    
}
-(void)deleteUserWithData:(NSString*)phone{
    NSMutableArray *usersArray = [[NSMutableArray alloc ] initWithArray:self.outcomeArray];
    
    for (NSDictionary *dict in usersArray) {
        NSString *tempPhone = dict[@"phone"];
        if([tempPhone isEqualToString:phone]){
            [usersArray removeObject:dict];
        }
    }

    //写入文件
    [usersArray writeToFile:self.outcomePath atomically:YES];
    
    [self getOutcomeArray];
}


-(NSArray*)getAllRegistUser{
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.outcomePath];
    return allArray;
}



-(void)saveIncomeWithData:(NSDictionary*)income{
    NSMutableArray *usersArray = [[NSMutableArray alloc ] initWithArray:self.incomeArray];
//    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];
//    [dic1 setObject:[self getCurrentTimestamp] forKey:@"time"];
//    [dic1 setObject:income[@"money"] forKey:@"money"];
//    [dic1 setObject:income[@"desc"] forKey:@"desc"];
//
//    NSLog(@"saveIncomeWithData-%@",dic1);
    [usersArray insertObject:income atIndex:0];
    
    //写入文件
    [usersArray writeToFile:self.incomePath atomically:YES];
    [self getIncomeArray];
}
-(void)saveOutcomeWithData:(NSDictionary*)outcome{
    NSMutableArray *usersArray = [[NSMutableArray alloc ] initWithArray:self.outcomeArray];
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:[self getCurrentTimestamp] forKey:@"time"];
    [dic1 setObject:outcome[@"money"] forKey:@"money"];
    [dic1 setObject:outcome[@"desc"] forKey:@"desc"];
    
    NSLog(@"saveIncomeWithData-%@",dic1);
    [usersArray addObject:dic1];
    
    //写入文件
    [usersArray writeToFile:self.outcomePath atomically:YES];
    
    [self getOutcomeArray];
}
-(void)saveMoneyWithData:(NSDictionary*)dict{
    NSMutableArray *usersArray = [[NSMutableArray alloc ] initWithArray:self.incomeArray];
    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]init];
    [dic1 setObject:[self getCurrentTimestamp] forKey:@"time"];
    [dic1 setObject:dict[@"money"] forKey:@"money"];
//    [dic1 setObject:dict[@"desc"] forKey:@"desc"];
//    [dic1 setObject:dict[@"type"] forKey:@"type"];
//    [dic1 setObject:dict[@"category"] forKey:@"category"];
    
    NSLog(@"saveMoneyWithData-%@",dic1);
    [usersArray addObject:dic1];
    
    //写入文件
    [usersArray writeToFile:self.incomePath atomically:YES];
    [self getIncomeArray];
}




-(void)loadData{

    NSDate *currentDate = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *currentDateStr = [self.dateFormatter stringFromDate:currentDate];
    
    NSDate * date = [NSDate date];
   //一周的秒数
   NSTimeInterval time = 7 * 24 * 60 * 60;
   //下周就把"-"去掉
   NSDate *lastWeek = [date dateByAddingTimeInterval:-time];
   NSString *startDate =  [self.dateFormatter stringFromDate:lastWeek];
    
    NSLog(@"%@-----%@",currentDateStr,startDate);
    
    
    
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];
    
    NSMutableArray *addCount = [NSMutableArray array];
    
    //1、筛选所有日期
    NSMutableArray *dateArray = [NSMutableArray array];
    for (NSDictionary *dict in allArray) {
        NSString *date = [self ConvertStrToTime:dict[@"time"]];
        NSLog(@"%@-----%@",date,dict[@"time"]);
        if (![dateArray containsObject:date]) {
            [dateArray addObject:date];
        }
        [addCount addObject:dict];
    }
    
    //2、遍历日期里面的数据
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSString *dateStr in dateArray) {
        NSMutableArray *dayArray  = [NSMutableArray array];
        for (NSDictionary *dict in allArray) {
            NSString *date = [self ConvertStrToTime:dict[@"time"]];
            if ([dateStr isEqualToString:date]) {
                [dayArray addObject:dict];
            }
        }
        [finalArray addObject:dayArray];
    }
    
    //3、倒叙取值
    [finalArray reverse];
    
//    [self getDateYearMonth];
    
}

-(NSArray*)getIncomeArray{
    self.incomeArray =   [NSMutableArray arrayWithContentsOfFile:self.incomePath];
    return self.incomeArray ;
}
-(NSArray*)getOutcomeArray{
    self.outcomeArray =   [NSMutableArray arrayWithContentsOfFile:self.outcomePath];
    return self.outcomeArray ;
}
-(NSArray*)getHomeArray{
    
    NSDate *currentDate = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *currentDateStr = [self.dateFormatter stringFromDate:currentDate];
    
    NSDate * date = [NSDate date];
   //一周的秒数
   NSTimeInterval time = 7 * 24 * 60 * 60;
   //下周就把"-"去掉
   NSDate *lastWeek = [date dateByAddingTimeInterval:-time];
   NSString *startDate =  [self.dateFormatter stringFromDate:lastWeek];
    
    NSLog(@"%@-----%@",currentDateStr,startDate);
    
    
    
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];

    NSMutableArray *addCountArray = [NSMutableArray array];
    CGFloat allOutMoney = 0;
    CGFloat allInMoney = 0;
    
    //1、筛选所有日期
    NSMutableArray *dateArray = [NSMutableArray array];
    for (NSDictionary *dict in allArray) {
        NSString *date = [self ConvertStrToTime:dict[@"time"]];
        NSDate *itemDate = [self.dateFormatter dateFromString:date];
        
        if ([self date:itemDate isBetweenDate:lastWeek andDate:currentDate]) {
            NSLog(@"%@-----%@",date,dict[@"time"]);
            if (![dateArray containsObject:date]) {
                [dateArray addObject:date];
            }
            [addCountArray addObject:dict];
        }
       
    }
    
    //2、遍历日期里面的数据
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSString *dateStr in dateArray) {
        NSMutableArray *dayDataArray  = [NSMutableArray array];
        for (NSDictionary *dict in allArray) {
            NSString *date = [self ConvertStrToTime:dict[@"time"]];
            if ([dateStr isEqualToString:date]) {
                [dayDataArray addObject:dict];
                
                NSString *tpye = [NSString stringWithFormat:@"%@",dict[@"type"]];
                CGFloat money = [dict[@"money"] floatValue];
                if ([tpye isEqualToString:@"0"]) {
                    allOutMoney = allOutMoney + money;
                }else{
                    allInMoney = allInMoney + money;
                }
         
                
            }
        }
        //倒序取值
        [dayDataArray reverse];
        [finalArray addObject:dayDataArray];
    }
    
    //3、倒叙取值
    [finalArray reverse];
    [dateArray reverse];
    
    
    //获取日期
    
    NSMutableArray *dateStrArray = [NSMutableArray array];
    for (NSString *string in dateArray) {
        NSDate *data = [self.dateFormatter dateFromString:string];
        NSString *zhouStr = [self weekStringFromDate:data];
        NSString *finalDateStr = [NSString stringWithFormat:@"%@  %@",string,zhouStr];
        [dateStrArray addObject:finalDateStr];
    }
    
    
    
    NSString *weekCount = [NSString stringWithFormat:@"%lu",(unsigned long)addCountArray.count];
    
    return @[finalArray,
             weekCount,
             dateStrArray,
             [NSString stringWithFormat:@"%f",allOutMoney],
             [NSString stringWithFormat:@"%f",allInMoney],
    
    ] ;
}



-(NSArray*)getBillArrayWithTimeSort:(BOOL)isNormalTime year:(NSString*)year month:(NSString*)month{
    NSString *sortStr;
    if ([month integerValue]<10) {
        sortStr = [NSString stringWithFormat:@"%@-0%@",year,month];
    }else{
        sortStr = [NSString stringWithFormat:@"%@-%@",year,month];
    }
    NSLog(@"sortStr-%@",sortStr);

    
    NSDate *currentDate = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *currentDateStr = [self.dateFormatter stringFromDate:currentDate];
    
    NSDate * date = [NSDate date];
   //一周的秒数
   NSTimeInterval time = 7 * 24 * 60 * 60;
   //下周就把"-"去掉
   NSDate *lastWeek = [date dateByAddingTimeInterval:-time];
   NSString *startDate =  [self.dateFormatter stringFromDate:lastWeek];
    
    NSLog(@"%@-----%@",currentDateStr,startDate);
    
    
    
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];

    NSMutableArray *addCountArray = [NSMutableArray array];
    CGFloat allOutMoney = 0;
    CGFloat allInMoney = 0;
    
    //1、筛选所有日期
    NSMutableArray *dateArray = [NSMutableArray array];
    for (NSDictionary *dict in allArray) {
        NSString *date = [self ConvertStrToTime:dict[@"time"]];
        NSLog(@"%@-----%@",date,dict[@"time"]);
        
        
        if ([date containsString:sortStr]) {  //核心 -日期筛选
            if (![dateArray containsObject:date]) {
                [dateArray addObject:date];
            }
            [addCountArray addObject:dict];
        }
        
      
    }
    
    //2、遍历日期里面的数据
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSString *dateStr in dateArray) {
        NSMutableArray *dayDataArray  = [NSMutableArray array];
        for (NSDictionary *dict in allArray) {
            NSString *date = [self ConvertStrToTime:dict[@"time"]];
            if ([dateStr isEqualToString:date]) {
                [dayDataArray addObject:dict];
                
                NSString *tpye = [NSString stringWithFormat:@"%@",dict[@"type"]];
                CGFloat money = [dict[@"money"] floatValue];
                if ([tpye isEqualToString:@"0"]) {
                    allOutMoney = allOutMoney + money;
                }else{
                    allInMoney = allInMoney + money;
                }
         
                
            }
        }
        //倒序取值
        [dayDataArray reverse];
        [finalArray addObject:dayDataArray];
    }
    
    //3、倒叙取值
    if (isNormalTime) {
        [finalArray reverse];
        [dateArray reverse];
    }
    
    
    
    //获取日期
    
    NSMutableArray *dateStrArray = [NSMutableArray array];
    for (NSString *string in dateArray) {
        NSDate *data = [self.dateFormatter dateFromString:string];
        NSString *zhouStr = [self weekStringFromDate:data];
        NSString *finalDateStr = [NSString stringWithFormat:@"%@  %@",string,zhouStr];
        [dateStrArray addObject:finalDateStr];
    }
    
    
    
    NSString *weekCount = [NSString stringWithFormat:@"%lu",(unsigned long)addCountArray.count];
    
    return @[finalArray,
             weekCount,
             dateStrArray,
             [NSString stringWithFormat:@"%f",allOutMoney],
             [NSString stringWithFormat:@"%f",allInMoney],
    
    ] ;
}
-(NSArray*)getMineArray{
    NSDate *currentDate = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *currentDateStr = [self.dateFormatter stringFromDate:currentDate];
    
    NSDate * date = [NSDate date];
   //一周的秒数
   NSTimeInterval time = 7 * 24 * 60 * 60;
   //下周就把"-"去掉
   NSDate *lastWeek = [date dateByAddingTimeInterval:-time];
   NSString *startDate =  [self.dateFormatter stringFromDate:lastWeek];
    
    NSLog(@"%@-----%@",currentDateStr,startDate);
    
    
    
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];

    NSMutableArray *addCountArray = [NSMutableArray array];
    CGFloat allOutMoney = 0;
    CGFloat allInMoney = 0;
    
    //1、筛选所有日期
    NSMutableArray *dateArray = [NSMutableArray array];
    for (NSDictionary *dict in allArray) {
        NSString *date = [self ConvertStrToTime:dict[@"time"]];
        NSLog(@"%@-----%@",date,dict[@"time"]);
        if (![dateArray containsObject:date]) {
            [dateArray addObject:date];
        }
        [addCountArray addObject:dict];
    }
    
    //2、遍历日期里面的数据
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSString *dateStr in dateArray) {
        NSMutableArray *dayDataArray  = [NSMutableArray array];
        for (NSDictionary *dict in allArray) {
            NSString *date = [self ConvertStrToTime:dict[@"time"]];
            if ([dateStr isEqualToString:date]) {
                [dayDataArray addObject:dict];
                
                NSString *tpye = [NSString stringWithFormat:@"%@",dict[@"type"]];
                CGFloat money = [dict[@"money"] floatValue];
                if ([tpye isEqualToString:@"0"]) {
                    allOutMoney = allOutMoney + money;
                }else{
                    allInMoney = allInMoney + money;
                }
         
                
            }
        }
        //倒序取值
        [dayDataArray reverse];
        [finalArray addObject:dayDataArray];
    }
    
    //3、倒叙取值
    [finalArray reverse];
    [dateArray reverse];
    
    
    //获取日期
    
    NSMutableArray *dateStrArray = [NSMutableArray array];
    for (NSString *string in dateArray) {
        NSDate *data = [self.dateFormatter dateFromString:string];
        NSString *zhouStr = [self weekStringFromDate:data];
        NSString *finalDateStr = [NSString stringWithFormat:@"%@  %@",string,zhouStr];
        [dateStrArray addObject:finalDateStr];
    }
    
    NSString *dateArrayCountlianxu = [NSString stringWithFormat:@"%lu",(unsigned long)dateArray.count-1];
    if (dateArray.count==0) {
        dateArrayCountlianxu = @"0";
    }
    NSString *dateArrayCount = [NSString stringWithFormat:@"%lu",(unsigned long)dateArray.count];
    NSString *weekCount = [NSString stringWithFormat:@"%lu",(unsigned long)addCountArray.count];
    
    return @[dateArrayCountlianxu,
             dateArrayCount,
             weekCount,

    ] ;
}

-(NSArray*)getCofigureOutComeAndIncomeWithIsMonth:(BOOL)isMonth{
    NSDate *currentDate = [NSDate date];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *currentDateStr = [self.dateFormatter stringFromDate:currentDate];
    
    NSDate * date = [NSDate date];
   //一周的秒数
    NSTimeInterval time;
    if (isMonth) {
        time  = 30 * (24 * 60 * 60);
    }else{
        time  = 365 * (24 * 60 * 60);
    }
    
   //下周就把"-"去掉
   NSDate *lastWeek = [date dateByAddingTimeInterval:-time];
   NSString *startDate =  [self.dateFormatter stringFromDate:lastWeek];
    
    NSLog(@"%@-----%@",currentDateStr,startDate);
    
    
    
    NSArray *allArray = [NSMutableArray arrayWithContentsOfFile:self.incomePath];

    NSMutableArray *addCountArray = [NSMutableArray array];
    CGFloat allOutMoney = 0;
    CGFloat allInMoney = 0;
    
    //1、筛选所有日期
    NSMutableArray *dateArray = [NSMutableArray array];
    for (NSDictionary *dict in allArray) {
        NSString *date = [self ConvertStrToTime:dict[@"time"]];
        NSDate *itemDate = [self.dateFormatter dateFromString:date];
        
        if ([self date:itemDate isBetweenDate:lastWeek andDate:currentDate]) {
            NSLog(@"%@-----%@",date,dict[@"time"]);
            if (![dateArray containsObject:date]) {
                [dateArray addObject:date];
            }
            [addCountArray addObject:dict];
        }
       
    }
    
    //2、遍历日期里面的数据
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSString *dateStr in dateArray) {
        NSMutableArray *dayDataArray  = [NSMutableArray array];
        for (NSDictionary *dict in allArray) {
            NSString *date = [self ConvertStrToTime:dict[@"time"]];
            if ([dateStr isEqualToString:date]) {
                [dayDataArray addObject:dict];
                
                NSString *tpye = [NSString stringWithFormat:@"%@",dict[@"type"]];
                CGFloat money = [dict[@"money"] floatValue];
                if ([tpye isEqualToString:@"0"]) {
                    allOutMoney = allOutMoney + money;
                }else{
                    allInMoney = allInMoney + money;
                }
         
                
            }
        }
        //倒序取值
        [dayDataArray reverse];
        [finalArray addObject:dayDataArray];
    }
    
    //3、倒叙取值
    [finalArray reverse];
    [dateArray reverse];
    
    
    //获取日期
    
    NSMutableArray *dateStrArray = [NSMutableArray array];
    for (NSString *string in dateArray) {
        NSDate *data = [self.dateFormatter dateFromString:string];
        NSString *zhouStr = [self weekStringFromDate:data];
        NSString *finalDateStr = [NSString stringWithFormat:@"%@  %@",string,zhouStr];
        [dateStrArray addObject:finalDateStr];
    }
    
    
    
    NSString *weekCount = [NSString stringWithFormat:@"%lu",(unsigned long)addCountArray.count];
    

    return @[[NSString stringWithFormat:@"%.2f",allOutMoney],[NSString stringWithFormat:@"%.2f",allInMoney]];
}

- (NSString *)getDateMonth {

    

    NSDate *newDate = [NSDate date];

    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth;

    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:newDate];

    NSInteger year = [dateComponent year];

    NSInteger month = [dateComponent month];

    return [NSString stringWithFormat:@"%ld",month];

}
- (NSString *)getDateYear {

    

    NSDate *newDate = [NSDate date];

    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth;

    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:newDate];

    NSInteger year = [dateComponent year];

    NSInteger month = [dateComponent month];

    return [NSString stringWithFormat:@"%ld",year];

}


-(NSString *)weekStringFromDate:(NSDate *)date{

    

    NSArray *weeks=@[[NSNull null],@"星期日",@"星期一",@"星期二",@"星期三",@"星期四",@"星期五",@"星期六"];

    NSCalendar *calendar=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];

    NSTimeZone *timeZone=[[NSTimeZone alloc]initWithName:@"Asia/Beijing"];

    [calendar setTimeZone:timeZone];

    NSCalendarUnit calendarUnit=NSWeekdayCalendarUnit;

    NSDateComponents *components=[calendar components:calendarUnit fromDate:date];

    return [weeks objectAtIndex:components.weekday];

}



//时间戳变为格式时间
- (NSString *)ConvertStrToTime:(NSString *)timeStr

{

    long long time=[timeStr longLongValue];
    //    如果服务器返回的是13位字符串，需要除以1000，否则显示不正确(13位其实代表的是毫秒，需要除以1000)
    //    long long time=[timeStr longLongValue] / 1000;

    NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:time];

    NSString*timeString=[self.dateFormatter stringFromDate:date];

    return timeString;

}


+ (NSString *)timestampSwitchTime:(NSInteger)timestamp andFormatter:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    if ([format isEqual:@"HH:mm:ss"] && confromTimespStr.length<8) {
        confromTimespStr = @"00:00:00";
    }
    if ([format isEqual:@"YYYY-MM-dd HH:mm:ss"] && confromTimespStr.length<18) {
        confromTimespStr = @"2000-01-01 00:00:00"; //默认返回
    }
    return confromTimespStr;
}

-(NSDate *)dateFromString:(NSString *)datestring


{

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;

    NSDateFormatter *format=[[NSDateFormatter alloc] init];

    [format setDateFormat:@"yyyy-MM-dd"];

    NSDate *fromdate=[format dateFromString:datestring];

    return fromdate;

    

}

- (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate

{

    if ([date compare:beginDate] == NSOrderedAscending)

        return NO;

    

    if ([date compare:endDate] == NSOrderedDescending)

        return NO;

    

    return YES;

}

-(NSArray *)adddDataOnViews
{
    NSDate *nowDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitDay fromDate:nowDate];
    // 获取今天是周几
    NSInteger weekDay = [comp weekday];
    // 获取几天是几号
    NSInteger day = [comp day];
    // 计算当前日期和本周的星期一和星期天相差天数
    long firstDiff,lastDiff;
    // weekDay = 1;
    if (weekDay == 1)
    {
        firstDiff = -6;
        lastDiff = 0;
    }
    else
    {
        firstDiff = [calendar firstWeekday] - weekDay + 1;
        lastDiff = 8 - weekDay;
    }
    // NSLog(@"firstDiff: %ld lastDiff: %ld",firstDiff,lastDiff);
    // 在当前日期(去掉时分秒)基础上加上差的天
    NSDateComponents *firstDayComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:nowDate];
    [firstDayComp setDay:day + firstDiff];
    NSDate *firstDayOfWeek = [calendar dateFromComponents:firstDayComp];
    NSDateComponents *lastDayComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:nowDate];
    [lastDayComp setDay:day + lastDiff];
    NSDate *lastDayOfWeek = [calendar dateFromComponents:lastDayComp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    NSString *firstDay = [formatter stringFromDate:firstDayOfWeek];
    NSString *lastDay = [formatter stringFromDate:lastDayOfWeek];
     NSLog(@"%@=======%@",firstDay,lastDay);
    
    int firstValue = firstDay.intValue;
    int lastValue = lastDay.intValue;
    
    NSMutableArray *dateArr = [[NSMutableArray alloc]init];
    if (firstValue < lastValue) {
        
        for (int j = 0; j<7; j++) {
            NSString *obj = [NSString stringWithFormat:@"%d",firstValue+j];
            [dateArr addObject:obj];
        }
    }
    else if (firstValue > lastValue)
    {
        for (int j = 0; j < 7-lastValue; j++) {
            NSString *obj = [NSString stringWithFormat:@"%d",firstValue+j];
            [dateArr addObject:obj];
        }
        for (int z = 0; z<lastValue; z++) {

            NSString *obj = [NSString stringWithFormat:@"%d",z+1];
            [dateArr addObject:obj];
        }
    }
    return dateArr;

}

//获取当前时间戳
- (NSTimeInterval)currentTimeStr {
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
    return time;
}

- (NSTimeInterval)calculateTheTimeInterval:(NSDate *)startTime {
    NSTimeInterval seconds = [startTime timeIntervalSinceDate:[NSDate date]];
    return seconds;
}

- (NSTimeInterval)calculatedIntervalWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    NSTimeInterval seconds = [endDate timeIntervalSinceDate:[NSDate date]];
    return seconds;
}
/// 根据时间戳获取时间
- (NSDate *)dateWithTimeInterval:(NSTimeInterval)timeInterval {
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeInterval / 1000.0];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:confromTimesp];
    NSDate *localeDate = [confromTimesp  dateByAddingTimeInterval: interval];
    return localeDate;
}



//时间显示内容
-(NSString *)getDateDisplayString:(long long) miliSeconds{
    
    //获取当前时间戳
    NSTimeInterval currentTime = [self currentTimeStr];
    
    NSTimeInterval tempMilli = miliSeconds;
    NSTimeInterval seconds = tempMilli/1000.0;
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    
    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc ] init ];
    
    //2. 指定日历对象,要去取日期对象的那些部分.
    NSDateComponents *comp =  [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday fromDate:myDate];
    
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd hh:mm";
    } else if (nowCmps.month != myCmps.month) {
        NSInteger datys = unit + myCmps.day;
        if ((datys - nowCmps.day) == 1) {
            dateFmt.dateFormat = @"明天 HH:mm";
        } else if ((datys - nowCmps.day) == 2) {
            dateFmt.dateFormat = @"后天 HH:mm";
        } else {
            switch (comp.weekday) {
                case 1:
                    dateFmt.dateFormat = @"MM-dd(周日) HH:mm";
                    break;
                case 2:
                    dateFmt.dateFormat = @"MM-dd(周一) HH:mm";
                    break;
                case 3:
                    dateFmt.dateFormat = @"MM-dd(周二) HH:mm";
                    break;
                case 4:
                    dateFmt.dateFormat = @"MM-dd(周三) HH:mm";
                    break;
                case 5:
                    dateFmt.dateFormat = @"MM-dd(周四) HH:mm";
                    break;
                case 6:
                    dateFmt.dateFormat = @"MM-dd(周五) HH:mm";
                    break;
                case 7:
                    dateFmt.dateFormat = @"MM-dd(周六) HH:mm";
                    break;
                default:
                    break;
            }
        }
        
    } else {
        if (nowCmps.day==myCmps.day) {

            dateFmt.dateFormat = @"今天 HH:mm";
            
        } else if((nowCmps.day-myCmps.day)==1) {

            dateFmt.dateFormat = @"昨天 HH:mm";
            
        } else if((myCmps.day-nowCmps.day)==1) {

            dateFmt.dateFormat = @"明天 HH:mm";
            
        } else if((myCmps.day-nowCmps.day)==2) {
            
            dateFmt.dateFormat = @"后天 HH:mm";
            
        } else {

            switch (comp.weekday) {
                case 1:
                    dateFmt.dateFormat = @"MM-dd(周日) HH:mm";
                    break;
                case 2:
                    dateFmt.dateFormat = @"MM-dd(周一) HH:mm";
                    break;
                case 3:
                    dateFmt.dateFormat = @"MM-dd(周二) HH:mm";
                    break;
                case 4:
                    dateFmt.dateFormat = @"MM-dd(周三) HH:mm";
                    break;
                case 5:
                    dateFmt.dateFormat = @"MM-dd(周四) HH:mm";
                    break;
                case 6:
                    dateFmt.dateFormat = @"MM-dd(周五) HH:mm";
                    break;
                case 7:
                    dateFmt.dateFormat = @"MM-dd(周六) HH:mm";
                    break;
                default:
                    break;
            }
        }
    }
    return [dateFmt stringFromDate:myDate];
}

#pragma mark ---- 字符 or 时间

+ (NSString *)stringWithDate:(NSTimeInterval)time {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    /*
     yyyy-MM-dd HH:mm:ss.SSS
     yyyy-MM-dd HH:mm:ss
     yyyy-MM-dd
     MM dd yyyy
     */
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time / 1000];
    return [formatter stringFromDate:timeDate];
}

+ (NSString *)stringWithDate:(NSTimeInterval)time dateFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:dateFormat];
    /*
     yyyy-MM-dd HH:mm:ss.SSS
     yyyy-MM-dd HH:mm:ss
     yyyy-MM-dd
     MM dd yyyy
     */
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time / 1000];
    return [formatter stringFromDate:timeDate];
}

+ (NSString *)stringWithDay:(NSTimeInterval)time {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM/dd"];
    /*
     yyyy-MM-dd HH:mm:ss.SSS
     yyyy-MM-dd HH:mm:ss
     yyyy-MM-dd
     MM dd yyyy
     */
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:time / 1000];
    return [formatter stringFromDate:timeDate];
}

+ (NSString *)stringWithHour:(NSTimeInterval)time
{
    int seconds = (int)time % 60;
    int minutes = ((int)time / 60) % 60;
    int hours = (int)time / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (NSString*)getCurrentTimestamp{
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    return timeString;
    
}
//json格式字符串转字典：
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
+ (NSString *)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startD =[date dateFromString:startTime];
    NSDate *endD = [date dateFromString:endTime];
    NSTimeInterval start = [startD timeIntervalSince1970]*1;
    NSTimeInterval end = [endD timeIntervalSince1970]*1;
    NSTimeInterval value = end - start;
    int minute = (int)value /60%60;
    int house = (int)value / (24 * 3600)%3600;
    int sum = house * 60 + minute + 1;
    NSString *str = [NSString stringWithFormat:@"%d",sum];
    return str;
}
//获取当前的时间
-(NSString*)getCurrentTimes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}
- (NSArray *)dateStringAfterlocalDateForYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day Hour:(NSInteger)hour Minute:(NSInteger)minute Second:(NSInteger)second{
    NSDate *localDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:year];
    [comps setMonth:month];
    [comps setDay:day];
    [comps setHour:hour];
    [comps setMinute:minute];
    [comps setSecond:second];
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *minDate = [calender dateByAddingComponents:comps toDate:localDate options:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH"];
   NSDateComponents *components = [calender components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour fromDate:minDate];
   NSInteger thisYear=[components year];
   NSInteger thisMonth=[components month];
   NSInteger thisDay=[components day];
   NSInteger thisHour=[components hour];
   NSString *DateTime = [NSString stringWithFormat:@"%ld-%ld-%ld-%ld",(long)thisYear,(long)thisMonth,(long)thisDay,(long)thisHour];
   NSArray *array = [DateTime componentsSeparatedByString:@"-"];
   return array;
}


-(NSString *)getNowTimeTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    return timeSp;
    
}



-(NSString *)getNowTimeTimestamp2{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    
    return timeString;
    
}

//获取当前时间戳  （以毫秒为单位）

-(NSString *)getNowTimeTimestamp3{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
    
}

-(NSArray*)getNewData{
    self.incomeArray =   [NSMutableArray arrayWithContentsOfFile:self.incomePath];
    return self.incomeArray ;
}
@end
