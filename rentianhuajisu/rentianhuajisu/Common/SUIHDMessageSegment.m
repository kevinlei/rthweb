//
//  ZKCustomSegement.m
//  TeacherKudou
//
//  Created by 赵坪生 on 2016/12/23.
//  Copyright © 2016年 赵坪生. All rights reserved.
//

#import "SUIHDMessageSegment.h"
#define kButtonTag 100
#define kLineViewTag 200
#define kSegementHeight 44
@interface SUIHDMessageSegment ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)NSArray *viewArray;
@property (nonatomic, strong)CurrentIndex indexBlock;
//@property (nonatomic, strong)FAKIcon *buttonImage;
@end

@implementation SUIHDMessageSegment

+ (SUIHDMessageSegment *)createSegementViews:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles{
    SUIHDMessageSegment *segement = [SUIHDMessageSegment new];
    //    segement.backgroundColor = [ZKColor getColor:kTableViewBackGroundColor];
    segement.frame = CGRectMake(0, 0, kScreenWidth, 64);
    segement.viewArray = views;
    segement.indexBlock = index;
    [segement setupUIWithtitles:titles];
    return segement;
}
+ (SUIHDMessageSegment *)createSegementWithFrame:(CGRect)frame views:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles buttonImage:(UIImage *)image{
    SUIHDMessageSegment *segement = [SUIHDMessageSegment new];
    segement.backgroundColor = [UIColor whiteColor];
    segement.frame = frame;
    //segement.buttonImage = image;
    segement.viewArray = views;
    segement.indexBlock = index;
    [segement setupUIWithtitles:titles];
    return segement;
}


- (void)setupUIWithtitles:(NSArray *)titles{
    for (int i = 0; i < titles.count; i ++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitle:titles[i] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        button.tag = kButtonTag + i;
        //        [button setTitleColor:[ZKColor getColor:i == 0 ? kNavigationBar_BackGroundColor:kFontcolor_777777] forState:UIControlStateNormal];
        //        if (self.buttonImage) {
        //            [self.buttonImage addAttribute:NSForegroundColorAttributeName value:[ZKColor getColor:i == 0 ? kNavigationBar_BackGroundColor:kFontcolor_777777]];
        //            [button setImage:[self.buttonImage imageWithSize:CGSizeMake(20, 20)] forState:UIControlStateNormal];
        //        }
        if (i == 0) {
            [button setTitleColor: [UIColor colorWithHexString:Background_Color] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
            [button setBackgroundColor:[UIColor whiteColor]];
            button.titleLabel.font = [UIFont systemFontOfSize:22 weight:UIFontWeightMedium];
        }else{
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor whiteColor]];
            button.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
        }
        [button addTarget:self action:@selector(buttonaction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(kScreenWidth/titles.count* i);
            make.top.equalTo(self);
            make.width.mas_offset(kScreenWidth /titles.count);
            make.height.equalTo(@(kSegementHeight));
        }];
        
                UIView *lineView = [UIView new];
        lineView.layer.masksToBounds = YES;
        lineView.layer.cornerRadius = 3;
                if (i >= 1) {
                    lineView.hidden = YES;
                }
                lineView.tag = kLineViewTag + i;
//                lineView.backgroundColor = [UIColor colorWithHexString:@"#401AC4"];
                lineView.backgroundColor = [UIColor colorWithHexString:Background_Color];
                [button addSubview:lineView];
                [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(button);
                    make.height.mas_equalTo(@2);
                    make.width.mas_equalTo(40);
//                    make.width.equalTo(button).multipliedBy(0.8);
                    make.bottom.equalTo(button).offset(-0.5);
                }];
    }
    
//        UIView *bottomLineView = [UIView new];
//    //    bottomLineView.hidden = YES;
//        bottomLineView.backgroundColor = [UIColor colorWithHexString:@"#DDDDDD"];
//        [self addSubview:bottomLineView];
//        [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(self);
//            make.height.equalTo(@0.5);
//            make.top.equalTo(@(kSegementHeight - 0.5));
//        }];
//
//        UIView *topLineView = [UIView new];
////        topLineView.hidden = YES;
//        topLineView.backgroundColor = [UIColor colorWithHexString:@"#DDDDDD"];
//        [self addSubview:topLineView];
//        [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(self);
//            make.height.equalTo(@1);
//            make.top.equalTo(self);
//        }];
    
//        for (int i = 1; i < titles.count; i++) {
//            UIView *lineView = [UIView new];
//            lineView.backgroundColor = bottomLineView.backgroundColor;
//            [self addSubview:lineView];
//            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(self).offset(kScreenWidth / titles.count * i);
//                make.top.equalTo(self).offset(8);
//                make.bottom.equalTo(bottomLineView).offset(-8);
//                make.width.equalTo(@0.5);
//            }];
//        }
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSegementHeight, kScreenWidth, kScreenHeight - kSegementHeight)];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth * self.viewArray.count, self.scrollView.frame.size.width);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.alwaysBounceHorizontal = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    [self addSubview:self.scrollView];
    for (int i = 0; i < self.viewArray.count; i++) {
        ((UIView *)(self.viewArray[i])).frame = CGRectMake(self.scrollView.frame.size.width * i, 0, self.scrollView.frame.size.width, kScreenHeight);
        [self.scrollView addSubview:self.viewArray[i]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat offest = scrollView.contentOffset.x;
    NSInteger index = offest / kScreenWidth;
    [self setButtonStateWithIndex:index];
    if (self.indexBlock) {
        self.indexBlock(index);
    }
}

- (void)buttonaction:(UIButton *)button{
    NSInteger buttonTag = button.tag - kButtonTag;
    [self setButtonStateWithIndex:buttonTag];
    if (self.indexBlock) {
        self.indexBlock(buttonTag);
    }
    [self.scrollView setContentOffset:CGPointMake(kScreenWidth * buttonTag, 0) animated:YES];
}

- (void)setButtonStateWithIndex:(NSInteger)index{
    for (int i = 0; i < self.viewArray.count; i++) {
        UIButton * button = [self viewWithTag:kButtonTag + i];
                UIView * view = [button viewWithTag:kLineViewTag + i];
        if (i == index) {
            [button setTitleColor: [UIColor colorWithHexString:Background_Color] forState:UIControlStateNormal];
//            button.backgroundColor = [UIColor whiteColor];
            [button setBackgroundColor:[UIColor whiteColor]];
            button.titleLabel.font = [UIFont systemFontOfSize:22 weight:UIFontWeightMedium];
        }else{
            [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//            button.backgroundColor = [UIColor colorWithHexString:@"#333333"];
            [button setBackgroundColor:[UIColor whiteColor]];
            button.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
        }
        
                view.hidden = i == index ? NO:YES;
//                if (self.buttonImage) {
//        //            [self.buttonImage addAttribute:NSForegroundColorAttributeName value:[ZKColor getColor:i == index ? kNavigationBar_BackGroundColor:kFontcolor_777777]];
//        //            [button setImage:[self.buttonImage imageWithSize:CGSizeMake(20, 20)] forState:UIControlStateNormal];
//                }
    }
}


@end
