//
//  SCJSONKitAdapter.m
//  ShiHua
//
//  Created by Pingan Yi on 1/23/15.
//  Copyright (c) 2015 shuchuang. All rights reserved.
//

#import "SCJSONKitAdapter.h"

static NSString* sc_JSONString(id object) {
    if (!object)
        return nil;
    if (![NSJSONSerialization isValidJSONObject:object])
        return nil;
    NSError* error = nil;
    NSData* data = [NSJSONSerialization dataWithJSONObject:object options:0 error:&error];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@implementation NSArray (SCJSONKitAdapter)

- (NSString*) sc_JSONString {
    return sc_JSONString(self);
}

@end

@implementation NSDictionary (SCJSONKitAdapter)

- (NSString*) sc_JSONString {
    return sc_JSONString(self);
}

@end

@implementation NSString (SCJSONKitAdapter)

- (id) sc_objectFromJSONString {
    NSData* data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error = nil;
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
}

@end