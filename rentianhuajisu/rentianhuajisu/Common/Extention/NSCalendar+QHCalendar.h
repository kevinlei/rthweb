//
//  NSCalendar+QHCalendar.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSCalendar (QHCalendar)

- (nullable NSDate *)qh_firstDayOfMonth:(NSDate *)month;
- (nullable NSDate *)qh_lastDayOfMonth:(NSDate *)month;
- (nullable NSDate *)qh_firstDayOfWeek:(NSDate *)week;
- (nullable NSDate *)qh_lastDayOfWeek:(NSDate *)week;
- (nullable NSDate *)qh_middleDayOfWeek:(NSDate *)week;
- (NSInteger)qh_numberOfDaysInMonth:(NSDate *)month;


@end

NS_ASSUME_NONNULL_END
