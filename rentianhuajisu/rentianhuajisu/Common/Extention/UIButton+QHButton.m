//
//  UIButton+QHButton.m
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/7.
//

#import "UIButton+QHButton.h"

@implementation UIButton (QHButton)

+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:backgroundColor];
    [button setTitle:normalTitle forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = titleFont;
    if (cornerRadius > 0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    return button;
}

+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:backgroundColor];
    [button setTitle:normalTitle forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = titleFont;
    if (cornerRadius > 0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    if (borderWidth > 0) {
        button.layer.borderColor = titleColor.CGColor;
        button.layer.borderWidth = borderWidth;
    }
    
    return button;
}


@end
