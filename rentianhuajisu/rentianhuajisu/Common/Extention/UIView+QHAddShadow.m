//
//  UIView+QHAddShadow.m
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/19.
//

#import "UIView+QHAddShadow.h"

@implementation UIView (QHAddShadow)

/**
 * @brief 在当前view上添加阴影.
 */
- (void)qh_addShadowWithColor:(UIColor *)theColor shadowOffset:(CGSize)shadowOffset shadowRadius:(CGFloat)shadowRadius
{
    // 阴影颜色
    self.layer.shadowColor = theColor.CGColor;
    // 阴影偏移，默认(0, -3)
    self.layer.shadowOffset = shadowOffset;
    // 阴影透明度，默认0
    self.layer.shadowOpacity = 1.0f;
    // 阴影半径，默认3
    self.layer.shadowRadius = shadowRadius;
}

/**
 * @brief 在当前view指定位置添加阴影.
 */
- (void)qh_addShadowWithColor:(UIColor *)theColor shadowOffset:(CGSize)shadowOffset shadowRadius:(CGFloat)shadowRadius borderSideType:(QHBorderSideType)borderSideType
{
    self.layer.shadowColor = theColor.CGColor;
    self.layer.shadowOffset = shadowOffset;
    self.layer.shadowOpacity = 1.0f;
    self.layer.shadowRadius = shadowRadius;
    // 单边阴影 顶边
    float shadowPathWidth = self.layer.shadowRadius;
    CGRect shadowRect = CGRectMake(0, 0-shadowPathWidth/2.0, self.bounds.size.width, shadowPathWidth);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:shadowRect];
    self.layer.shadowPath = path.CGPath;
}

- (CALayer *)qh_addShadowWithShadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset
{
    return [self qh_addShadowWithShadowBackgroundColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0] shadowColor:[UIColor colorWithRed:46/255.0 green:89/255.0 blue:129/255.0 alpha:0.5] shadowRadius:shadowRadius contentInset:contentInset];
}

- (CALayer *)qh_addShadowWithShadowBackgroundColor:(UIColor *)shadowBackgroundColor shadowColor:(UIColor *)shadowColor shadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset
{
    //创建阴影layer
    CALayer *coverImageLayer = [CALayer layer];
    //设置阴影layer背景色
    coverImageLayer.backgroundColor = shadowBackgroundColor.CGColor;
    //阴影的颜色
    coverImageLayer.shadowColor = shadowColor.CGColor;
    //阴影偏移
    coverImageLayer.shadowOffset = CGSizeMake(-3,0);
    //阴影的不透明度
    coverImageLayer.shadowOpacity = 1;
    //用于创建阴影的模糊半径
    coverImageLayer.shadowRadius = shadowRadius;
    //layer的位置
    coverImageLayer.frame = self.frame;
    //计算贝塞尔曲线的位置路径
    CGFloat x = -contentInset.left;
    CGFloat y = -contentInset.top;
    CGFloat w = self.bounds.size.width + contentInset.left + contentInset.right;
    CGFloat h = self.bounds.size.height + contentInset.top + contentInset.bottom;
    CGRect shadowRect = CGRectMake(x, y, w, h);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:shadowRect];
    coverImageLayer.shadowPath = path.CGPath;
    [self.superview.layer insertSublayer:coverImageLayer below:self.layer];
    return coverImageLayer;
}

- (CALayer *)qh_addShadowWithShadowBackgroundColor:(UIColor *)shadowBackgroundColor shadowColor:(UIColor *)shadowColor shadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset cornerRadius:(CGFloat)cornerRadius
{
    //创建阴影layer
    CALayer *coverImageLayer = [CALayer layer];
    //设置阴影layer背景色
    coverImageLayer.backgroundColor = shadowBackgroundColor.CGColor;
    //阴影的颜色
    coverImageLayer.shadowColor = shadowColor.CGColor;
    //阴影偏移
    coverImageLayer.shadowOffset = CGSizeMake(-7,0);
    //阴影的不透明度
    coverImageLayer.shadowOpacity = 1;
    //用于创建阴影的模糊半径
    coverImageLayer.shadowRadius = shadowRadius;
    //layer的位置
    coverImageLayer.frame = self.frame;
    //计算贝塞尔曲线的位置路径
    CGFloat x = -contentInset.left;
    CGFloat y = -contentInset.top;
    CGFloat w = self.bounds.size.width + contentInset.left + contentInset.right;
    CGFloat h = self.bounds.size.height + contentInset.top + contentInset.bottom;
    CGRect shadowRect = CGRectMake(x, y, w, h);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:shadowRect cornerRadius:cornerRadius];
    coverImageLayer.shadowPath = path.CGPath;
    [self.superview.layer insertSublayer:coverImageLayer below:self.layer];
    return coverImageLayer;
}

- (void)qh_addBorderWithLineWidth:(CGFloat)lineWidth linesColor:(UIColor *)linesColor corners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius {
    if (@available(iOS 11.0, *)) {
        self.layer.cornerRadius = cornerRadius;
        self.layer.maskedCorners = (CACornerMask)corners;
        self.layer.borderColor = linesColor.CGColor;
        self.layer.borderWidth = lineWidth;
    } else {
        UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
        CAShapeLayer *borderLayer = [[CAShapeLayer alloc] init];
        borderLayer.frame = self.bounds;
        borderLayer.lineWidth = lineWidth;
        borderLayer.strokeColor = linesColor.CGColor;
        borderLayer.fillColor = [UIColor clearColor].CGColor;
        borderLayer.path = path.CGPath;
       [self.layer addSublayer:borderLayer];
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.path = bezierPath.CGPath;
        [self.layer setMask:maskLayer];
    }
}

/// 虚线框
- (void)qh_addDottedLine:(UIColor *)linesColor lineWidth:(CGFloat)lineWidth cornerRadius:(CGFloat)cornerRadius
{
    CAShapeLayer *border = [CAShapeLayer layer];
    
    //虚线的颜色
    border.strokeColor = linesColor.CGColor;
    //填充的颜色
    border.fillColor = [UIColor clearColor].CGColor;
    //设置路径
    border.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius].CGPath;
    border.frame = self.bounds;
    //虚线的宽度
    border.lineWidth = lineWidth;
    
    //设置线条的样式
    //    border.lineCap = @"square";
    //虚线的间隔
    border.lineDashPattern = @[@4, @4];
    
    [self.layer addSublayer:border];
}

/**
 ** lineLength:     虚线的宽度
 ** lineSpacing:    虚线的间距
 ** linesColor:      虚线的颜色
 **/
- (void)qh_drawDashLineLength:(int)lineLength lineSpacing:(int)lineSpacing linesColor:(UIColor *)linesColor
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:self.bounds];
    [shapeLayer setPosition:CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame))];
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    
    //  设置虚线颜色为
    [shapeLayer setStrokeColor:linesColor.CGColor];
    
    //  设置虚线宽度
    [shapeLayer setLineWidth:CGRectGetHeight(self.frame)];
    [shapeLayer setLineJoin:kCALineJoinRound];
    
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddLineToPoint(path, NULL, CGRectGetWidth(self.frame), 0);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    //  把绘制好的虚线添加上来
    [self.layer addSublayer:shapeLayer];
}

- (UIView *)qh_borderForColor:(UIColor *)color borderWidth:(CGFloat)borderWidth borderType:(QHBorderSideType)borderType {
    
    if (borderType == QHBorderSideTypeAll) {
        self.layer.borderWidth = borderWidth;
        self.layer.borderColor = color.CGColor;
        return self;
    }
    
    
    /// 左侧
    if (borderType & QHBorderSideTypeLeft) {
        /// 左侧线路径
        [self.layer addSublayer:[self qh_addLineOriginPoint:CGPointMake(0.f, 0.f) toPoint:CGPointMake(0.0f, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    /// 右侧
    if (borderType & QHBorderSideTypeRight) {
        /// 右侧线路径
        [self.layer addSublayer:[self qh_addLineOriginPoint:CGPointMake(self.frame.size.width, 0.0f) toPoint:CGPointMake( self.frame.size.width, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    /// top
    if (borderType & QHBorderSideTypeTop) {
        /// top线路径
        [self.layer addSublayer:[self qh_addLineOriginPoint:CGPointMake(0.0f, 0.0f) toPoint:CGPointMake(self.frame.size.width, 0.0f) color:color borderWidth:borderWidth]];
    }
    
    /// bottom
    if (borderType & QHBorderSideTypeBottom) {
        /// bottom线路径
        [self.layer addSublayer:[self qh_addLineOriginPoint:CGPointMake(0.0f, self.frame.size.height) toPoint:CGPointMake( self.frame.size.width, self.frame.size.height) color:color borderWidth:borderWidth]];
    }
    
    return self;
}

- (CAShapeLayer *)qh_addLineOriginPoint:(CGPoint)p0 toPoint:(CGPoint)p1 color:(UIColor *)color borderWidth:(CGFloat)borderWidth {
    
    /// 线的路径
    UIBezierPath * bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:p0];
    [bezierPath addLineToPoint:p1];
    
    CAShapeLayer * shapeLayer = [CAShapeLayer layer];
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.fillColor  = [UIColor clearColor].CGColor;
    /// 添加路径
    shapeLayer.path = bezierPath.CGPath;
    /// 线宽度
    shapeLayer.lineWidth = borderWidth;
    return shapeLayer;
}

/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)qh_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    shape.frame = self.bounds;
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)qh_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}


@end
