//
//  NSObject+QHTools.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/2/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (QHTools)
/**
 * @brief 复制文本到剪切板.
*/
- (void)pasteboardWithCopyString:(NSString *)copyString complete:(void(^)(BOOL isSuccess))complete;

/**
 * @brief 四舍五入小数保留两位
*/
- (NSDecimalNumber *)roundFloat:(float)price;

/**
 * @brief 获取当前顶层控制器
*/
- (UIViewController *)getCurrentViewController;
@end

NS_ASSUME_NONNULL_END
