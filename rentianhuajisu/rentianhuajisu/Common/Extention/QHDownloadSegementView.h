//
//  QHDownloadSegementView.h
//  LearningCenter
//
//  Created by 启航龙图.在线事业部.5555 on 2021/2/1.
//

#import <UIKit/UIKit.h>
typedef void(^CurrentIndex)(NSInteger index);
NS_ASSUME_NONNULL_BEGIN

@interface QHDownloadSegementView : UIView
+ (QHDownloadSegementView *)createSegementViews:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles;

-(void)reloadRecommandData;
-(void)scrollAtIndex:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
