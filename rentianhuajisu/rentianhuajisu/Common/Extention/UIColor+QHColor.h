//
//  UIColor+QHColor.h
//  QHiPad
//
//  Created by 启航龙图.在线事业部.李东 on 2019/3/1.
//  Copyright © 2019 启航教育. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (QHColor)

/**
 * @brief 根据字16进制字符串获取颜色.
 *
 * @param hexString 16进制字符串.
 *
 * @return UIColor.
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString;

/**
 * @brief 根据字色值获取颜色.
 */
+ (UIColor *)colorWithRGB:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

/**
 * @brief 根据字色值获取颜色包含透明度.
 */
+ (UIColor *)colorWithRGBA:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

@end

NS_ASSUME_NONNULL_END
