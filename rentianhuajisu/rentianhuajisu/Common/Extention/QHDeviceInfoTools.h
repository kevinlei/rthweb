//
//  QHDeviceInfoTools.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QHDeviceInfoTools : NSObject
/// 获取设备信息
+ (NSString *)getDeviceInfo;
/// 获取UA
+ (NSString *)getUserAgentInfo;
/// 获取文件大小
+ (NSString *)freeSize;
/// 获取webView中App信息
+ (NSString *)getWebviewAppInfo;
/// app名称
+ (NSString *)app_name;
/// app版本
+ (NSString *)app_version;
/// app build版本
+ (NSString *)app_build;
///// 手机序列号
//+ (NSString *)phone_identifierNumber;
/// 手机别名： 用户定义的名称
+ (NSString *)phone_userPhoneName;
/// 设备名称
+ (NSString *)device_name;
/// 设备系统版本号
+ (NSString *)system_version;
/// 手机型号
+ (NSString *)device_model;
/// 地方型号  （国际化区域名称）
+ (NSString *)localPhoneModel;
/// machine
+ (NSString *)machine;
/// 运营商
+ (NSString *)carrierInfo;
/// 当前网络连接
+ (NSString *)networkStatus;
/// 设备型号
+ (NSString *)deviceModel;
/// 设备类型
+ (NSString *)deviceType;

@end

NS_ASSUME_NONNULL_END
