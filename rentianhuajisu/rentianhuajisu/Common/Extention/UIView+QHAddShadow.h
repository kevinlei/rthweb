//
//  UIView+QHAddShadow.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, QHBorderSideType) {
    QHBorderSideTypeAll  = 0,
    QHBorderSideTypeTop = 1 << 0,
    QHBorderSideTypeBottom = 1 << 1,
    QHBorderSideTypeLeft = 1 << 2,
    QHBorderSideTypeRight = 1 << 3,
};

@interface UIView (QHAddShadow)
/**
 * @brief 在当前view上添加阴影，.
 *
 * @param theColor 阴影的颜色.
 *
 * @param shadowOffset 阴影阴影偏移.
 *
 * @param shadowRadius 阴影半径.
 *
 */
- (void)qh_addShadowWithColor:(UIColor *)theColor shadowOffset:(CGSize)shadowOffset shadowRadius:(CGFloat)shadowRadius;

/**
 * @brief 在当前view指定位置添加阴影，.
 *
 * @param theColor 阴影的颜色.
 *
 * @param shadowOffset 阴影阴影偏移.
 *
 * @param shadowRadius 阴影半径.
 *
 */
- (void)qh_addShadowWithColor:(UIColor *)theColor shadowOffset:(CGSize)shadowOffset shadowRadius:(CGFloat)shadowRadius borderSideType:(QHBorderSideType)borderSideType;

/**
 * @brief 在view底部添加一个layer，来实现阴影.
 *
 * @param shadowRadius 阴影的模糊半径.
 *
 * @param contentInset 阴影偏移.
 *
 */
- (CALayer *)qh_addShadowWithShadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset;

/**
 * @brief 在view底部添加一个layer，来实现阴影.
 *
 * @param shadowBackgroundColor 阴影layer背景色.
 *
 * @param shadowColor 阴影的颜色.
 *
 * @param shadowRadius 阴影的模糊半径.
 *
 * @param contentInset 阴影偏移.
 *
 */
- (CALayer *)qh_addShadowWithShadowBackgroundColor:(UIColor *)shadowBackgroundColor shadowColor:(UIColor *)shadowColor shadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset;

/**
 * @brief 在view底部添加一个layer，来实现阴影，带圆角.
 *
 * @param shadowBackgroundColor 阴影layer背景色.
 *
 * @param shadowColor 阴影的颜色.
 *
 * @param shadowRadius 阴影的模糊半径.
 *
 * @param contentInset 阴影偏移.
 *
 * @param cornerRadius 圆角.
 *
 */
- (CALayer *)qh_addShadowWithShadowBackgroundColor:(UIColor *)shadowBackgroundColor shadowColor:(UIColor *)shadowColor shadowRadius:(CGFloat)shadowRadius contentInset:(UIEdgeInsets)contentInset cornerRadius:(CGFloat)cornerRadius;

/**
 * @brief 给view边框.
 *
 * @param lineWidth 边框的宽度.
 *
 * @param linesColor 边框的颜色.
 *
 * @param corners 圆角位置.
 *
 * @param cornerRadius 边框圆角的半径.
 *
 */
- (void)qh_addBorderWithLineWidth:(CGFloat)lineWidth linesColor:(UIColor *)linesColor corners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius;
/**
 * @brief 给view添加虚线的边框.
 *
 * @param linesColor 边框的颜色.
 *
 * @param lineWidth 边框的宽度.
 *
 * @param cornerRadius 边框圆角的半径.
 *
 */
- (void)qh_addDottedLine:(UIColor *)linesColor lineWidth:(CGFloat)lineWidth cornerRadius:(CGFloat)cornerRadius;

/**
 * @brief 给view添加虚线.
 *
 * @param lineLength 虚线的宽度.
 *
 * @param lineSpacing 虚线的间距.
 *
 * @param linesColor 虚线的颜色.
 *
 */
- (void)qh_drawDashLineLength:(int)lineLength lineSpacing:(int)lineSpacing linesColor:(UIColor *)linesColor;


- (UIView *)qh_borderForColor:(UIColor *)color borderWidth:(CGFloat)borderWidth borderType:(QHBorderSideType)borderType;

/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)qh_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii;
/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)qh_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect;
@end

NS_ASSUME_NONNULL_END
