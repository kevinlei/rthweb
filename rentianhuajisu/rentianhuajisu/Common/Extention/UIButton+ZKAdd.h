//
//  UIButton+ZKAdd.h
//  StudentKudou
//
//  Created by 宋晨 on 2017/4/30.
//  Copyright © 2017年 赵坪生. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ZKAdd)
/**
 *  下面两个方法，在设置了button的title和image之后调用，调整布局
 */
- (void)verticalImageAndTitle:(CGFloat)spacing;//!< 调整imageView和titleLabel位置，变成垂直居中

- (void)horizontalImageAndTitle:(CGFloat)spacing;//!< 调整imageView和titleLabel位置，变成水平居中

- (void)leftTitleAndRightImage:(CGFloat)spacing;//!< 左边标题，右边图片
@end
