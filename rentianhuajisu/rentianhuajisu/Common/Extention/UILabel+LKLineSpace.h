//
//  UILabel+LKLineSpace.h
//  BQPersonal
//
//  Created by xiaohua on 2020/11/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (LKLineSpace)

-(void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing;

@end

NS_ASSUME_NONNULL_END
