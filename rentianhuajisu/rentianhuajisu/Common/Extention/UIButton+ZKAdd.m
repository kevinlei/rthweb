//
//  UIButton+ZKAdd.m
//  StudentKudou
//
//  Created by 宋晨 on 2017/4/30.
//  Copyright © 2017年 赵坪生. All rights reserved.
//

#import "UIButton+ZKAdd.h"

@implementation UIButton (ZKAdd)

#pragma mark - 调整imageview和titleLabel的位置,垂直居中

- (void)verticalImageAndTitle:(CGFloat)spacing {
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    NSDictionary *attributesDict = @{NSFontAttributeName:self.titleLabel.font};
    
    CGSize textSize = [self.titleLabel.text sizeWithAttributes:attributesDict];
    
    CGSize frameSize = CGSizeMake(ceilf(textSize.width), ceilf(textSize.height));
    if (titleSize.width + 0.5 < frameSize.width) {
        titleSize.width = frameSize.width;
    }
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height), 0.0, 0.0, - titleSize.width);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, - imageSize.width, - (totalHeight - titleSize.height), 0);
}

#pragma mark - 调整位置，水平居中

- (void)horizontalImageAndTitle:(CGFloat)spacing {
    CGSize titleSize = self.titleLabel.frame.size;
    NSDictionary *attributesDict = @{NSFontAttributeName : self.titleLabel.font};
    CGSize textSize = [self.titleLabel.text sizeWithAttributes:attributesDict];
    CGSize frameSize = CGSizeMake(ceilf(textSize.width), ceilf(textSize.height));
    if (titleSize.width + 0.5 < frameSize.width) {
        titleSize.width = frameSize.width;
    }
    self.imageEdgeInsets = UIEdgeInsetsMake(0, - spacing / 2.000f, 0, 0);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing / 2.000f, 0, 0);
}

#pragma mark - 左边标题，右边图片

- (void)leftTitleAndRightImage:(CGFloat)spacing {
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    
    NSDictionary *attributesDict = @{NSFontAttributeName : self.titleLabel.font};
    CGSize textSize = [self.titleLabel.text sizeWithAttributes:attributesDict];
    
    CGSize frameSize = CGSizeMake(ceilf(textSize.width), ceilf(textSize.height));
    if (titleSize.width + 0.5 < frameSize.width) {
        titleSize.width = frameSize.width;
    }
    CGFloat totalWitdh = (imageSize.width + titleSize.width + spacing);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(0, totalWitdh - imageSize.width, 0, - (totalWitdh - imageSize.width));
    self.titleEdgeInsets = UIEdgeInsetsMake(0, - (totalWitdh - titleSize.width), 0, (totalWitdh - titleSize.width));
}
@end
