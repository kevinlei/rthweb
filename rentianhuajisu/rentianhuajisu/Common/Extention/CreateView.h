///
///  CreateView.h
///  268EDU_Demo
///
///  Created by EDU268 on 15/10/28.
///  Copyright © 2015年 edu268. All rights reserved.
///

#import <UIKit/UIKit.h>

@interface CreateView : UIView
+ (UIView *)createViewFrame:(CGRect)rect andBackgroundColor:(UIColor *)color;



@end
