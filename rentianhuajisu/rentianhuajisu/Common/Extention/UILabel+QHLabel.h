//
//  UILabel+QHLabel.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (QHLabel)

- (instancetype)initWithFrame:(CGRect)frame textColor:(UIColor *)textColor font:(CGFloat)font weight:(UIFontWeight)weight;

/**
 * @brief 初始化label.
 *
 * @param text 文本.
 *
 * @param fontSize 字体大小.
 *
 * @param textColor 字体颜色.
 *
 * @param textAlignment 文本位置.
 *
 * @return UILabel.
 */
+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment;

/**
 * @brief 初始化label.
 *
 * @param text 文本.
 *
 * @param fontSize 字体大小.
 *
 * @param textColor 字体颜色.
 *
 * @return UILabel.
 */
+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor;

/**
 * @brief 初始化label.
 *
 * @param fontSize 字体大小.
 *
 * @param textColor 字体颜色.
 *
 * @return UILabel.
 */
+ (UILabel *)labelWithFontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor;

/**
 * @brief 初始化label.
 *
 * @param fontName 字体名字.
 *
 * @param fontSize 字体大小.
 *
 * @param textColor 字体颜色.
 *
 * @return UILabel.
 */
+ (UILabel *)labelWithFontSize:(CGFloat)fontSize fontName:(NSString *)fontName textColor:(UIColor *)textColor;
+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize fontName:(NSString *)fontName textColor:(UIColor *)textColor;

/**
 *  改变行间距
 */
- (void)iqh_changeLineSpaceWithSpace:(float)space;

/**
 *  改变字间距
 */
- (void)iqh_changeWordSpaceWithSpace:(float)space;

/**
 *  改变行间距和字间距
 */
- (void)iqh_changeSpaceWithLineSpace:(float)lineSpace WordSpace:(float)wordSpace;

@end

NS_ASSUME_NONNULL_END
