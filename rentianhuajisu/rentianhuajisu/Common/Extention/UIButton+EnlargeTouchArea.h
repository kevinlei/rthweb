//
//  UIButton+EnlargeTouchArea.h
//  爱启航
//
//  Created by 5555 on 2018/4/24.
//  Copyright © 2018年 爱启航. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (EnlargeTouchArea)


- (void)setEnlargeEdgeWithTop:(CGFloat) top right:(CGFloat) right bottom:(CGFloat) bottom left:(CGFloat) left;
- (void)setEnlargeEdge:(CGFloat) size;
@end
