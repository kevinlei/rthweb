//
//  UILabel+QHLabel.m
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/7.
//

#import "UILabel+QHLabel.h"

@implementation UILabel (QHLabel)

- (instancetype)initWithFrame:(CGRect)frame textColor:(UIColor *)textColor font:(CGFloat)font weight:(UIFontWeight)weight {
    if (self = [self initWithFrame:frame]) {
        self.numberOfLines = 0;
        self.textColor = textColor;
        self.font = [UIFont systemFontOfSize:font weight:weight];
    }
    return self;
}

+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment {
    if (textAlignment == NSTextAlignmentJustified) {
        UILabel *label = [[UILabel alloc] init];
        NSMutableAttributedString *mutaString = [[NSMutableAttributedString alloc]initWithString:text];
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc]init];
        paraStyle.alignment = NSTextAlignmentJustified;//两端对齐
        paraStyle.paragraphSpacing = fontSize;//行后间距
        NSDictionary *dic = @{
                              NSForegroundColorAttributeName:textColor,
                              NSFontAttributeName:[UIFont systemFontOfSize:fontSize weight:weight],
                              NSParagraphStyleAttributeName:paraStyle,
                              NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone]
                              };
        [mutaString setAttributes:dic range:NSMakeRange(0, mutaString.length)];
        label.attributedText = mutaString;
        return label;
    } else {
        UILabel *label = [self labelWithFontSize:fontSize weight:weight textColor:textColor];
        label.text = text;
        label.textAlignment = textAlignment;
        return label;
    }
}

+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor {
    UILabel *label = [self labelWithFontSize:fontSize weight:weight textColor:textColor];
    label.text = text;
    return label;
}

+ (UILabel *)labelWithFontSize:(CGFloat)fontSize weight:(UIFontWeight)weight textColor:(UIColor *)textColor {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = textColor;
    label.font = [UIFont systemFontOfSize:fontSize weight:weight];
    return label;
}

+ (UILabel *)labelWithFontSize:(CGFloat)fontSize fontName:(NSString *)fontName textColor:(UIColor *)textColor {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = textColor;
    label.font = [UIFont fontWithName:fontName size:fontSize];
    return label;
}

+ (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize fontName:(NSString *)fontName textColor:(UIColor *)textColor {
    UILabel *label = [UILabel labelWithFontSize:fontSize fontName:fontName textColor:textColor];
    label.text = text;
    return label;
}

- (void)iqh_changeLineSpaceWithSpace:(float)space {
    if (!self.text || !self) {
        return;
    }
    NSString *labelText = self.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:space];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    self.attributedText = attributedString;
    [self sizeToFit];
    
}

- (void)iqh_changeWordSpaceWithSpace:(float)space {
    if (!self.text || !self) {
        return;
    }
    NSString *labelText = self.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(space)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    self.attributedText = attributedString;
    [self sizeToFit];
    
}

- (void)iqh_changeSpaceWithLineSpace:(float)lineSpace WordSpace:(float)wordSpace {
    if (!self.text || !self) {
        return;
    }
    NSString *labelText = self.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(wordSpace)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    self.attributedText = attributedString;
    [self sizeToFit];
    
}

@end
