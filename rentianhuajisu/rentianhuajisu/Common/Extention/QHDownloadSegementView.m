//
//  QHDownloadSegementView.m
//  LearningCenter
//
//  Created by 启航龙图.在线事业部.5555 on 2021/2/1.
//

#import "QHDownloadSegementView.h"
#define kButtonTag 100
#define kLineViewTag 200
#define kSegementHeight 50

@interface QHDownloadSegementView ()<UIScrollViewDelegate>
@property (nonatomic, strong)UIScrollView *scrollView;
@property (nonatomic, strong)NSArray *viewArray;
@property (nonatomic, strong)NSMutableArray *buttonArray;
@property (nonatomic, strong)CurrentIndex indexBlock;
@property (nonatomic, strong)UIImage *buttonImage;

@end
@implementation QHDownloadSegementView
+ (QHDownloadSegementView *)createSegementViews:(NSArray *)views viewIndex:(CurrentIndex)index titles:(NSArray *)titles{
    QHDownloadSegementView *segement = [QHDownloadSegementView new];
    segement.backgroundColor = [UIColor clearColor];
    segement.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight );
    segement.viewArray = views;
    segement.indexBlock = index;
    [segement setupUIWithtitles:titles];
    return segement;
}
- (void)setupUIWithtitles:(NSArray *)titles{
    _buttonArray = [NSMutableArray array];
    for (int i = 0; i < titles.count; i ++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor whiteColor];
        button.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        [button setTitle:titles[i] forState:UIControlStateNormal];
        button.tag = kButtonTag + i;
        [_buttonArray addObject:button];
        if (i==0) {
            button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [button setTitleColor:[UIColor colorWithHexString:@"#258DF3"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor colorWithHexString:@"#E6F3FF"];
        }else{
            button.titleLabel.font = [UIFont systemFontOfSize:14];
            [button setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
        }
        
        if (self.buttonImage) {
            [button setImage:self.buttonImage forState:UIControlStateNormal];
        }
        [button addTarget:self action:@selector(buttonaction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(KScreenWidth / titles.count * i);
            make.top.equalTo(self);
            make.width.mas_offset(KScreenWidth / titles.count);
            make.height.equalTo(@(kSegementHeight));
        }];
        
        //        UIView *lineView = [UIView new];
        //        if (i >= 1) {
        //            lineView.hidden = YES;
        //        }
        //        lineView.hidden = YES;
        //        lineView.tag = kLineViewTag + i;
        //        lineView.backgroundColor = [UIColor colorWithBLString:@"#3ACCAE"];
        //        [button addSubview:lineView];
        //        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        //            make.centerX.equalTo(button);
        //            make.height.mas_equalTo(@2);
        //            make.width.equalTo(button).multipliedBy(0.6);
        //            make.bottom.equalTo(button).offset(-0.5);
        //        }];
        UIImageView *lineView = [[UIImageView alloc] initWithImage:nil];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#E83828"];
        if (i != 0) {
            lineView.hidden = YES;
        }
        lineView.tag = kLineViewTag + i;
//        [button addSubview:lineView];
//        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(button);
//            make.height.mas_equalTo(@1);
//            make.width.equalTo(button).multipliedBy(0.65);
//            make.bottom.equalTo(button).offset(0);
//        }];
        
    }
    
    UIView *bottomLineView = [UIView new];
    bottomLineView.backgroundColor = [UIColor redColor];
    bottomLineView.hidden = YES;
    [self addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.equalTo(@0.5);
        make.top.equalTo(@(kSegementHeight - 0.5));
    }];
    
    for (int i = 1; i < titles.count; i++) {
        UIView *lineView = [UIView new];
//        lineView.hidden = YES;
        lineView.backgroundColor =[UIColor colorWithHexString:@"#E1E1E1"];
//        [self addSubview:lineView];
//        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self).offset(KScreenWidth / titles.count * i);
//            make.top.equalTo(self.mas_top).offset(18);
////            make.bottom.equalTo(self.mas_bottom).offset(-8);
//            make.width.equalTo(@1);
//            make.height.equalTo(@14);
////            make.centerY.equalTo(self.mas_centerY).offset(0);
//        }];
    }

    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSegementHeight, KScreenWidth, self.frame.size.height - kSegementHeight)];
    self.scrollView.contentSize = CGSizeMake(KScreenWidth * self.viewArray.count, self.scrollView.frame.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.alwaysBounceHorizontal = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = self.backgroundColor;
    [self addSubview:self.scrollView];
    for (int i = 0; i < self.viewArray.count; i++) {
        CGFloat height;
        height = self.scrollView.frame.size.height;
        ((UIView *)(self.viewArray[i])).frame = CGRectMake(self.scrollView.frame.size.width * i, 0, self.scrollView.frame.size.width, height);
        [self.scrollView addSubview:self.viewArray[i]];
    }
    
    UIView *lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = [UIColor colorWithHexString:@"#DDDDDD"];
    lineView1.frame = CGRectMake(0, 0, KScreenWidth, 0.5);
    [self addSubview:lineView1];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#DDDDDD"];
    lineView.frame = CGRectMake(0, kSegementHeight, KScreenWidth, 0.5);
    [self addSubview:lineView];
}

-(void)scrollAtIndex:(NSInteger)index{
    [self buttonaction: [self.buttonArray objectAtIndex:index]];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat offest = scrollView.contentOffset.x;
    NSInteger index = offest / KScreenWidth;
    [self setButtonStateWithIndex:index];
    if (self.indexBlock) {
        self.indexBlock(index);
    }
}

- (void)buttonaction:(UIButton *)button{
    NSInteger buttonTag = button.tag - kButtonTag;
    [self setButtonStateWithIndex:buttonTag];
    if (self.indexBlock) {
        self.indexBlock(buttonTag);
    }
    [self.scrollView setContentOffset:CGPointMake(KScreenWidth * buttonTag, 0) animated:YES];
}

- (void)setButtonStateWithIndex:(NSInteger)index{
    for (int i = 0; i < self.viewArray.count; i++) {
        UIButton * button = [self viewWithTag:kButtonTag + i];
        UIView * view = [button viewWithTag:kLineViewTag + i];
        if (i==index) {
            button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [button setTitleColor:[UIColor colorWithHexString:@"#258DF3"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor colorWithHexString:@"#E6F3FF"];
        }else{
            button.titleLabel.font = [UIFont systemFontOfSize:14];
            [button setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
        }
        view.hidden = i == index ? NO:YES;
        if (self.buttonImage) {
            [button setImage:self.buttonImage forState:UIControlStateNormal];
        }
    }
}


@end
