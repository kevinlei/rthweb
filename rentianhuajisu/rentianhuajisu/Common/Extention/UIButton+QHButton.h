//
//  UIButton+QHButton.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (QHButton)

/**
 * @brief 初始化button.
 *
 * @param normalTitle 默认title.
 *
 * @param titleFont 字体.
 *
 * @param titleColor 默认字体颜色.
 *
 * @param backgroundColor 背景色.
 *
 * @param cornerRadius 圆角.
 *
 * @return UIButton.
 */
+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius;

/**
 * @brief 初始化button.
 *
 * @param normalTitle 默认title.
 *
 * @param titleFont 字体.
 *
 * @param titleColor 默认字体颜色.
 *
 * @param backgroundColor 背景色.
 *
 * @param cornerRadius 圆角.
 *
 * @param borderWidth 边框宽度.
 *
 * @return UIButton.
 */
+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth;

@end

NS_ASSUME_NONNULL_END
