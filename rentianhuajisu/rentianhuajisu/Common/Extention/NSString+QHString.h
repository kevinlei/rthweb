//
//  NSString+QHString.h
//  Common
//
//  Created by 启航龙图.在线事业部.李东 on 2021/1/8.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (QHString)
#pragma mark ---- 字符串判断
/**
 * @brief 判断当前字符串是否为空.
 *
 * @param string 字符串.
 *
 * @return 一个BOOL值，字符串为空，返回YES，不为空返回NO.
 */
+ (BOOL)iqh_isBlank:(NSString *)string;

/**
 * @brief 判断当前字符串是否为空.
 *
 * @param string 字符串.
 *
 * @return 一个BOOL值，字符串为空，返回YES，不为空返回NO.
 */
+ (BOOL)iqh_isNull:(NSString *)string;

/**
 * @brief 去除当前字符串的空格.
 */
- (NSString *)iqh_trim;

/**
*  @brief  字符串去空格
*  @param  text 文本
*  @return 钱的字符串
*/
+ (NSString *)deletSpaceInString:(NSString *)text andIsAll:(BOOL )all;

#pragma mark ---- 字符串 or json转字典
/**
 * @brief json字符串转字典.
 */
- (NSDictionary *)iqh_JSON;

#pragma mark ---- 字符串 or 金钱


#pragma mark ---- 字符 or 时间
/**
 * @brief 时间戳转字符串（yyyy-MM-dd）.
 *
 * @param timeInterval 时间戳（毫秒级）.
 *
 * @return NSString.
 */
+ (NSString *)stringWithTimeInterval:(NSTimeInterval)timeInterval ;
+ (NSString *)stringWithDate:(NSDate *)date;

/// 将服务器返回的时间（yyyy-MM-dd HH:mm:ss）转换成指定格式
+ (NSString *)stringWithServiceTimeString:(NSString *)serviceTimeString dateFormat:(NSString *)dateFormat;

/**
 * @brief 时间戳转字符串(根据dateWithTimeIntervalSince1970).
 *
 * @param time 时间戳（毫秒级）.
 *
 * @param dateFormat 时间格式.
 *
 * @return NSString.
 */
+ (NSString *)stringWithDate:(NSTimeInterval)time dateFormat:(NSString *)dateFormat;
//将NSDate戳转化成 时间(根据dateWithTimeIntervalSince1970)
+ (NSString *)stringTimeWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat;

/**
* @brief 将某个时间戳转化成 时间(根据本地时区).
*
* @param timestamp 时间戳（毫秒级）.
*
* @param format 时间格式.
*
* @return NSString.
*/
+ (NSString *)timestampSwitchTime:(NSTimeInterval)timestamp andFormatter:(NSString *)format;

/**
 * @brief 时间戳转字符串（MM/dd）.
 *
 * @param time 时间戳（毫秒级）.
 *
 * @return NSString.
 */
+ (NSString *)stringWithDay:(NSTimeInterval)time;

/**
 * @brief 时间戳转字符串（HH:MM:SS）.
 *
 * @param time 时间戳（毫秒级）.
 *
 * @return NSString.
 */
+ (NSString *)stringWithHour:(NSTimeInterval)time;

/**
 * @brief 获取当前时间的时间戳.
 *
 * @return NSString.
 */
+ (NSString*)getCurrentTimestamp;

#pragma mark ---- 字符串 or data

/**
 * @brief 获取字符串的MD5.
 */
- (NSString *)md5;

/**
 * @brief 获取假烟后的字符串的MD5.
 */
- (NSString *)smsmd5;

#pragma mark ---- 字符串 or URL

/**
 * @brief 获取url的Encode后的字符串.
 */
- (NSString *)iqh_urlEncode;

/**
 * @brief 获取url的Decode后的字符串.
 */
- (NSString *)iqh_urlDecode;

/**
 * @brief 获取url的Encode后的字符串.
 */
- (NSString *)iqh_URLEncodedString;

/**
 * @brief 获取url的Decode后的字符串.
 */
- (NSString *)iqh_URLDecodedString;

#pragma mark ---- 字符串 or 富文本
/**
 * @brief 将当前字符串转成富文本，将图片添加到文本头部.
 *
 * @param string 字符串.
 *
 * @param imageName 图片的名字.
 *
 * @return NSMutableAttributedString.
 */
+ (NSMutableAttributedString *)attWithString:(NSString *)string imageName:(NSString *)imageName;

/**
 * @brief 将当前字符串转成富文本，将图片添加到文本尾部.
 *
 * @param string 字符串.
 *
 * @param imageName 图片的名字.
 *
 * @return NSMutableAttributedString.
 */
+ (NSMutableAttributedString *)attWithString:(NSString *)string lastImageName:(NSString *)imageName;

/**
 * @brief 将当前字符串转成富文本，将图片添加到文本中.

 @param string 字符串
 @param imageName 图片名称
 @param index 插入位置
 @param rect 图片的大小
 @return 富文本
 */
+ (NSMutableAttributedString *)attWithString:(NSString *)string imageName:(NSString *)imageName imageIndex:(NSInteger)index imageRect:(CGRect)rect ;

/**
 * @brief 将当前字符串转成富文本，将图片添加到文本中.
 
 @param string 字符串
 @param imageName 图片名称
 @param index 插入位置
 @param y 图片的Y的位置
 @return 富文本
 */
+ (NSMutableAttributedString *)attWithString:(NSString *)string imageName:(NSString *)imageName imageIndex:(NSInteger)index textColor:(UIColor *)color imageY:(float)y ;

/**
 * @brief 将当前字符串转成富文本，指定位置的字符设置成指定字体大小及其颜色.
 *
 * @param string 字符串.
 *
 * @param color 指定颜色.
 *
 * @param font 指定大小.
 *
 * @param rang 指定位置.
 *
 * @return NSMutableAttributedString.
 */
+ (NSMutableAttributedString *)attWithString:(NSString *)string color:(UIColor *)color font:(CGFloat)font rang:(NSRange)rang;

/**
 计算文本高度
 @param width       lab宽度
 @param lineSpacing 行间距(没有行间距就传0)
 @param font        文本字体大小
 
 @return 文本高度
 */
- (CGFloat)getTextHeightWithWidth:(CGFloat)width withLineSpacing:(CGFloat)lineSpacing withFont:(CGFloat)font;

//根据高度度求宽度  text 计算的内容  Height 计算的高度 font字体大小
/**
根据高度度求宽度
@param height       高度
@param font        文本字体大小

@return 文本高度
*/
- (CGFloat)getTextWidthHeight:(CGFloat)height font:(CGFloat)font;

// 生成两端对齐的富文本
+ (NSMutableAttributedString *)getJustifiedString:(NSString *)text fontSize:(CGFloat)fontSize fontWeight:(UIFontWeight)fontWeight textColor:(UIColor *)textColor;

/// 匹配字符串中所有的数字设置指定颜色
+ (NSMutableAttributedString *)attributedString:(NSString *)string font:(UIFont *)font normalColor:(UIColor *)normalColor numberColor:(UIColor *)numberColor;

#pragma mark ----字符串 or 验证（电话号码、邮箱、身份证、密码等）
/**
 * @brief 判断当前字符串是否是手机验证码.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validatePhoneVerificationCode;

/**
 * @brief 判断当前字符串是否是图片验证码.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validatePictureVerificationCode;

/**
 * @brief 判断当前字符串是否是电话号码.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateMobile;

/**
 * @brief 判断当前字符串是否是邮箱.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateEmail;

/**
 * @brief 判断当前字符串是否是符合密码的输入规则.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validatePassword;

/**
 * @brief 判断当前字符串是否是身份证号码.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateIdentityCard;

/**
 * @brief 判断当前字符串是否是银行卡号.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateBankCardNumCheck;

/**
 * @brief 判断当前字符串是否是文本.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateTextField;

/**
 * @brief 判断当前字符串是否是钱数.
 *
 * @return 返回YES：通过验证，返回NO：验证未通过.
 */
- (BOOL)validateMoney;

/**
* @brief 判断当前字符串是否是纯数字.
*
* @return 返回YES：通过验证，返回NO：验证未通过.
*/
- (BOOL)validateNumber;

/**
 *  md5加密
 */
+ (NSString*)md5HexDigest:(NSString*)input;
/**
 *  根据文件名计算出文件大小
 */
- (unsigned long long)lx_fileSize;
/**
 *  生成缓存目录全路径
 */
- (instancetype)cacheDir;
/**
 *  生成文档目录全路径
 */
- (instancetype)docDir;
/**
 *  生成临时目录全路径
 */
- (instancetype)tmpDir;

/**
 *  @brief 根据字数的不同,返回UILabel中的text文字需要占用多少Size
 *  @param size 约束的尺寸
 *  @param font 文本字体
 *  @return 文本的实际尺寸
 */
- (CGSize)textSizeWithContentSize:(CGSize)size font:(UIFont *)font;

/**
 *  @brief  根据文本字数/文本宽度约束/文本字体 求得text的Size
 *  @param width 宽度约束
 *  @param font  文本字体
 *  @return 文本的实际高度
 */
- (CGFloat)textHeightWithContentWidth:(CGFloat)width font:(UIFont *)font;

/**
 *  @brief  根据文本字数/文本宽度约束/文本字体 求得text的Size
 *  @param height 宽度约束
 *  @param font  文本字体
 *  @return 文本的实际长度
 */
- (CGFloat)textWidthWithContentHeight:(CGFloat)height font:(UIFont *)font;
/**
 *  @brief  秒数转字符串分：秒
 *  @param  timeSecond 秒数
 *  @return 分：秒
 */
+ (NSString *)convertTimeSecond:(NSInteger)timeSecond;

#pragma mark ---- 生成二维码
+ (CIImage *)creatQRcodeWithUrlstring:(NSString *)urlString;

/// 生成二维码
+ (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat)size;
@end

NS_ASSUME_NONNULL_END
