//
//  NSDictionary+SCUtils.h
//  ShiHua
//
//  Created by Pingan Yi on 11/7/14.
//  Copyright (c) 2014 shuchuang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SCUtils)

- (int) sc_intForKeyPath:(NSString*)key defaultValue:(int)defaultValue;
- (float) sc_floatForKeyPath:(NSString*)key defaultValue:(float)defaultValue;

- (int) sc_intForKeyPath:(NSString*)key;
- (float) sc_floatForKeyPath:(NSString*)key;
- (NSString*) sc_stringForKeyPath:(NSString*)key;
@end
