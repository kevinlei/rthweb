///
///  NSDate+Extension.m
///  北京易知路科技有限公司
///
///  Created by apple on 14-10-18.
///  Copyright (c) 2014年 heima. All rights reserved.
///

#import "NSDate+Extension.h"

@implementation NSDate (Extension)

/**
 *  判断某个时间是否为今年
 */
- (BOOL)isThisYear
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    /// 获得某个时间的年月日时分秒
    NSDateComponents *dateCmps = [calendar components:NSCalendarUnitYear fromDate:self];
    NSDateComponents *nowCmps = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
    return dateCmps.year == nowCmps.year;
}

/**
 *  判断某个时间是否为昨天
 */
- (BOOL)isYesterday
{
    NSDate *now = [NSDate date];
    
    /// date ==  2014-04-30 10:05:28 --> 2014-04-30 00:00:00
    /// now == 2014-05-01 09:22:10 --> 2014-05-01 00:00:00
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    
    /// 2014-04-30
    NSString *dateStr = [fmt stringFromDate:self];
    /// 2014-10-18
    NSString *nowStr = [fmt stringFromDate:now];
    
    /// 2014-10-30 00:00:00
    NSDate *date = [fmt dateFromString:dateStr];
    /// 2014-10-18 00:00:00
    now = [fmt dateFromString:nowStr];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *cmps = [calendar components:unit fromDate:date toDate:now options:0];
    
    return cmps.year == 0 && cmps.month == 0 && cmps.day == 1;
}

/**
 *  判断某个时间是否为今天
 */
- (BOOL)isToday
{
    NSDate *now = [NSDate date];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    
    NSString *dateStr = [fmt stringFromDate:self];
    NSString *nowStr = [fmt stringFromDate:now];
    
    return [dateStr isEqualToString:nowStr];
}

/// 获取当前时间
+ (NSString *)currentDateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    /*
     yyyy-MM-dd HH:mm:ss.SSS
     yyyy-MM-dd HH:mm:ss
     yyyy-MM-dd
     MM dd yyyy
     */
    if (@available(iOS 13.0, *)) {
        return [formatter stringFromDate:[NSDate now]];
    } else {
        return [formatter stringFromDate:[NSDate new]];
    }
}

/// 获取当前时间字符串（yyyy-mm-dd）
- (NSString *)currentDateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    /*
     yyyy-MM-dd HH:mm:ss.SSS
     yyyy-MM-dd HH:mm:ss
     yyyy-MM-dd
     MM dd yyyy
     */
    return [formatter stringFromDate:self];
}

/// 输入开始时间和结束时间返回一个hh:mm - hh:mm的字符串
+ (NSString *)timePeriodWithStartTime:(NSString *)stratTime endTime:(NSString *)endTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"HH:mm"];
    
    return [NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:[self dateWithString:stratTime]],[formatter stringFromDate:[self dateWithString:endTime]]];
}

/// 字符串转时间(yyyy-MM-dd HH:mm:ss)
+ (NSDate *)dateWithString:(NSString *)dateString
{
    return [self dateWithString:dateString formatter:@"yyyy-MM-dd HH:mm:ss"];
}

/// 字符串转时间
+ (NSDate *)dateWithString:(NSString *)dateString formatter:(NSString *)formatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:formatter];
    
    return [dateFormatter dateFromString:dateString];
}

/// 获取当前时间的前后时间
- (NSDate *)getCurrentDateAppointDateWithDays:(int)days
{
    NSDate *appointDate; // 指定日期声明
    NSTimeInterval oneDay = 24 * 60 * 60; // 一天一共有多少秒
    appointDate = [self initWithTimeIntervalSinceNow: oneDay * days];
    return appointDate;
}

@end
