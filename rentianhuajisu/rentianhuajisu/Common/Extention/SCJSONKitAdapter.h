//
//  SCJSONKitAdapter.h
//  ShiHua
//
//  Created by Pingan Yi on 1/23/15.
//  Copyright (c) 2015 shuchuang. All rights reserved.
//

#import <Foundation/Foundation.h>

// 包装NSJSONSerialization, 提供和JSONKit类似的接口
@interface NSArray (SCJSONKitAdapter)
- (NSString*) sc_JSONString;
@end

@interface NSDictionary (SCJSONKitAdapter)
- (NSString*) sc_JSONString;
@end

// @interface NSData (SCJSONKitAdapter)
// // The NSData MUST be UTF8 encoded JSON.
// - (id)objectFromJSONData;
// @end

@interface NSString (SCJSONKitAdapter)
- (id)sc_objectFromJSONString;
@end