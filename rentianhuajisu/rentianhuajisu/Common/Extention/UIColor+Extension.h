///
///  UIColor+Extension.h
///  XinDaiBao
///
///  Created by 姚家褀 on 16/1/29.
///  Copyright © 2016年 XinDaiBao. All rights reserved.
///

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

/** * color 的一个扩展, 识别16进制色值 */
+ (UIColor *) colorWithBLString: (NSString *)color;
@end
