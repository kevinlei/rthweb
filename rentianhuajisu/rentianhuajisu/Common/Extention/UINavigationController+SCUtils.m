//
//  UINavigationController+SCUtils.m
//  ShiHua
//
//  Created by Pingan Yi on 9/28/14.
//  Copyright (c) 2014 shuchuang. All rights reserved.
//

#import "UINavigationController+SCUtils.h"

@implementation UINavigationController (SCUtils)

-(BOOL) popToViewControllerByClass: (Class)viewControllerClass animated:(BOOL)animated {
    UIViewController* targetViewController = nil;
    for (UIViewController* controller in self.viewControllers) {
        if ([controller isKindOfClass:viewControllerClass]) {
            targetViewController = controller;
        }
    }
    if (targetViewController) {
        [self popToViewController:targetViewController animated:animated];
        return YES;
    }
    return NO;
}

@end
