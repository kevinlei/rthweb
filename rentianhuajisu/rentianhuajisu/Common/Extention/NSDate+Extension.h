///
///  NSDate+Extension.h
///
///
///  Created by apple on 14-10-18.
///  Copyright (c) 2014年 heima. All rights reserved.
///

#import <Foundation/Foundation.h>

@interface NSDate (Extension)
/**
 *  判断某个时间是否为今年
 */
- (BOOL)isThisYear;
/**
 *  判断某个时间是否为昨天
 */
- (BOOL)isYesterday;
/**
 *  判断某个时间是否为今天
 */
- (BOOL)isToday;

/// 获取当前时间字符串（yyyy-mm-dd）
+ (NSString *)currentDateString;

/// 获取当前时间字符串（yyyy-mm-dd）
- (NSString *)currentDateString;

/// 字符串转时间(yyyy-MM-dd HH:mm:ss)
+ (NSDate *)dateWithString:(NSString *)dateString;

/// 输入开始时间和结束时间返回一个hh:mm - hh:mm的字符串
+ (NSString *)timePeriodWithStartTime:(NSString *)stratTime endTime:(NSString *)endTime;

/// 字符串转时间
+ (NSDate *)dateWithString:(NSString *)dateString formatter:(NSString *)formatter;

/**
 * 获取当前时间的前或者后时间（负数表示前几天，正数表述后几天）
 * @param days 天数
 */
- (NSDate *)getCurrentDateAppointDateWithDays:(int)days;

@end
