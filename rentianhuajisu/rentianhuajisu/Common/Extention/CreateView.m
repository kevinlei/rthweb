///
///  CreateView.m
///  268EDU_Demo
///
///  Created by EDU268 on 15/10/28.
///  Copyright © 2015年 edu268. All rights reserved.
///

#import "CreateView.h"

@implementation CreateView

+ (UIView *)createViewFrame:(CGRect)rect andBackgroundColor:(UIColor *)color
{
    UIView *view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = color;
    
    return view;
}
@end
