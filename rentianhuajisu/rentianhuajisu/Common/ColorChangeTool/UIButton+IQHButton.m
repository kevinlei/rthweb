//
//  UIButton+IQHButton.m
//  iqhiPad
//
//  Created by 启航龙图.在线事业部.李东 on 2019/3/1.
//  Copyright © 2019 爱启航. All rights reserved.
//

#import "UIButton+IQHButton.h"

@implementation UIButton (IQHButton)

//- (instancetype)initWithType:(IQHButtonType)buttonTYpe normalTitle:(NSString *)normalTitle titleFont:(CGFloat)titleFont cornerRadius:(NSInteger)cornerRadius {
//    if (self = [self init]) {
//        [self setTitle:normalTitle forState:UIControlStateNormal];
//        self.titleLabel.font = [UIFont systemFontOfSize:titleFont];
//        self.layer.cornerRadius = cornerRadius;
//        switch (buttonTYpe) {
//            case IQHButtonTypeLine:
//            {
//                [self setBackgroundColor:[UIColor whiteColor]];
////                [self setTitleColor:IQHConstants.color_3A4A5E forState:UIControlStateDisabled];
//                [self setTitleColor:[UIColor baseColor] forState:UIControlStateNormal];
//                if (self.enabled) {
//                    self.layer.borderColor = [UIColor baseColor].CGColor;
//                    self.layer.borderWidth = 0.5;
//                }else{
//                    self.layer.borderColor = IQHConstants.color_FF6C02.CGColor;
//                    self.layer.borderWidth = 0.5;
//                }
//            }
//                break;
//            case IQHButtonTypeBG:
//            {
//                self.backgroundColor = [UIColor baseColor];
//                [self addTarget:self action:@selector(IQHbuttonBackGroundHighlighted:) forControlEvents:UIControlEventTouchDown];
//                [self addTarget:self action:@selector(IQHbuttonBackGroundNormal:) forControlEvents:UIControlEventTouchUpInside];
//                //            [button setBackgroundImage:[self imageWithColor:[[UIColor baseColor] colorWithAlphaComponent:0.3]] forState:UIControlStateDisabled];
//            }
//                break;
//            case IQHButtonTypeCancel:
//            {
//                [self setTitleColor:IQHConstants.color_3A4A5E forState:UIControlStateNormal];
//                [self setTitleColor:IQHConstants.color_B7BABF forState:UIControlStateDisabled];
//                self.layer.borderColor = IQHConstants.color_FF6C02.CGColor;
//                self.layer.borderWidth = 0.5;
//                self.layer.masksToBounds = YES;
//            }
//                break;
//            case IQHButtonTypeDefault:
//            {
//                [self setBackgroundColor:[UIColor whiteColor]];
//                [self setTitleColor:IQHConstants.color_3A4A5E forState:UIControlStateNormal];
//            }
//                break;
//            default:
//                break;
//        }
//    }
//    return self;
//}
//
//- (void)IQHbuttonBackGroundNormal:(UIButton *)sender
//{
//    sender.backgroundColor = [UIColor baseColor];
//}
//
////  button1高亮状态下的背景色
//- (void)IQHbuttonBackGroundHighlighted:(UIButton *)sender
//{
//    sender.backgroundColor = IQHConstants.color_E15E0E;
//}

- (void)setImagePosition:(IQHImagePosition)postion spacing:(CGFloat)spacing {
    CGFloat imageWith = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
    CGFloat labelWidth = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].width;
    CGFloat labelHeight = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].height;
    
    //image中心移动的x距离
    CGFloat imageOffsetX = labelWidth / 2 ;
    //image中心移动的y距离
    CGFloat imageOffsetY = labelHeight / 2 + spacing / 2;
    //label中心移动的x距离
    CGFloat labelOffsetX = imageWith/2;
    //label中心移动的y距离
    CGFloat labelOffsetY = imageHeight / 2 + spacing / 2;
    
    switch (postion) {
        case IQHImagePositionLeft:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing/2, 0, spacing/2);
            self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing/2, 0, -spacing/2);
            break;
            
        case IQHImagePositionRight:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth + spacing/2, 0, -(labelWidth + spacing/2));
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(imageHeight + spacing/2), 0, imageHeight + spacing/2);
            break;
            
        case IQHImagePositionTop:
            self.imageEdgeInsets = UIEdgeInsetsMake(-imageOffsetY, imageOffsetX, imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(labelOffsetY, -labelOffsetX, -labelOffsetY, labelOffsetX);
            break;
            
        case IQHImagePositionBottom:
            self.imageEdgeInsets = UIEdgeInsetsMake(imageOffsetY, imageOffsetX, -imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(-labelOffsetY, -labelOffsetX, labelOffsetY, labelOffsetX);
            break;
            
        default:
            break;
    }
    
}

/**根据图文距边框的距离调整图文间距*/
- (void)setImagePosition:(IQHImagePosition)postion WithMargin:(CGFloat )margin{
    CGFloat imageWith = self.imageView.image.size.width;
    CGFloat labelWidth = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].width;
    CGFloat spacing = self.bounds.size.width - imageWith - labelWidth - 2*margin;
    
    [self setImagePosition:postion spacing:spacing];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:backgroundColor];
    [button setTitle:normalTitle forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = titleFont;
    if (cornerRadius > 0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    return button;
}

+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:backgroundColor];
    [button setTitle:normalTitle forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = titleFont;
    if (cornerRadius > 0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    if (borderWidth > 0) {
        button.layer.borderColor = titleColor.CGColor;
        button.layer.borderWidth = borderWidth;
    }
    
    return button;
}


@end
