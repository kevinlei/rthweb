//
//  HDOceanMainNavBar.h
//  HDOceanMainNavBar
//
//  Created by xiaogongzhu on 2022/3/28.
//  Copyright © 2022 YuanBo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HDOceanMainNavBar : UIView
+ (HDOceanMainNavBar *)createNavBarWithType:(NSString*)type title:(NSString*)title;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign)BOOL isSelecte1;
@property (nonatomic, strong)UIButton *leftBtn;
@property (nonatomic, strong)UIButton *leftBtn1;
@property (nonatomic, strong)UIButton *leftBtn2;
@end

NS_ASSUME_NONNULL_END
