//
//  HDOceanMainNavBar.m
//  HDOceanMainNavBar
//
//  Created by xiaogongzhu on 2022/3/28.
//  Copyright © 2022 YuanBo. All rights reserved.
//

#import "HDOceanMainNavBar.h"

@implementation HDOceanMainNavBar
+ (HDOceanMainNavBar *)createNavBarWithType:(NSString*)type title:(NSString*)title{
    HDOceanMainNavBar * view = [[HDOceanMainNavBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [[UIApplication sharedApplication] statusBarFrame].size.height+44)];
    view.type = type;
    view.title = title;
    [view setupViews];
    return view;
    
}
-(void)buttonClick{
//    if (self.isSelecte1) {
//        [self.leftBtn1 setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
//        self.leftBtn1.titleLabel.font = [UIFont systemFontOfSize:21 weight:UIFontWeightMedium];
//
//        [self.leftBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
//        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
//    }else{
//        [self.leftBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
//        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:21 weight:UIFontWeightMedium];
//
//        [self.leftBtn1 setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
//        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
//    }
//
//    self.isSelecte1 = !self.isSelecte1;
    
}
-(void)setupViews{
    self.backgroundColor = [UIColor whiteColor];
    if ([self.type isEqualToString:@"3"]) {
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setTitle:@"视频秀" forState:UIControlStateNormal];
        [leftBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        leftBtn.titleLabel.font = [UIFont systemFontOfSize:21 weight:UIFontWeightMedium];
        [self addSubview:leftBtn];
        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).mas_offset(15);
            make.bottom.equalTo(self.mas_bottom).offset(-3);
    //        make.width.mas_equalTo(50);
            make.height.mas_equalTo(30);
        }];
    }else if ([self.type isEqualToString:@"4"]){
        self.isSelecte1 = YES;
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setTitle:@"消息" forState:UIControlStateNormal];
        [leftBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        leftBtn.titleLabel.font = [UIFont systemFontOfSize:21 weight:UIFontWeightMedium];
        [self addSubview:leftBtn];
        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).mas_offset(15);
            make.bottom.equalTo(self.mas_bottom).offset(-3);
    //        make.width.mas_equalTo(50);
            make.height.mas_equalTo(30);
        }];
        self.leftBtn = leftBtn;
        [self.leftBtn addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *leftBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn1 setTitle:@"通话" forState:UIControlStateNormal];
        [leftBtn1 setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        leftBtn1.titleLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
        [self addSubview:leftBtn1];
        [leftBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftBtn.mas_right).mas_offset(15);
            make.bottom.equalTo(self.mas_bottom).offset(-3);
    //        make.width.mas_equalTo(50);
            make.height.mas_equalTo(30);
        }];
        self.leftBtn1 = leftBtn1;
        [self.leftBtn1 addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        leftBtn1.hidden = YES;
    }
    
//    [self.leftBtn bk_whenTapped:^{
//        [[HDUtils getCurrentVC].navigationController popViewControllerAnimated:YES];
//    }];
    
//    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.leftBtn setImage:[OldProjectBundle imageNamed:@"IQH_vip_back"] forState:UIControlStateNormal];
//    [self addSubview:self.leftBtn];
//    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_left).mas_offset(15);
//        make.bottom.equalTo(self.mas_bottom).offset(-3);
//        make.width.mas_equalTo(24);
//        make.height.mas_equalTo(44);
//    }];
//    [self.leftBtn bk_whenTapped:^{
//        [[HDUtils getCurrentVC].navigationController popViewControllerAnimated:YES];
//    }];
//
//    UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.text = self.title;
//    titleLabel.textColor = [UIColor generateDynamicColor:[UIColor colorWithHexString:@"#000000"] darkColor:[UIColor colorWithHexString:@"#FFFFFF"]];
//    titleLabel.font = [UIFont boldSystemFontOfSize:18];
//    [self addSubview:titleLabel];
//    self.titleLabel = titleLabel;
//    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.mas_centerX);
//        make.centerY.equalTo(self.leftBtn.mas_centerY);
//        make.height.mas_equalTo(30);
//    }];
//
//    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rightButton setTitleColor:IQH_title_color(title_FF6C02_FF6C02) forState:UIControlStateNormal];
//    rightButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
//    self.rightBtn = rightButton;
//    [self addSubview:self.rightBtn];
//    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_right).mas_offset(-15);
//         make.centerY.equalTo(self.leftBtn.mas_centerY);
//        make.width.mas_equalTo(60);
//        make.height.mas_equalTo(44);
//    }];
//    [[rightButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
//        [self.rightSubject sendNext:@""];
//    }];
//
//    UIView *singleLine = [UIView new];
//    singleLine.hidden = YES;
//    self.singleLine = singleLine;
//    singleLine.backgroundColor = [UIColor generateDynamicColor: [UIColor colorWithHexString:@"#EEEEEE"] darkColor: [UIColor colorWithHexString:@"#2C2F34"]];
//    [self addSubview:singleLine];
//    self.singleLine = singleLine;
//    [self.singleLine mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.right.left.equalTo(self);
//        make.height.mas_equalTo(@1);
//    }];
//
//    switch (self.type) {
//        case IQHeacherVIPBarHomeType:{
//             [self.rightBtn setTitle:@"收藏夹" forState:UIControlStateNormal];
//        }break;
//        case IQHeacherVIPBarPostType:{
//            [self.leftBtn setImage:[OldProjectBundle imageNamed:@"IQH_vip_navClose"] forState:UIControlStateNormal];
//            self.singleLine.hidden = NO;
//            self.rightBtn.hidden = NO;
//            [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.right.equalTo(self.mas_right).mas_offset(-15);
//                make.centerY.equalTo(self.leftBtn.mas_centerY);
//                make.width.mas_equalTo(44);
//                make.height.mas_equalTo(24);
//            }];
//        }break;
//        case IQHeacherVIPBarEvaluateType:{
//            self.singleLine.hidden = NO;
//            [self.leftBtn setImage:[OldProjectBundle imageNamed:@"IQH_vip_navClose"] forState:UIControlStateNormal];
//        }break;
//        case IQHeacherVIPBarNormalType:{
//            self.rightBtn.hidden = YES;
//
//        }break;
//        default:{}break;
//    }
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
