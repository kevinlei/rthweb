//
//  UIButton+IQHButton.h
//  iqhiPad
//
//  Created by 启航龙图.在线事业部.李东 on 2019/3/1.
//  Copyright © 2019 爱启航. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, IQHButtonType) {
    IQHButtonTypeLine,      //边框
    IQHButtonTypeBG,        //背景
    IQHButtonTypeCancel,    //取消
    IQHButtonTypeDefault    //默认
};

typedef NS_ENUM(NSInteger, IQHImagePosition) {
    IQHImagePositionLeft     = 0,            //图片在左，文字在右，默认
    IQHImagePositionRight    = 1,            //图片在右，文字在左
    IQHImagePositionTop      = 2,            //图片在上，文字在下
    IQHImagePositionBottom   = 3,            //图片在下，文字在上
};

@interface UIButton (IQHButton)

/**
 * @brief 初始化button.
 *
 * @param normalTitle 默认title.
 *
 * @param titleFont 字体.
 *
 * @param titleColor 默认字体颜色.
 *
 * @param backgroundColor 背景色.
 *
 * @param cornerRadius 圆角.
 *
 * @return UIButton.
 */
+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius;

/**
 * @brief 初始化button.
 *
 * @param normalTitle 默认title.
 *
 * @param titleFont 字体.
 *
 * @param titleColor 默认字体颜色.
 *
 * @param backgroundColor 背景色.
 *
 * @param cornerRadius 圆角.
 *
 * @param borderWidth 边框宽度.
 *
 * @return UIButton.
 */
+ (instancetype)buttonWithNormalTitle:(NSString *)normalTitle titleFont:(UIFont *)titleFont normalTitleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth;

/**
 * @brief 初始化button.
 *
 * @param buttonTYpe 按钮类型.
 *
 * @param normalTitle 默认title.
 *
 * @param titleFont 字体大小.
 *
 * @param cornerRadius 圆角.
 *
 * @return UIButton.
 */
- (instancetype)initWithType:(IQHButtonType)buttonTYpe normalTitle:(NSString *)normalTitle titleFont:(CGFloat)titleFont cornerRadius:(NSInteger)cornerRadius;

/**
 * @brief 设置button中按钮的位置.
 *
 * @param spacing 偏移量.
 */
- (void)setImagePosition:(IQHImagePosition)postion spacing:(CGFloat)spacing;

/**
 * @brief 根据图文距边框的距离调整图文间距.
 *
 * @param margin 间隔.
 */
- (void)setImagePosition:(IQHImagePosition)postion WithMargin:(CGFloat )margin;

@end

NS_ASSUME_NONNULL_END
