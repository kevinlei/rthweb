//
//  ZKWKWebViewController.h
//  StudentKudou
//
//  Created by 赵坪生 on 2017/5/11.
//  Copyright © 2017年 赵坪生. All rights reserved.
//

#import <WebKit/WebKit.h>
typedef NSDictionary * (^WebViewShareBlock)();

@interface ZKWKWebViewController : SUIRootViewController

/**
 网络加载
 */
@property (nonatomic, strong)NSString *url;

/**
 本地加载
 */
@property (nonatomic, strong)NSString *html;
@property (nonatomic, assign)BOOL isShowSave;

@property (nonatomic, copy) WebViewShareBlock shareOptions;
@property (nonatomic, copy) NSString *myTitle;
- (WKWebView *)getWkwebView;

- (void)share;

@property (nonatomic, strong)NSDictionary *savaDict;
@end
