//
//  MEUtils.h
//  mechanic
//
//  Created by HDOceandeep on 2021/9/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SUIUtils : NSObject
+ (UIViewController *)topViewController;

+ (UIViewController *)topViewController:(UIViewController *)rootViewController;

+ (void) makeShortToastAtCenter: (NSString*)toast;
+ (void) makeShortToastAtBottom: (NSString*)toast;
+ (void) makeShortToastWindowAtCenter: (NSString*)toast;
+ (void) makeShortToastWindowAtBottom: (NSString*)toast;

+ (UIWindow*) getDefaultWindow;
//字典转json格式字符串：
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

//json格式字符串转字典：
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString ;
+ (NSString *)jsonStringWithDict:(NSDictionary *)dict;
+ (NSString *)getCurrentDeviceUUID;
+ (CGFloat)sizoOfCache;
+(NSString *)convertToJsonStr:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
