//
//  AppDelegate.h
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/19.
//

#import <UIKit/UIKit.h>
#import "SUIBaseTabBarController.h"
@interface TIANAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSMutableArray *users;
@property (nonatomic, strong)SUIBaseTabBarController *mianvc;
+ (TIANAppDelegate *)shareAppDelegate;
- (UIViewController*) getCurrentVC;
- (UIViewController*) getCurrentUIVC;
-(void)showChat;
-(void)resetTab;
@end

