//
//  HDDataManager.h
//  HDDataManager
//
//  Created by HDOceandeep on 2022/3/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MKZHDDataManager : NSObject
+ (instancetype)shareManager;

-(void)saveMoneyWithData:(NSDictionary*)dict;
-(void)saveIncomeWithData:(NSDictionary*)income;
-(void)saveOutcomeWithData:(NSDictionary*)outcome;

-(NSArray*)getIncomeArray;

-(NSArray*)getHomeArray;
-(NSArray*)getBillArrayWithTimeSort:(BOOL)isNormalTime year:(NSString*)year month:(NSString*)month;
-(NSArray*)getMineArray;
- (NSString *)getDateMonth;
- (NSString *)getDateYear;

-(NSArray*)getCofigureOutComeAndIncomeWithIsMonth:(BOOL)isMonth;

-(NSArray*)getAllRegistUser;
-(void)registUserWithData:(NSDictionary*)dict;

-(void)deleteUserWithData:(NSString*)phone;
-(NSArray*)getHomeData;
-(void)saveCaigouWithData:(NSDictionary*)dict;
-(NSArray*)getCaigouData;

-(void)savefabushuiguoWithData:(NSDictionary*)dict;
-(NSArray*)getFabushuiguoData;
@end

NS_ASSUME_NONNULL_END
