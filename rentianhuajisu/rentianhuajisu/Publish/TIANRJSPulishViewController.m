//
//  RJSPulishViewController.m
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/20.
//

#import "TIANRJSPulishViewController.h"
#import "HDFankuiView.h"
#import <WebKit/WebKit.h>
@interface TIANRJSPulishViewController ()<WKUIDelegate>
@property(nonatomic,strong)  HDFankuiView *headerView;
@end

@implementation TIANRJSPulishViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = YES;
//    [[AppDelegate shareAppDelegate] showChat];
 
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
        
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            completionHandler();
//        }]]];
        
        [self presentViewController:alertController animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame];
    webView.UIDelegate = self;
//    webView.show
    [self.view addSubview:webView];
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"riskCalculator" withExtension:@"html"];
    NSURL *baseURL = [[NSBundle mainBundle] bundleURL];
//    NSURL *htmlURLs = [[NSBundle mainBundle] URLForResource:@"riskCalculator" withExtension:@"js"];

    [webView loadFileURL:htmlURL allowingReadAccessToURL:baseURL];
    
//    return;
//    SUIWKWebViewController* _chatVC = [SUIWKWebViewController new];
////    _chatVC.isChat = YES;
//    _chatVC.getWkwebView.scrollView.scrollEnabled = false;
//    _chatVC.url = @"http://lixi.zuhedaikuan.com/40.html";
//    [self.view addSubview:_chatVC.view];
//    [self addChildViewController:_chatVC];
    
    UIImageView * topBGb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zhuanfa"]];
//    topBGb.backgroundColor = [UIColor colorWithHexString:@"zhuanfa"];
    [self.view addSubview:topBGb];
    topBGb.userInteractionEnabled = YES;
    [topBGb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-100);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.height.width.mas_equalTo(80);
    }];
    [topBGb bk_whenTapped:^{
        if([USERPHONE length] == 0){
            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
            [self.navigationController pushViewController:vc animated:YES];
            [SUIUtils makeShortToastAtCenter:@"请先登录"];
            return;
        }
        
        //分享的标题
        // 当前app的信息
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

    //     CFShow(i(__bridge CFTypeRef)(infoDictionary));

        // app名称

        NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        NSString *textToShare = app_Name;
        //分享的图片
        
        UIImage *imageToShare = [UIImage imageNamed:@"mylogo"];;
        //分享的url
    //        NSURL *urlToShare = [NSURL URLWithString:@"http://www.baidu.com"];

        NSArray *activityItems = @[textToShare,imageToShare];

        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];

        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];

        [self presentViewController:activityVC animated:YES completion:nil];

        //分享之后的回调
        activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
            if (completed) {
                NSLog(@"completed");
                //分享 成功
            } else  {
                NSLog(@"cancled");
                //分享 取消
            }
        };
    }];
//    WKWebView *webView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//       // 设置访问的URL
//       NSURL *url = [NSURL URLWithString:@"https://kf25.niapp.top/index/index/home?visiter_id=&visiter_name=&avatar=&groupid=0&business_id=1"];
//       // 根据URL创建请求
//       NSURLRequest *request = [NSURLRequest requestWithURL:url];
//       // WKWebView加载请求
//       [webView loadRequest:request];
//       // 将WKWebView添加到视图
//       [self.view addSubview:webView];
//    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top).offset(0);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(0);
//        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
//    }];
   
    // Do any additional setup after loading the view.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
