//
//  WJUITextView.m
//  ZYQAssetPickerControllerDemo
//
//  Created by 小孩子科技 on 16/6/13.
//  Copyright © 2016年 heroims. All rights reserved.
//

#import "WJTextView.h"

@interface WJTextView ()
@property (nonatomic, strong) UILabel *placeHolderLabel;
@end

@implementation WJTextView

CGFloat const UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION = 0.25;


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Use Interface Builder User Defined Runtime Attributes to set
    // placeholder and placeholderColor in Interface Builder.
    if (!self.placeholder) {
        [self setPlaceholder:@""];
    }
    
    if (!self.placeholderColor) {
        [self setPlaceholderColor:[UIColor colorWithRed:0.63f green:0.63f blue:0.63f alpha:1.00f]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
        
        self.placeholderFont = [UIFont systemFontOfSize: 15];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    //    CGSize size;
    if (/*[Util isAfterIOS7]*/NO) {
        //        NSMutableDictionary *attr = [NSMutableDictionary dictionary];
        //        attr[NSFontAttributeName] = self.placeholderFont;
        //        size = [self.placeholder sizeWithAttributes: attr];
        //        _placeHolderLabel.frame = CGRectMake(8, 9, self.bounds.size.width - 16, size.height);
    } else {
        //        size = [self.placeholder sizeWithFont: self.placeholderFont constrainedToSize: CGSizeMake(self.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attribute2 = @{NSFontAttributeName: [UIFont systemFontOfSize:18]};
        CGSize size =[self.placeholder boundingRectWithSize:CGSizeMake(self.bounds.size.width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine |
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading attributes:attribute2 context:nil].size;
        _placeHolderLabel.frame = CGRectMake(8, 9, self.bounds.size.width - 16, size.height);
    }
    
}

- (void)textChanged:(NSNotification *)notification
{
    if([[self placeholder] length] == 0)
    {
        return;
    }
    
    [UIView animateWithDuration:UI_PLACEHOLDER_TEXT_CHANGED_ANIMATION_DURATION animations:^{
        if([[self text] length] == 0)
        {
            [[self viewWithTag:999] setAlpha:1];
        }
        else
        {
            [[self viewWithTag:999] setAlpha:0];
        }
    }];
    if(self.text.length==0){
        self.textColor=[UIColor colorWithRed:0.19f green:0.19f blue:0.19f alpha:1.00f];
    }
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
}

- (void)setPlaceholderFont:(UIFont *)placeholderFont
{
    _placeholderFont = placeholderFont;
    self.placeHolderLabel.font = placeholderFont;
    [self setNeedsDisplay];
}

- (UILabel *)placeHolderLabel
{
    if (_placeHolderLabel == nil) {
        _placeHolderLabel = [[UILabel alloc] init];
        _placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _placeHolderLabel.numberOfLines = 0;
        _placeHolderLabel.font = self.placeholderFont;
        
        _placeHolderLabel.backgroundColor = [UIColor clearColor];
        _placeHolderLabel.textColor = self.placeholderColor;
        _placeHolderLabel.alpha = 0;
        _placeHolderLabel.tag = 999;
        [self addSubview:_placeHolderLabel];
    }
    
    return _placeHolderLabel;
}


- (void)drawRect:(CGRect)rect
{
    if( [[self placeholder] length] > 0 )
    {
        _placeHolderLabel.text = self.placeholder;
        [self sendSubviewToBack:_placeHolderLabel];
    }
    
    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    
    [super drawRect:rect];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
