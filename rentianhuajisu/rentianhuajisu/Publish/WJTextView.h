//
//  WJUITextView.h
//  ZYQAssetPickerControllerDemo
//
//  Created by 小孩子科技 on 16/6/13.
//  Copyright © 2016年 heroims. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WJTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, strong) UIFont *placeholderFont;

-(void)textChanged:(NSNotification*)notification;

@end
