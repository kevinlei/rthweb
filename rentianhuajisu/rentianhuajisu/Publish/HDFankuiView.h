//
//  HDFankuiView.h
//  SafetyTrainProject
//
//  Created by 董学雷 on 2018/10/16.
//  Copyright © 2018年 kevinxuelei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WJTextView.h"
#import "HXPhotoPicker.h"
NS_ASSUME_NONNULL_BEGIN

@interface HDFankuiView : UIView
@property (weak, nonatomic) IBOutlet WJTextView *TF;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet WJTextView *tf1;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;

@end

NS_ASSUME_NONNULL_END
