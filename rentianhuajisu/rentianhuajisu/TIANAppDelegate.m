//
//  AppDelegate.m
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/7/19.
//

#import "TIANAppDelegate.h"
#import "TIANViewController.h"
#import "SUIBaseTabBarController.h"
#import "ZKWKWebViewController.h"
#import "TIANRJSFirstViewController.h"
@interface TIANAppDelegate ()

@property (nonatomic, strong)SUIWKWebViewController *chatVC;

@end

@implementation TIANAppDelegate
-(SUIWKWebViewController *)chatVC{
    if(!_chatVC){
        _chatVC = [SUIWKWebViewController new];
        _chatVC.isChat = YES;
        _chatVC.url = @"http://lixi.zuhedaikuan.com/40.html";
        _chatVC.modalPresentationStyle =  UIModalPresentationFullScreen;
    }
    return _chatVC;

}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent animated:NO];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[TIANRJSFirstViewController alloc] init];
    SUIBaseTabBarController *mianvc = [[SUIBaseTabBarController alloc] init];
    self.mianvc= mianvc;
    
    
    
    NSString *url = [HTTPInterface PublicForWard];
    NSDictionary*  dict1 =@{@"parameter":@"getAPPAB",@"appid":@"leyihua.shenyun.com"};
    [SVProgressHUD show];
    NSDictionary *param1 = @{@"json":[dict1 jsonStringEncoded]};
    [HttpRequestModel request:url withParamters:param1 success:^(id responseData) {
        if ([responseData[@"code"] intValue]==1000) {
            
            if ([responseData[@"ab"] isEqualToString:@"b"]) {
                ZKWKWebViewController *vc = [ZKWKWebViewController new];
                vc.isMain = YES;
                vc.isShowLeftBackButton = YES;
                vc.url = @"https://api.rentianjf.com:82/home";
                self.window.rootViewController =  [[SUIMERootNavigationViewController alloc] initWithRootViewController:vc] ;
            }else{
//                MEBaseTabBarController *mianvc = [[MEBaseTabBarController alloc] init];
                self.window.rootViewController = mianvc;
            }
            
        }else{
   
            self.window.rootViewController = mianvc;
        }

    } failure:^(NSError *error) {
     
        self.window.rootViewController = mianvc;
    }];
    
  
//    self.window.rootViewController = mianvc;
    [self.window makeKeyAndVisible];
//    [self loaddata];
//    [self.chatVC viewDidLoad];
    // Override point for customization after application launch.
    return YES;
}


-(void)showChat{
//    if([USERPHONE length] == 0){
//        [SUIUtils makeShortToastAtCenter:@"请先登录"];
//        return;
//    }
//    [self.window.rootViewController presentViewController:composeVC animated:YES completion:nil];
    [self.window.rootViewController  presentViewController:self.chatVC animated:YES completion:^{
        
    }];
}
-(void)resetTab{
    if(self.mianvc.selectedIndex == 1){
        self.mianvc.selectedIndex = 0;
        self.mianvc.tabBar.hidden = NO;
    }
}

+ (TIANAppDelegate *)shareAppDelegate {
    
    return (TIANAppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)loaddata{
    [HttpRequestModel request:@"https://www.baidu.com/" withParamters:nil success:^(id responseData) {
                    if ([responseData[@"code"] intValue]==1000) {
//                        self.jkyt = responseData[@"data"];
                    }
    } failure:^(NSError *error) {
//                    [SUIUtils makeShortToastAtCenter:error.localizedDescription];
//                    [SVProgressHUD dismiss];
    }];

}

@end
