//
//  RTHHomeViewController.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/1.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "TIANRJSHomeViewController.h"
#import "UIButton+ImageTitleStyle.h"
#import "RTHHomeItemView.h"
#import "CalculatorViewController.h"
#import "YouViewController.h"
#import "HomeView.h"
#import "TIANHDPeiyuAboutUsViewController.h"


//#import "RTHGetMoneyViewController.h"
//#import "RTHOrderDetailViewController.h"
//#import "RTHRebackMoneyViewController.h"
//#import "RTHMyBillViewController.h"
//#import "RTHUncomeBillViewController.h"
//#import "RTHBillClearViewController.h"
//#import "RTHVerifyIndentiyViewController.h"
//#import "RTHAboutViewController.h"
//#import "RTHFankuiViewController.h"
#import "XWDreamHomeTableViewCell.h"
#import "ZKWKWebViewController.h"
#import "WYLMortgageCalculatorViewController.h"
#import "RTHSectionView.h"
#import "XWDreamHomeTableViewCell.h"
//#import "RTHMyOrderViewController.h"
@interface TIANRJSHomeViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) UIView *bgView2;
@property(nonatomic,strong) UIView *bgView3;
@property(nonatomic,strong) UIView *bgView4;
@end

@implementation TIANRJSHomeViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)showProgress{
    [SVProgressHUD show];
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}
#pragma mark 返回分组数
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   return 0;
}

#pragma mark 返回每组行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return 0;
}


#pragma mark cell视图
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    XWDreamHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"XWDreamHomeTableViewCell" forIndexPath:indexPath];
    [cell getMyNewHomeCell];
    cell.contentView.layer.masksToBounds = YES;
    cell.contentView.layer.cornerRadius = 8;
    NSDictionary *dict = self.dataArray[indexPath.section];
    cell.iconImageView.image = [UIImage imageNamed:dict[@"image"]];
    cell.titleLabel.text = dict[@"title"];
    cell.bottomLabel.text = dict[@"time"];
    NSInteger previousTime = [[NSUserDefaults standardUserDefaults] integerForKey:dict[@"title"]];
    if(!previousTime){
        previousTime = 0;
        cell.rightLabel.text = @"去阅读";
    }else{
        cell.rightLabel.text = @"继续阅读";
    }
    
    cell.studyLabel.text = [NSString stringWithFormat:@"阅读时长:%lds",(long)previousTime];
//    cell.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
//    NSArray *section =self.dataArray[indexPath.section];
//    cell.dict = [section objectAtIndex:indexPath.row];
   return cell;
}

#pragma mark 设置cell高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  90;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 15;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *vc =[UIView new];
    vc.backgroundColor = [UIColor clearColor];
    return vc;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.dataArray[indexPath.section];
            ZKWKWebViewController *vc = [ZKWKWebViewController new];
            vc.savaDict = dict;
            vc.isShowSave = YES;
            vc.myTitle = dict[@"title"];
            NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
            vc.html = str1;
            [self.navigationController pushViewController:vc animated:YES];
//    if([dict[@"urltype"] isEqualToString:@"url"]){
//        ZKWKWebViewController *vc = [ZKWKWebViewController new];
//        vc.savaDict = dict;
//        vc.isShowSave = YES;
//        vc.myTitle = dict[@"title"];
//        NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
//        vc.url = dict[@"content"];
//        [self.navigationController pushViewController:vc animated:YES];
//    }else{
//        ZKWKWebViewController *vc = [ZKWKWebViewController new];
//        vc.savaDict = dict;
//        vc.isShowSave = YES;
//        vc.myTitle = dict[@"title"];
//        NSString * str1 = [NSString stringWithFormat:@"<div>%@</div&gt",dict[@"content"]];
//        vc.html = str1;
//        [self.navigationController pushViewController:vc animated:YES];
//    }
    
   
}
-(void)loadData{
    self.dataArray = [NSMutableArray array];
    
    NSDictionary *dict0 = @{@"title":@"纽约证券交易所",
                            @"time":@"1792-5-17",
                            @"type":@"1",
                            @"image":@"1",
                            @"urltype":@"html",
                            @"content":@"在美国证券发行之初，尚无集中交易的证券交易所，证券交易大都在咖啡馆和拍卖行里进行，纽约证券交易所的起源可以追溯到1792年5月17日，当时24个证券经纪人在纽约华尔街68号外一棵梧桐树下签署了梧桐树协议，协议规定了经纪人的“联盟与合作”规则，通过华尔街现代老板俱乐部会员制度交易股票和高级商品，这也是纽约交易所的诞生日。1817年3月8日这个组织起草了一项章程，并把名字更改为“纽约证券交易委员会”。1863年改为现名“纽约证券交易所”。从1868年起，只有从当时老成员中买得席位方可取得成员资格。纽约证券交易所的第一个总部是1817年一间月租200美金，位于华尔街40号的房间。1865年交易所才拥有自己的大楼。坐落在纽约市华尔街11号的大楼是1903年启用的。交易所内设有主厅、蓝厅、“车房”等三个股票交易厅和一个债券交易厅，是证券经纪人聚集和互相交易的场所，共设有十六个交易亭，每个交易亭有十六至二十个交易柜台，均装备有现代化办公设备和通讯设施。交易所经营对象主要为股票，其次为各种国内外债券。除节假日外，交易时间每周五天，每天五小时。自20世纪20年代起，它一直是国际金融中心，这里股票行市的暴涨与暴跌，都会在其他资本主义国家的股票市场产生连锁反应，引起波动。它还是纽约市最受欢迎的旅游名胜之一。1934年10月1日，交易所向美国证券交易委员会注册为一家全国性证券交易所，有一位主席和33位成员的董事会，1971年2月18日，非营利法人团体正式成立，董事会成员的数量减少到25位。2006年6月1日，纽约证券交易所宣布与泛欧证券交易所合并组成纽约证交所－泛欧证交所公司。1股纽约证交所的股票换成1股新公司股票，泛欧证交所股东以1股泛欧证交所股票换取新公司的0.98股股票和21.32欧元现金。新公司总部设在纽约。1953年起，成员限定为1366名。只有盈利250万美元（税前）、最低发行售出股票100万股、给普通股东以投票权并定期公布财务的公司，其股票才有资格在交易所挂牌。至1999年2月，交易所的日均交易量达亿股，交易额约达300亿美元。截至1999年2月，在交易所上市的公司已超过3000家，其中包括来自48个国家的385家外国公司，在全球资本市场上筹措资金超过10万亿。另外，美国政府、公司和外国政府、公司及国际银行的数千种债券也在交易所上市交易。"};
    [self.dataArray addObject:dict0];
    
    
    NSDictionary *dicta = @{@"title":@"东京证券交易所",
                            @"time":@"1878-5-15",
                            @"type":@"2",
                            @"image":@"2",
                            @"urltype":@"html",
                            @"content":@"它在1878年5月15日创立，同年6月1日开始交易，创立时的名称为「东京股票交易所」（日文：东京株式取引所）。二次大战时曾暂停交易，1949年5月16日重开，并更名为东京证券交易所。东京证券交易所的交易楼层於1999年4月30日关闭，全部改为电子交易，而新的TSE Arrows（东证アローズ）於2000年5月9日启用。东京证券交易所的前身是1879年5月曾成立东京证券交易株式会社。由于当时日本经济发展缓慢，证券交易不兴旺，1943年6月，日本政府合并所有证券交易所，成立了半官方的日本证券交易所，但成立不到四年就解体了。二次大战前，日本的资本主义虽有一定的发展，但由于军国主义向外侵略，重工业、兵器工业均由国家垄断经营，纺织、海运等行业也由国家控制，这是一种战争经济体制并带有浓厚的军国主义色彩。那时，即使企业发行股票，往往也被同一财阀内部的企业所消化。因此，证券业务难以发展。日本战败后，1946年在美军占领下交易所解散。1949年1月美国同意东京证券交易所重新开业。随着日本战后经济的恢复和发展，东京证券交易所也发展繁荣起来。东京证券交易所有上市公司1777家，其中外国公司 110家，市场资本总额将近45000亿美元。长期以来，大量的公众储蓄都依赖金融机构进行间接投资，使证券交易具有整批性、数量大等特点，同时也造成发行市场狭小，仅面向少数金融机构，使股票、债券的发行市场形成抽象的无形市场，与发达的流通市场相比较落后。70年代以来，日本经济实力大增，成为世界经济强国。为适应日本经济结构和经济发展的国际化需要，日本证券市场的国际化成为必然趋势。为此，日本政府自70年代以来全面放宽外汇管制，降低税率，以鼓励外国资金进入日本证券市场，使国际资本在东京证券市场的活动日益频繁。1988年，日本政府允许外国资本在东京进入场外交易；1989年，又允许外国证券公司进入东京证券交易所，使东京证券交易所在国际上的地位大大提高。"};
    [self.dataArray addObject:dicta];
    
    
    
    NSDictionary *dictb = @{@"title":@"伦敦证券交易所",
                            @"time":@"1986-10",
                            @"type":@"3",
                            @"image":@"3",
                            @"urltype":@"html",
                            @"content":@"伦敦证券交易所是世界四大证券交易所之一，作为世界上最国际化的金融中心，伦敦不仅是欧洲债券及外汇交易领域的全球领先者，还受理超过三分之二的国际股票承销业务。伦敦的规模与位置，意味着它为世界各地的公司及投资者提供了一个通往欧洲的理想门户。在保持伦敦的领先地位方面，伦敦证券交易所扮演着中心角色，它运作世界上国际最强的股票市场，其外国股票的交易超过其它任何证交所。作为世界第三大证券交易中心，伦敦证券交易所是世界上历史最悠久的证券交易所。伦敦证券交易所曾为当时英国经济的兴旺立下汗马功劳，但随着英国国内和世界经济形势的变化，其浓重的保守色彩，特别是沿袭的陈规陋习严重阻碍了英国证券市场的发展，影响厂中场竞争力，在这一形势下，伦敦证券交易所于1986年10月进行了重大改革，例如，改革固定佣金制；允许大公司直接进入交易所进行交易；放宽对会员的资格审查；允许批发商与经纪人兼营：证券交易全部实现电脑化，与纽约、东京交易所连机，实现24小时全球交易。这些改革措施使英国证券市场发生了根本性的变化，巩固了其在国际证券市场中的地位。伦敦的外国股票交易额始终高于其它市场。这反映了外国公司在伦敦证券交易所业务中的中心地位—在伦敦证券交易所交易的外国股票远远超出英国本土的股票，这种情形是独一无二的。在伦敦，外国股票的平均日交易额达到195亿美元，远远高于其它任何主要证交所。伦敦市场的规模、威望和全球操作围，意味着在这里上市和交易的外国公司可获得全球瞩目和覆盖。 伦敦证券交易所面向外国股票的交易及信息系统包括国际股票自动对盘交易系统和EAQ国际股票自动报价系统。伦敦证券交易所的国际股票自动对盘交易系统是一套由订单驱动的全自动交易设施。它有助于提高在伦敦证券交易所交易量最大的外国公司的知名度，并以其速度、透明度和效率促进这些公司股票的交易。 SEAQ国际股票自动报价系统是一套由报价驱动的基于屏幕上的交易及信息系统，已成为在伦敦上市的外国公司与其投资者之间出色的沟通渠道，并已成为该领域的衡量标准。在增进上市公司的股票在其本土市场、伦敦市场以及世界其它金融中心的流动性方面，SEAQ国际股票自动报价系统发挥着关键作用。"};
    [self.dataArray addObject:dictb];
    
    
    NSDictionary *dict1 = @{@"title":@"全球金融体系的分类及特点",
                            @"time":@"2023-01-13",
                            @"type":@"4",
                            @"image":@"4",
                            @"urltype":@"html",
                            @"content":@"首先，金融市场是指以金融资产为交易对象，以金融资产的供给方和需求方为交易主体形成的交易机制及其关系的总和。按照金融市场交易标的物划分是全球金融市场最常见的划分方法。按这一划分标准，金融市场分为货币市场、资本市场、外汇市场、金融衍生品市场、保险市场、黄金市场及其他投资品市场。1、货币市场 货币市场又称“短期金融市场”“短期资金市场”，是指融资期限在一年及一年以下的金融市场。可以看出货币市场的期限一般都比较短，流动性高。货币市场主要包括同业拆借市场、票据市场、回购市场和货币市场基金等。(1)同业拆借市场又被称为“同业拆放市场”，是指金融机构之间以货币借贷方式进行短期资金融通活动的市场。同业拆借市场最早出现于美国，其形成的根本原因在于法定存款准备金制度的实施。(2)票据市场指的是在商品交易和资金往来过程中产生的以汇票、本票和支票的发行、担保、承兑、贴现、转贴现、再贴现来实现短期资金融通的市场。(3)回购市场指通过回购协议进行短期资金融通交易的市场。回购是指资金融入方在出售证券时，与证券的购买商签订协议，约定在一定期限后按约定的价格购回所卖证券，从而获得即时可用资金的一种交易行为。在银行间债券市场上回购业务可以分为质押式回购业务、买断式回购业务和开放式回购业务三种类型。 (4)货币市场基金(简称“MMF”)是指投资于货币市场上短期(一年以内，平均期限120天)有价证券的一种投资基金。2、资本市场 资本市场亦称“长期金融市场”“长期资金市场”，是指期限在一年以上的各种资金借贷和证券交易的场所。资本市场一般偏长期。资本市场主要包括股票市场、债券市场和基金市场等。3、外汇市场 外汇市场是指经营外币和以外币计价的票据等有价证券买卖的市场。外汇市场具有高杠杆、双向交易、极高流动性、24小时交易等特点4、金融衍生品市场 金融衍生品是指以杠杆或信用交易为特征，在传统金融产品(货币、债券、股票等)的基础上派生出来的，具有新价值的金融工具，如期货、期权、互换及远期等。以上就是按照交易标的对全球金融体系分类和特点的简单介绍。"};
    [self.dataArray addObject:dict1];
    
    NSDictionary *dict2 = @{
        @"time":@"2023-01-20",@"type":@"5",
        @"image":@"5",@"urltype":@"html",@"title":@"金融风险的内涵及特征",@"content":@"所谓风险是指资产及其收益蒙受损失的可能性。金融风险作为风险的范畴之一，本质上也是一种引起损失的可能性，具体指的是经济主体在从事资金融通过程中遭受损失的可能性。金融风险是客观存在的，按所涉及的范围，可以划分为微观金融风险和宏观金融风险。前者指的是微观金融活动主体在其金融活动和管理过程中发生资产损失或收益损失的可能性。这种可能性一旦转化为现实，就会使该金融主体发生某笔甚至大量金融资产沉淀和亏损，资本金被侵蚀，甚至因资不抵债而破产。后者指的是整个金融体系面临的风险，当可能性变为现实时，宏观金融风险即演变为金融危机，对整个社会经济生活产生深刻的影响。 一般来说，当风险仅停留在少数机构的微观层面时，该风险并不对宏观经济运行构成明显影响，但是，当某一或某些金融机构发生的倒闭是大批金融机构发生倒闭的前兆，那么该微观风险就会演化为宏观经济风险。因此，宏观与微观两个层面的风险有着密切联系，这一联系既表现为内在的一致性。又存在着对立的成分。 两个层次的一致性表现为当宏观金融风险加深时，大部分微观企业的金融风险局势也趋恶化；反之，当许多金融机构或一些大的金融机构发生严重亏损时，宏观金融风险也会加剧，甚至爆发金融危机。但是，它们彼此独立，甚至有相对立的成分，微观层面的某些避险行为，如资金撤离高风险国家与地区，会加大这些地区的宏观金融风险，触发金融危机，或使之进一步恶化。反过来。为避免金融危机发生或加剧的宏观政策措施，也会给微观企业的运作带来一定的风险，如政府突然放弃固定汇率制（如东南亚金融危机中的泰国、韩国)，或突然宣布实行外汇管制措施（如东南亚金融危机中的马来西亚)，以及突然改变以前对国债管理做出的承诺（如俄罗斯）等等。这些政策措施可能有助于在短期内控制宏观金融风险的进一步恶化，但却给一些企业带来了难以规避的风险。这些矛盾冲突在近年来的国际金融动荡中时有表现。在本文我们主要讨论宏观金融风险。但同时我们要深刻认识到金融风险在宏观层面与微观层面的既相一致又相矛盾冲突的辩证关系，在决策中更多地注意协调各经济主体之间的利益，制定更为全面与有效的风险管理措施，避免金融风险向金融危机转化。1.客观性。只要有金融活动存在，金融风险就不以人的意志为转移而客观存在。因为：(1)市场经济主体具有有限理性。由于市场信息的非对称性和主体对客观认识的有限性，市场经济主体作出的决策可能不是及时、全面和可靠的，有时甚至是错误的，从而在客观上可能导致金融风险的产生。 (2)市场经济主体具有机会主义倾向。人类的天性有一种道德上的冒险精神和趋利避害动机，因而某些人可能采用不正当的手段，诸如说谎、欺骗、违背承诺以及尽可能钻制度、政策的空子以谋取私利。投机、冒险和各种钻营行为的客观存在导致金融风险不可避免。(3)经济环境的不确定性。经济生活中的不确定性是始终存在的。"};
    [self.dataArray addObject:dict2];
    
    NSDictionary *dict3 = @{
        @"time":@"2022-12-13",@"type":@"6",
        @"image":@"6",@"urltype":@"html",@"title":@"关于金融的基本知识大全",@"content":@"一、金融学入门可以参考哪些书籍？金融学入门的书籍很多，学姐主要整理了以下自己学习金融时参考的书籍：1、曼昆《经济学原理》 曼昆《经济学：人们怎么样做出决策、人们如何进行互相交易、整体经济如何运行，这三大问题可运用书中的十大原理进行回答。 2、米什金《货币金融学》 米什金《货币金融学》主要介绍为什么要研究银行、货币和金融市场、经济体系、金融机构、利率、金融市场、衍生金融工具、货币需求、通货膨胀和通货紧缩、金融体系等等基础的金融知识。 3、滋维.博迪《投资学》 滋维.博迪《投资学》主要介绍投资相关的基础知识，包括：投资环境、资产类别、金融工具、风险资产配置、资本市场均衡、国定收益证券、期权、期货与其他衍生证券、应用投资组合管理等等。 4、约翰·赫尔《期权期货及其他衍生产品》约翰·赫尔《期权期货及其他衍生产品》囊括了金融衍生产品的绝大部分知识，主要介绍的内容有：波动率微笑、利率、在险值、奇异期权、利率期货、数值方法、期货市场的机制、期货套期保值策略、远期和期货价格的决定、互换、期权市场的机制、期权的交易策略、Black-Scholes-Merton模型、信用风险、信用衍生产品、凸性调整和实物期权等。。8、金融市场与衍生工具  金融市场的含义、构成要素、地位与功能、融资方式以及分类、衍生金融工具概述及常见的衍生金融工具。 9、外汇与国际收支外汇概述、功能、汇率及其标价方法、影响汇率变动的因素以及国际收支的含义、国际收支平衡表、国际收支的调节。 10、金融风险与金融监管  金融风险的含义和特点、成因、影响和分类、金融监管的含义、必要性、我国金融监管的现状、国际金融监管的发展趋势、国际“巴塞尔协议”。  来源：韩宗英、王玮薇《金融基础知识》 总结来说，金融学入门可以参考曼昆《经济学原理》、米什金《货币金融学》、滋维.博迪《投资学》等书籍，具体的金融学入门可以参考哪些书籍，可以参照上述内容，另外，金融学入门主要需要掌握金融的基础知识。"};
    [self.dataArray addObject:dict3];
    
    NSDictionary *dict4 = @{
        @"image":@"7",
        @"time":@"2022-04-12",@"type":@"7",  @"urltype":@"html",@"title":@"投资理财",@"content":@"投资者要进行投资理财，需先开立相应的投资理财账户。为防范投资者陷入投资理财误区，造成财产、精神上的损失，投资者在开立储蓄、保险、股票、债券、外汇、期货、黄金等投资理财账户时应到符合国家监管要求的正规金融机构办理。我国金融机构主要分为银行、保险及证券三大组成部分，但从投资理财途径上看，证券公司能为投资者提供较多的投资理财途径及申请更多类别的投资理财账户。而部分理财工具（如债券）也可通过多种渠道（银行或证券公司）开立投资理财账户。  一般而言，银行方面，通过银行开立的投资理财账户可以办理储蓄类产品和银行理财产品以及基金类产品，大型银行还可通过银行系统购买国债。由于银行网点分布较广，通过银行渠道开立的投资理财账户可到银行柜台办理。保险公司方面，通过保险公司开立的投资理财账户可以购买寿险、财产险等投资理财产品。证券公司方面，通过证券公司申请开立的投资理财账户可运用股票（包括A股、B股、H股等）、债券（包括国债、企业债、公司债等）、期货（包括金融期货如股指期货、外汇期货等，商品期货如黄金期货、农产品期货等）等一系列的投资理财工具进行投资理财。证券账户的开立可到各证券公司营业部办理，需要在交易日内办理。部分证券公司可通过各省份网站进行网络预约，通过预约的开户时间则相应较为灵活，可支持周六日开户。由于我国第三方理财发展尚未成熟,我国对第三方理财机构的监管仍然接近空白,既没有明确的监管部门,也没有针对性的监管制度,更没有监管细则。  第三方理财在界定和规范上属于法律真空。如今市场上的第三方理财机构多以“理财顾问公司”,“投资咨询公司”抑或“财富管理中心”的名义运作,具体又分为两种模式:其一是只提供理财咨询,其二是既可提供咨询也可代客理财。由于我国没有对应的法律部门或者法规对第三方理财机构进行监管,上海财经大学金融学院霍文文教授在接受媒体采访时就表示,很多没有合法法律地位的私募基金就会打着第三方理财机构的名义进行代客理财。  众多第三方理财公司的身份不被监管层认可,使许多正规公司也处于尴尬地位。所谓“身正也怕影子歪”,正规公司盼监管之切也就不足为奇了。第三方的优势是独立性,但是这种独立性或只是“看上去很美”。受托方或因专业知识有限,不可能知晓所有信息,也就是在受托方和接受委托方之间存在信息不对称。中国经济网记者查阅了百度给出的定义:信息不对称是指基于双方信息不对称,从事经济活动的一方在最大限度地增进自身效用的同时做出不利于他人的行动。 上海财经大学教师何韧曾表示:“受托方也就是第三方理财机构很有可能利用专业技术和信息的优势侵害投资者的利益。”如果是只提供理财规划建议的机构,很有可能因为利益上的牵连而跨越其所宣扬的“中立”。而对于帮助客户进行资产管理的第三方理财机构,就更有可能因投资能力不佳而让投资者受到损失。另外,很多理财机构操作不规范,缺少必要的定期信息披露机制,投资人的利益也难以获得稳妥的保障。"};
    [self.dataArray addObject:dict4];
    
    [self.dataArray reverse];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main" bundle: nil];
    ViewController *vc  = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self.view addSubview:vc.view];
    [self addChildViewController:vc];
    
    UIImageView * topBGb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zhuanfa"]];
//    topBGb.backgroundColor = [UIColor colorWithHexString:@"zhuanfa"];
    [self.view addSubview:topBGb];
    topBGb.userInteractionEnabled = YES;
    [topBGb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-100);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.height.width.mas_equalTo(80);
    }];
    [topBGb bk_whenTapped:^{
        if([USERPHONE length] == 0){
            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
            [self.navigationController pushViewController:vc animated:YES];
            [SUIUtils makeShortToastAtCenter:@"请先登录"];
            return;
        }
        
        //分享的标题
        // 当前app的信息
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

    //     CFShow(i(__bridge CFTypeRef)(infoDictionary));

        // app名称

        NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        NSString *textToShare = app_Name;
        //分享的图片
        
        UIImage *imageToShare = [UIImage imageNamed:@"mylogo"];;
        //分享的url
    //        NSURL *urlToShare = [NSURL URLWithString:@"http://www.baidu.com"];

        NSArray *activityItems = @[textToShare,imageToShare];

        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];

        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];

        [self presentViewController:activityVC animated:YES completion:nil];

        //分享之后的回调
        activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
            if (completed) {
                NSLog(@"completed");
                //分享 成功
            } else  {
                NSLog(@"cancled");
                //分享 取消
            }
        };
    }];
    
    
    
    
    return;
    [self loadData];
    self.view.backgroundColor = rgb(243, 246, 254);
    UIImageView * topBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    topBG.backgroundColor = [UIColor colorWithHexString:@"6699FF"];
    [self.view addSubview:topBG];
    topBG.userInteractionEnabled = YES;
    [topBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(0);
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(NavigationbarHeight+20);
    }];


    UILabel * Label = [UILabel new];
    Label.textAlignment = NSTextAlignmentCenter;
    Label.text = @"仁天花";
    Label.textColor = [UIColor whiteColor];
    Label.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    [topBG addSubview:Label];
    [Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(topBG.mas_bottom).offset(-20);
        make.centerX.mas_equalTo(topBG.mas_centerX).offset(-0);
    }];
    
    
//    UILabel * subLabel = [UILabel new];
//    subLabel.textAlignment = NSTextAlignmentLeft;
//    subLabel.text = @"年轻尽享信用分期生活";
//    subLabel.textColor = [UIColor whiteColor];
//    subLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
//    [topBG addSubview:subLabel];
//    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(Label.mas_centerY).offset(0);
//        make.left.mas_equalTo(Label.mas_right).offset(kMargin);
//    }];
    
    UIButton *qianbaoButton12 = [UIButton buttonWithType:UIButtonTypeCustom];
    [qianbaoButton12 setImage:[UIImage imageNamed:@"客服"] forState:UIControlStateNormal];
    [topBG addSubview:qianbaoButton12];
    qianbaoButton12.hidden  = YES;
    [qianbaoButton12 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(Label.mas_centerY).offset(0);
        make.right.mas_equalTo(topBG.mas_right).offset(-15);
        make.height.width.mas_equalTo(15);
    }];
    [qianbaoButton12 bk_whenTapped:^{
        NSMutableString * phoneStr=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"028-38289396"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
    }];
    
//    UIScrollView *scrollview = [[UIScrollView alloc] init];
////    scrollview.backgroundColor = [UIColor redColor];
////    scrollview.frame = CGRectMake(0, 0, 414, 280);
//    [self.view addSubview:scrollview];
//    [scrollview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(topBG.mas_bottom).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(0);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
//    }];
//
//    scrollview.contentSize = CGSizeMake(0, 1800);
    

//    homeView.frame = CGRectMake(0, 0, kScreenWidth, 1800);

   

    

   

//    UIView *bgView = [UIView new];
//    bgView.backgroundColor = [UIColor whiteColor];
//    bgView.layer.masksToBounds = YES;
//    bgView.layer.cornerRadius = 12;
//    [self.view addSubview:bgView];
//    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(Label.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
//        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
//        make.height.mas_equalTo(160);
//    }];
    
    
//    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStylePlain)];
//    tableView.layer.masksToBounds = YES;
//    tableView.layer.cornerRadius = 16;
//    tableView.backgroundColor = [UIColor clearColor];
//    self.tableView = tableView;
//    self.tableView.rowHeight = 80;
//    tableView.delegate = self;
//    tableView.dataSource = self;
//    tableView.showsVerticalScrollIndicator  = NO;
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.view addSubview:tableView];
//    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(topBG.mas_bottom).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(0);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
//    }];
//    [tableView reloadData];
//    [self showProgress];
//    [self.tableView registerClass:[XWDreamHomeTableViewCell class] forCellReuseIdentifier:@"XWDreamHomeTableViewCell"];
//    
//    HomeView *homeView  = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([HomeView class]) owner:self options:nil] firstObject];
//    homeView.frame = CGRectMake(0, 0, kScreenWidth, 800);
//    tableView.tableHeaderView = homeView;
//    homeView.banner.userInteractionEnabled = YES;
//    
//    homeView.help.userInteractionEnabled = YES;
//    homeView.tousu.userInteractionEnabled = YES;
//    homeView.guanyu.userInteractionEnabled = YES;
//    homeView.zhaopian.userInteractionEnabled = YES;
//    
//    [homeView.tousu bk_whenTapped:^{
//        if([USERPHONE length] == 0){
//            TIANYTXRegistrationVC *vc = [TIANYTXRegistrationVC new];
//            [self.navigationController pushViewController:vc animated:YES];
//            [SUIUtils makeShortToastAtCenter:@"请先登录"];
//            return;
//        }
//        TIANRTHFankuiViewController *vc = [TIANRTHFankuiViewController new];
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
//    
//    [homeView.guanyu bk_whenTapped:^{
//        [self.navigationController pushViewController:[TIANHDPeiyuAboutUsViewController new] animated:YES];
//      
//    }];
//    
//    [homeView.help bk_whenTapped:^{
//        [[TIANAppDelegate  shareAppDelegate] showChat];
//    }];
//    
//    [homeView.zhaopian bk_whenTapped:^{
//        [[TIANAppDelegate  shareAppDelegate] showChat];
//    }];
//    
//    [homeView.banner bk_whenTapped:^{
//        NSMutableString * phoneStr=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"028-38289396"];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneStr]];
//    }];
    
    

    
//    UIImageView * topBG12 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"矩形 3"]];
//    [self.view addSubview:topBG12];
//    topBG12.userInteractionEnabled = YES;
//    [topBG12 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top).offset(130);
//        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
//        make.height.mas_equalTo(80);
//        make.width.mas_equalTo(240);
//    }];
//
//    UILabel * Labelmoney1 = [UILabel new];
//    Labelmoney1.textAlignment = NSTextAlignmentCenter;
//    Labelmoney1.text = @"最高可借额度";
//    Labelmoney1.textColor = [UIColor colorWithHexString:@"#24B6F6"];
//    Labelmoney1.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
//    [topBG12 addSubview:Labelmoney1];
//    [Labelmoney1 mas_makeConstraints:^(MASConstraintMaker *make) {
////        make.top.mas_equalTo(bgView.mas_top).offset(60);
//        make.centerX.mas_equalTo(topBG12.mas_centerX).offset(0);
//        make.centerY.mas_equalTo(topBG12.mas_centerY).offset(-5);
////        make.height.mas_equalTo(40);
//    }];

    
//    UILabel * Labelmoney1 = [UILabel new];
//    Labelmoney1.textAlignment = NSTextAlignmentCenter;
//    Labelmoney1.text = @"最高可借额度（元）";
//    Labelmoney1.textColor = [UIColor colorWithHexString:@"#000000"];
//    Labelmoney1.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
//    [bgView addSubview:Labelmoney1];
//    [Labelmoney1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(bgView.mas_left).offset(15);
//        make.top.mas_equalTo(bgView.mas_top).offset(15);
//    }];
//
//
//    UILabel * Labelmoney = [UILabel new];
//    Labelmoney.textAlignment = NSTextAlignmentLeft;
//    Labelmoney.text = @"￥200,000";
//    Labelmoney.textColor = [UIColor colorWithHexString:@"#000000"];
//    Labelmoney.font = [UIFont systemFontOfSize:37 weight:UIFontWeightBold];
//    [bgView addSubview:Labelmoney];
//    [Labelmoney mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(bgView.mas_left).offset(15);
//        make.top.mas_equalTo(Labelmoney1.mas_bottom).offset(20);
//        make.height.mas_equalTo(40);
//        make.width.mas_equalTo(200);
//    }];
//
//    NSString *string = [NSString stringWithFormat:@"￥%@",@"200,000"];
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
//    NSRange range1 = [[str string] rangeOfString:@"￥"];
//    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15 weight:UIFontWeightMedium] range:range1];
//    NSRange range2 = [[str string] rangeOfString:@"200,000"];
//    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:37 weight:UIFontWeightBold] range:range2];
//    Labelmoney.attributedText = str;
//
//
//    UIButton *qianbaoButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
//    [qianbaoButton1 setTitle:@"我要借款" forState:UIControlStateNormal];
//    [qianbaoButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    qianbaoButton1.titleLabel.font =[UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
//    qianbaoButton1.layer.masksToBounds = YES;
//    qianbaoButton1.layer.cornerRadius = 20;
//    qianbaoButton1.backgroundColor = [UIColor colorWithHexString:APP_tab_Color];
////    [qianbaoButton1 setBackgroundImage:[UIImage imageNamed:@"矩形"] forState:UIControlStateNormal];
//    [bgView addSubview:qianbaoButton1];
//    [qianbaoButton1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(Labelmoney.mas_centerY).offset(0);
//        make.right.mas_equalTo(bgView.mas_right).offset(-15);
//        make.height.mas_equalTo(40);
//        make.width.mas_equalTo(110);
//    }];
//
//    [qianbaoButton1 bk_whenTapped:^{
////        if([USERTOKEN length] == 0){
////            [self.navigationController pushViewController:[YTXRegistrationVC new] animated:YES];
////            return;
////        }
////        RTHGetMoneyViewController *vc =[RTHGetMoneyViewController
////                                        new];
//
//
//
////        ZKWKWebViewController *vc = [ZKWKWebViewController new];
////        vc.myTitle = @"申请条件";
////        vc.html = @"03-申请条件";
//
////        RTHVerifyIndentiyViewController *vc = [RTHVerifyIndentiyViewController new];
//
////        RTHRebackMoneyViewController *vc = [RTHRebackMoneyViewController new];
//
////        RTHRebackMoneyViewController *vc = [RTHRebackMoneyViewController new];
////
////        RTHMyBillViewController *vc = [RTHMyBillViewController new];
//
//
////        RTHFankuiViewController *vc = [RTHFankuiViewController new];
//
////        RTHBillClearViewController*vc = [RTHBillClearViewController new];
////        [self.navigationController pushViewController:vc animated:YES];
//    }];
//
//    //单行布局 不需要考虑换行的问题
//        CGFloat marginX =10;  //按钮距离左边和右边的距离
//        CGFloat marginY =10;  //按钮距离布局顶部的距
//        CGFloat toTop =50;  //布局区域距离顶部的距离
//        CGFloat gap =marginX;    //按钮与按钮之间的距离
//        NSInteger col =3;    //这里只布局5列
//        NSInteger count =3;  //这里先设置布局5个按钮
//        CGFloat viewWidth = self.view.bounds.size.width - 10 *2;  //视图的宽度
//        CGFloat viewHeight = 50;  //视图的高度
//        CGFloat itemWidth = (viewWidth - marginX *(count + 1))/col*1.0f;  //根据列数 和 按钮之间的间距 这些参数基本可以确定要平铺的按钮的宽度
//        CGFloat height = itemWidth * 1;  //按钮的高度可以根据情况设定 这里设置为相等
//    height = 50;
//        UIButton*last =nil;  //上一个按钮
//        //准备工作完毕 既可以开始布局了
//        for(int i =0; i < count; i++) {
////            UIButton*item = [UIButton new];
////            [item setTitleColor:[UIColor colorWithHexString:@"#000000"] forState:UIControlStateNormal];
////            item.titleLabel.font = [UIFont systemFontOfSize:14];
////            [item setImage:[UIImage imageNamed:@"形状"] forState:UIControlStateNormal];
////            [item setBackgroundImage:[UIImage imageNamed:@"矩形 1"] forState:UIControlStateNormal];
//////            item.backgroundColor = [UIColor redColor];
////            [item setTitle:@"通过率高" forState:UIControlStateNormal];
////            [bgView addSubview:item];
////
////            [item setButtonImageTitleStyle:ButtonImageTitleStyleCenterBottom padding:0];
//
//            RTHHomeItemView *item = [[[NSBundle mainBundle] loadNibNamed:@"RTHHomeItemView" owner:nil options:nil] lastObject];
//            item.icon.image = [UIImage imageNamed:@"对号"];
////            [item setTitle:@"通过率高" forState:UIControlStateNormal];
//            [bgView addSubview:item];
//            if(i == 0){
//                item.label.text = @"通过率高";
////                item.icon.image  = [UIImage imageNamed:@"形状 1"];
//            }
//            if(i == 1){
//                item.label.text = @"利率超低";
////                item.icon.image  = [UIImage imageNamed:@"形状 1"];
//            }
//            if(i == 2){
//                item.label.text = @"极速放款";
////                item.icon.image  = [UIImage imageNamed:@"形状"];
//            }
//
//            //布局
//            [item mas_makeConstraints:^(MASConstraintMaker*make) {
//                //宽高是固定的，前面已经算好了
//                make.width.mas_equalTo(itemWidth);
//                make.height.mas_equalTo(height);
//                //topTop距离顶部的距离，单行不用考虑太多，多行，还需要计算距离顶部的距离
//                make.top.mas_equalTo(qianbaoButton1.mas_bottom).offset(15);
//                if(!last) {  //last为nil 说明是第一个按钮
//                    make.left.mas_offset(marginX);
//                }else{
//                    //第二个或者后面的按钮 距离前一个按钮右边的距离都是gap个单位
//                    make.left.mas_equalTo(last.mas_right).mas_offset(gap);
//                }
//            }];
//            last = item;
//        }
//
//
    
    
    
    //bg2
    
//    UIView *bgView2 = [UIView new];
//    bgView2.backgroundColor = [UIColor whiteColor];
//    bgView2.layer.masksToBounds = YES;
//    bgView2.layer.cornerRadius = 12;
//    [self.view addSubview:bgView2];
//    self.bgView2 = bgView2;
////    self.bgView2 = bgView2;
//    [bgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(Label.mas_bottom).offset(20);
//        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
//        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
//        make.height.mas_equalTo(100);
//    }];
//    [self createJiugongge2];
//    
//    
//    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.frame style:(UITableViewStyleGrouped)];
////        tableView.layer.masksToBounds = YES;
////        tableView.layer.cornerRadius = 16;
//    tableView.backgroundColor = [UIColor clearColor];
//    self.tableView = tableView;
//    self.tableView.rowHeight = 80;
//    tableView.delegate = self;
//    tableView.dataSource = self;
//    tableView.showsVerticalScrollIndicator  = NO;
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.view addSubview:tableView];
//    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
//        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
////        make.right.left.mas_equalTo(self.view).offset(0);
//        make.top.mas_equalTo(bgView2.mas_bottom).offset(20);
//        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
//    }];
//    [tableView reloadData];
//    [self showProgress];
//    [self.tableView registerClass:[XWDreamHomeTableViewCell class] forCellReuseIdentifier:@"XWDreamHomeTableViewCell"];
//
//[self loadData];
//[self.tableView reloadData];
    
    
//    RTHSectionView*sectionView = [[[NSBundle mainBundle] loadNibNamed:@"RTHSectionView" owner:nil options:nil] lastObject];
//    sectionView.titleLabel.text = @"热门功能";
//    [self.view addSubview:sectionView];
//    [sectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(bgView2.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(-0);
//        make.height.mas_equalTo(30);
//    }];
//
//
//    UIView *bgView3 = [UIView new];
//    bgView3.backgroundColor = [UIColor clearColor];
//    bgView3.layer.masksToBounds = YES;
//    bgView3.layer.cornerRadius = 12;
//    [self.view addSubview:bgView3];
//    self.bgView3 = bgView3;
////    self.bgView2 = bgView2;
//    [bgView3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(sectionView.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
//        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
//        make.height.mas_equalTo(150);
//    }];
//    [self createJiugongge3];
//
//
//    RTHSectionView*sectionView1 = [[[NSBundle mainBundle] loadNibNamed:@"RTHSectionView" owner:nil options:nil] lastObject];
//    sectionView1.titleLabel.text = @"新手必读";
//    [self.view addSubview:sectionView1];
//    [sectionView1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(bgView3.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(-0);
//        make.height.mas_equalTo(30);
//    }];
//
//    UIView *bgView4 = [UIView new];
//    bgView4.backgroundColor = [UIColor whiteColor];
//    bgView4.layer.masksToBounds = YES;
//    bgView4.layer.cornerRadius = 12;
//    [self.view addSubview:bgView4];
//    self.bgView4 = bgView4;
////    self.bgView2 = bgView2;
//    [bgView4 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(sectionView1.mas_bottom).offset(15);
//        make.left.mas_equalTo(self.view.mas_left).offset(kMargin);
//        make.right.mas_equalTo(self.view.mas_right).offset(-kMargin);
//        make.height.mas_equalTo(90);
//    }];
//    [self createJiugongge4];

    // Do any additional setup after loading the view.
}

- (void) createJiugongge4 {
    NSArray *titles = @[@"完善信息",@"极速审核",@"立即放款"];
    int padding = 0;
    float width = (CGRectGetWidth(self.view.bounds) - 40) / titles.count;
    int count = 1;
    for(int i = 0; i < titles.count; i++) {
        for(int j = 0; j < 1; j++) {
            UIView *view = [UIView new];
            view.userInteractionEnabled = YES;
            view.backgroundColor = [UIColor clearColor];
            [self.bgView4 addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.bgView4).offset(padding+(padding+width)*i);
                make.centerY.mas_equalTo(self.bgView4.mas_centerY).offset(15+(padding+width)*j);
                make.width.and.height.mas_equalTo(width);
            }];
            
            view.userInteractionEnabled = YES;
            
            [view bk_whenTapped:^{
               
//                if(i  ==  0){
                    ZKWKWebViewController *vc = [ZKWKWebViewController new];
                    vc.myTitle = titles[i];
                    vc.html = @"03-申请条件";
                    [self.navigationController pushViewController:vc animated:YES];
//                }
               
            }];
            

            UIImageView *l = [UIImageView new];
            l.image = [UIImage imageNamed:titles[i]];

            [view addSubview:l];

            [l mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(view.mas_centerX);
                make.centerY.mas_equalTo(view.mas_centerY).offset(-25);
                make.height.width.mas_equalTo(36);
            }];
            
            UILabel *lable = [UILabel new];
            lable.text = titles[i];


            lable.textAlignment = NSTextAlignmentCenter;

            lable.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];

            lable.textColor = [UIColor colorWithHexString:@"#333333"];

            [view addSubview:lable];

            [lable mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(view.mas_centerX);
                make.top.mas_equalTo(l.mas_bottom).offset(8);
                make.height.mas_equalTo(21);
            }];

            

            count++;

        }

    }

}

- (void) createJiugongge2 {
    NSArray *titles = @[@"计算器",@"汇率计算",@"房贷计算器"];
    int padding = 0;
    float width = (CGRectGetWidth(self.view.bounds) - 40) / titles.count;
    int count = 1;
    for(int i = 0; i < titles.count; i++) {
        for(int j = 0; j < 1; j++) {
            UIView *view = [UIView new];
            view.userInteractionEnabled = YES;
            view.backgroundColor = [UIColor clearColor];
            [self.bgView2 addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.bgView2).offset(padding+(padding+width)*i);
                make.centerY.mas_equalTo(self.bgView2.mas_centerY).offset(15+(padding+width)*j);
                make.width.and.height.mas_equalTo(width);
            }];
            
            view.userInteractionEnabled = YES;
            
            [view bk_whenTapped:^{
                
                if(i == 0){
//                    if([USERTOKEN length] == 0){
//                        [self.navigationController pushViewController:[YTXRegistrationVC new] animated:YES];
//                        return;
//                    }
                    [[TIANAppDelegate  shareAppDelegate] showChat];
                }
                
                if(i == 1 ){
//                    if([USERTOKEN length] == 0){
//                        [self.navigationController pushViewController:[YTXRegistrationVC new] animated:YES];
//                        return;
//                    }
                    CalculatorViewController *vc =[CalculatorViewController
                                                    new];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
                if(i == 2 ){
                    WYLMortgageCalculatorViewController *mainViewController = [WYLMortgageCalculatorViewController new];
                    [self.navigationController pushViewController:mainViewController animated:YES];
                }
                
                

               
            
                if(i  ==  3){
//                    WYLMortgageCalculatorViewController *mainViewController = [WYLMortgageCalculatorViewController new];
//                    [self.navigationController pushViewController:mainViewController animated:YES];
                }
               
            }];
            

            UIImageView *l = [UIImageView new];
//            l.image = [UIImage imageNamed:titles[i]];

            [view addSubview:l];
            
            if(i == 0){
                l.image = [UIImage imageNamed:@"借款记录"];
            }
            if(i == 1){
                l.image = [UIImage imageNamed:@"极速借款"];
            }
            if(i == 2){
                l.image = [UIImage imageNamed:@"利息计算器"];
            }

            [l mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(view.mas_centerX);
                make.centerY.mas_equalTo(view.mas_centerY).offset(-25);
                make.height.width.mas_equalTo(55);
            }];
            
            UILabel *lable = [UILabel new];
            lable.text = titles[i];


            lable.textAlignment = NSTextAlignmentCenter;

            lable.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];

            lable.textColor = [UIColor colorWithHexString:@"#333333"];

            [view addSubview:lable];

            [lable mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(view.mas_centerX);
                make.top.mas_equalTo(l.mas_bottom).offset(8);
                make.height.mas_equalTo(21);
            }];

            

            count++;

        }

    }

}

- (void) createJiugongge3 {
    NSArray *titles = @[@"借款指南",@"还款指南",@"防骗提醒"];
    int padding = 10;
    float width = (CGRectGetWidth(self.view.bounds) - 40) / titles.count;
    int count = 1;
    for(int i = 0; i < titles.count; i++) {
        for(int j = 0; j < 1; j++) {
            UIView *view = [UIView new];
            view.userInteractionEnabled = YES;
            view.backgroundColor = [UIColor clearColor];
            [self.bgView3 addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.bgView3).offset(+(padding+width)*i);
                make.top.mas_equalTo(self.bgView3.mas_top).offset(0);
                make.bottom.mas_equalTo(self.bgView3.mas_bottom).offset(-0);
                make.width.mas_equalTo(width);
            }];
            
            view.userInteractionEnabled = YES;
            
            [view bk_whenTapped:^{
               
            
                if(i  ==  0){
                    ZKWKWebViewController *vc = [ZKWKWebViewController new];
                    vc.myTitle = titles[i];
                    vc.html = @"03-申请条件";
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
                if(i  ==  1){
                    ZKWKWebViewController *vc = [ZKWKWebViewController new];
                    vc.myTitle = titles[i];
                    vc.html = @"04-提额";
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
                if(i  ==  2){
                    ZKWKWebViewController *vc = [ZKWKWebViewController new];
                    vc.myTitle = titles[i];
                    vc.html = @"09-关于我们";
                    [self.navigationController pushViewController:vc animated:YES];
                }
               
            }];
            

            UIImageView *l = [UIImageView new];
            l.image = [UIImage imageNamed:titles[i]];

            [view addSubview:l];

            [l mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerX.mas_equalTo(view.mas_centerX);
//                make.centerY.mas_equalTo(view.mas_centerY).offset(-25);
//                make.height.width.mas_equalTo(55);
                make.left.right.top.bottom.mas_equalTo(view);
            }];
            
//            UILabel *lable = [UILabel new];
//            lable.text = titles[i];
//
//
//            lable.textAlignment = NSTextAlignmentCenter;
//
//            lable.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
//
//            lable.textColor = [UIColor colorWithHexString:@"#333333"];
//
//            [view addSubview:lable];
//
//            [lable mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerX.mas_equalTo(view.mas_centerX);
//                make.top.mas_equalTo(l.mas_bottom).offset(8);
//                make.height.mas_equalTo(21);
//            }];

            

            count++;

        }

    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
