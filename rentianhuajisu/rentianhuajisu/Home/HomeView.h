//
//  HomeView.h
//  rentianhuajisu
//
//  Created by 董学雷 on 2023/8/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *banner;

@property (weak, nonatomic) IBOutlet UIImageView *help;
@property (weak, nonatomic) IBOutlet UIImageView *guanyu;
@property (weak, nonatomic) IBOutlet UIImageView *zhaopian;



@property (weak, nonatomic) IBOutlet UIImageView *tousu;


@end

NS_ASSUME_NONNULL_END
