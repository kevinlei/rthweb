//
//  RTHHomeItemView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/1.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHHomeItemView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *bg;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

NS_ASSUME_NONNULL_END
