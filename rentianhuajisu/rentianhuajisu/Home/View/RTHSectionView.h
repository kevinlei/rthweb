//
//  RTHSectionView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/7/12.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHSectionView : UIView
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
