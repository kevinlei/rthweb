//
//  RTHVerifyIdView.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/1.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHVerifyIdView.h"

@implementation RTHVerifyIdView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.bg1.layer.masksToBounds = YES;
    self.bg1.layer.cornerRadius = 4;
    self.bg1.backgroundColor = [UIColor colorWithHexString:@"#EDF3F5"];
    
    self.bg2.layer.masksToBounds = YES;
    self.bg2.layer.cornerRadius = 4;
    self.bg2.backgroundColor = [UIColor colorWithHexString:@"#EDF3F5"];
    
    self.bg1lab1.textColor = [UIColor colorWithHexString:@"#333333"];
    self.bg1lab2.textColor = [UIColor colorWithHexString:@"#999999"];
    
    self.bg2lab1.textColor = [UIColor colorWithHexString:@"#333333"];
    self.bg2lab2.textColor = [UIColor colorWithHexString:@"#999999"];
    
    
    self.lab5.textColor = [UIColor colorWithHexString:@"#333333"];
    self.lab6.textColor = [UIColor colorWithHexString:@"#999999"];
    
    self.lab7.textColor = [UIColor colorWithHexString:@"#999999"];
    self.lab8.textColor = [UIColor colorWithHexString:@"#999999"];
    
    
}

-(void)setIsyhk:(BOOL)isyhk{
    _isyhk = isyhk;
    if(isyhk){
        self.bg1lab1.text = @"正面";
        self.bg1lab2.text = @"上传银行卡正面照片";
        
        self.bg2lab1.text = @"反面";
        self.bg2lab2.text = @"上传银行卡反面照片";
        
        self.fanmian.image = [UIImage imageNamed:@"58c72890987e132e92dda77ed3004437"];
        self.zhengmian.image = [UIImage imageNamed:@"cf79532ba5d9a4a396441c3915f694c5"];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
