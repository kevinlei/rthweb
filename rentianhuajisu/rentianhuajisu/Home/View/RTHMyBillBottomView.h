//
//  RTHMyBillBottomView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHMyBillBottomView : UIView
@property (weak, nonatomic) IBOutlet UIView *bg1;
@property (weak, nonatomic) IBOutlet UILabel *bg1lab1;

@property (weak, nonatomic) IBOutlet UILabel *bg1lab2;



@property (weak, nonatomic) IBOutlet UIView *bg2;

@property (weak, nonatomic) IBOutlet UILabel *bg2lab1;
@property (weak, nonatomic) IBOutlet UILabel *bg2lab2;


@end

NS_ASSUME_NONNULL_END
