//
//  HomeTableViewCell.h
//  HomeTableViewCell
//
//  Created by HDOceandeep on 2022/3/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XWDreamHomeTableViewCell : UITableViewCell
/// 图片
@property (nonatomic, strong) UIImageView *iconImageView;
/// 消息title
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *bottomLabel;
@property (nonatomic, strong) UILabel *rightLabel;

@property (nonatomic, strong) UILabel *studyLabel;

/// 时间
@property (nonatomic, strong) UIButton *guanzhuBtn;
@property (nonatomic, strong)UIImageView * ImageView1;

@property (nonatomic, strong)UIButton *closeButton;
@property (nonatomic,assign) BOOL isFans;//是否是粉丝列表
-(void)getFensiView;
-(void)getMyNewHomeCell;
@property (nonatomic, strong)NSDictionary *dict;
@end

NS_ASSUME_NONNULL_END
