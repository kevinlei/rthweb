//
//  RTHMyBillBottomView.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHMyBillBottomView.h"

@implementation RTHMyBillBottomView


-(void)awakeFromNib{
    [super awakeFromNib];
    self.bg1.layer.masksToBounds = YES;
    self.bg1.layer.cornerRadius = 30;
    self.bg1.layer.borderWidth = 1;
    self.bg1.backgroundColor = [UIColor whiteColor];
    self.bg1.layer.borderColor = [UIColor colorWithHexString:@"#24B6F6"].CGColor;
    
    
    self.bg1lab1.textColor = [UIColor colorWithHexString:@"#24B6F6"];
    self.bg1lab2.textColor = [UIColor colorWithHexString:@"#24B6F6"];
    
    self.bg2.layer.masksToBounds = YES;
    self.bg2.layer.cornerRadius = 30;
    self.bg2.layer.borderWidth = 1;
    self.bg2.layer.borderColor = [UIColor colorWithHexString:@"#24B6F6"].CGColor;
    
    self.bg2lab1.textColor = [UIColor whiteColor];
    self.bg2lab2.textColor = [UIColor whiteColor];
    self.bg2.backgroundColor =[UIColor colorWithHexString:@"#24B6F6"];;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
