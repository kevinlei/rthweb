//
//  RTHSelectOperationView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHSelectOperationView : UIView
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

NS_ASSUME_NONNULL_END
