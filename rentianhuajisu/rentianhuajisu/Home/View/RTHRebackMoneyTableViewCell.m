//
//  RTHRebackMoneyTableViewCell.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHRebackMoneyTableViewCell.h"

@implementation RTHRebackMoneyTableViewCell
+(instancetype)RTHRebackMoneyTableViewCellWithTableView:(UITableView *)tableview{
    static NSString *ID = @"RTHRebackMoneyTableViewCell";
    RTHRebackMoneyTableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RTHRebackMoneyTableViewCell" owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectedBackgroundView = [UIView new];
    }
    return cell;
}
-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
    self.moengy.text = [NSString stringWithFormat:@"%@",dict[@"je"]];
    self.time.text = [NSString stringWithFormat:@"%@",dict[@"createdate"]];
    
    self.reback.titleLabel.text = [NSString stringWithFormat:@"%@",dict[@"loanstatus"]];
//    [self.reback setTitle:self.reback.titleLabel.text forState:UIControlStateNormal];
    self.reback.titleLabel.font = [UIFont systemFontOfSize:12];
//    [self.reback setti]
    self.yueper.hidden = YES;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.reback.userInteractionEnabled = NO;
    self.bg.backgroundColor = [UIColor whiteColor];
    self.bg.layer.masksToBounds = YES;
    self.bg.layer.cornerRadius = 16;
    
    
    [self.reback setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.reback.layer.masksToBounds = YES;
    self.reback.layer.cornerRadius = 15;
    self.reback.backgroundColor = [UIColor colorWithHexString:@"#24B6F6"];
    self.reback.titleLabel.textColor = [UIColor whiteColor];
    self.moengy.textColor = [UIColor colorWithHexString:@"#24B6F6"];
    self.syb.textColor = [UIColor colorWithHexString:@"#24B6F6"];
    
    
    
    self.jie.textColor = [UIColor colorWithHexString:@"#333333"];
    self.yueper.textColor= [UIColor colorWithHexString:@"#666666"];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
