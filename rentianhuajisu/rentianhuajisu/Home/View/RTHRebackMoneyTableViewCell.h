//
//  RTHRebackMoneyTableViewCell.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHRebackMoneyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bg;

@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIButton *reback;
@property (weak, nonatomic) IBOutlet UILabel *syb;
@property (weak, nonatomic) IBOutlet UILabel *moengy;
@property (weak, nonatomic) IBOutlet UILabel *jie;

@property (weak, nonatomic) IBOutlet UILabel *yueper;

+(instancetype)RTHRebackMoneyTableViewCellWithTableView:(UITableView *)tableview;
@property(nonatomic,strong) NSDictionary *dict;

@end

NS_ASSUME_NONNULL_END
