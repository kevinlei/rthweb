//
//  RTHMoneyTableViewCell.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHMoneyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgV;

@property (weak, nonatomic) IBOutlet UILabel *dataLab;
@property (weak, nonatomic) IBOutlet UILabel *moenyLabel;
+(instancetype)RTHMoneyTableViewCelllWithTableView:(UITableView *)tableview;
@end

NS_ASSUME_NONNULL_END
