//
//  HomeTableViewCell.m
//  HomeTableViewCell
//
//  Created by HDOceandeep on 2022/3/22.
//

#import "XWDreamHomeTableViewCell.h"

@implementation XWDreamHomeTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)setupUI
{
    UIImageView * imageView = [UIImageView new];
    imageView.layer.masksToBounds  = YES;
    imageView.layer.cornerRadius=  10;
//    imageView.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(self.contentView);
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-10);
        make.left.mas_equalTo(self.contentView.mas_left).offset(10);
        make.width.mas_equalTo(90);
    }];
    self.iconImageView = imageView;
    
//    imageView.hidden = YES;
    
    UILabel * Label = [UILabel new];
    Label.text = @"教育";
    Label.textColor = [UIColor colorWithHexString:@"#333333"];
    Label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    [self.contentView addSubview:Label];
    [Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.left.mas_equalTo(imageView.mas_right).offset(10);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-70);
        make.height.mas_equalTo(21);
    }];
    self.titleLabel= Label;
    
    UILabel * bottomLabel = [UILabel new];
    bottomLabel.text = @"2023-1-13";
    bottomLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    bottomLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    [self.contentView addSubview:bottomLabel];
    [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-10);
//        make.left.mas_equalTo(imageView.mas_right).offset(10);
        make.left.mas_equalTo(imageView.mas_right).offset(10);
        make.height.mas_equalTo(21);
        make.width.mas_equalTo(80);
    }];
    self.bottomLabel= bottomLabel;
    
    UILabel * studyLabel = [UILabel new];
//    studyLabel.text = @"学习时长:0s";
    studyLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    studyLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    [self.contentView addSubview:studyLabel];
    [studyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-10);
        make.left.mas_equalTo(bottomLabel.mas_right).offset(10);
        make.height.mas_equalTo(21);
    }];
    self.studyLabel= studyLabel;

  
    
    
    UIImageView * ImageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back-1"]];
    self.ImageView1 = ImageView1;
    ImageView1.userInteractionEnabled = YES;
    [self.contentView addSubview:ImageView1];
    [ImageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).offset(-20);
        make.width.mas_equalTo(14);
        make.height.mas_equalTo(16);
        make.centerY.mas_equalTo(self.contentView.mas_centerY).offset(0);
    }];
    
    UILabel * rightLabel = [UILabel new];
    rightLabel.text = @"去阅读";
    rightLabel.textColor = [UIColor colorWithHexString:Navigation_And_StatusBar_Color];;
    rightLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [self.contentView addSubview:rightLabel];
    [rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
        make.height.mas_equalTo(15);
    }];
    self.rightLabel= rightLabel;
    
//    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [closeButton setImage:[UIImage imageNamed:@"me_xiaoclose"] forState:UIControlStateNormal];
//    [self.contentView addSubview:closeButton];
//    [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(imageView.mas_centerY);
//        make.right.mas_equalTo(self.contentView.mas_right).offset(-15);;
//        make.height.mas_equalTo(10);
//        make.width.mas_equalTo(10);
//    }];
//    closeButton.hidden = YES;
//    self.closeButton = closeButton;
//
//
//    UIButton *qianbaoButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [qianbaoButton setTitle:@"已关注" forState:UIControlStateNormal];
//    [qianbaoButton setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
//    qianbaoButton.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
//    qianbaoButton.layer.masksToBounds = YES;
//    qianbaoButton.layer.cornerRadius= 4;
//    qianbaoButton.layer.borderWidth = 0.5;
//    qianbaoButton.layer.borderColor = [UIColor colorWithHexString:@"666666"].CGColor;
//    [self.contentView addSubview:qianbaoButton];
//    [qianbaoButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_equalTo(imageView.mas_centerY);
//        make.right.mas_equalTo(self.contentView.mas_right).offset(-15);
//        make.height.mas_equalTo(30);
//        make.width.mas_equalTo(70);
//    }];
//    self.guanzhuBtn  = qianbaoButton;
    
    UIView *bottomLine = [UIView new];
    bottomLine.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    [self.contentView addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left).offset(25);
        make.right.equalTo(self.contentView.mas_right).offset(-25);
        make.height.mas_equalTo(0.5);
    }];
    
}
-(void)getMyNewHomeCell{
    self.ImageView1.hidden = NO;
//    self.rightLabel.hidden = YES;
//    self.bottomLabel.hidden = YES;
}
-(void)setDict:(NSDictionary *)dict{
    _dict = dict;
    NSString *type = dict[@"type"];
    if ([type isEqualToString:@"0"]) {
        self.rightLabel.textColor =  [UIColor colorWithHexString:@"#FF5858"];
    }else{
        self.rightLabel.textColor =  [UIColor colorWithHexString:@"#18C880"];
    }
    self.rightLabel.text = [NSString stringWithFormat:@"￥%@",dict[@"money"]];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@",dict[@"category"]];
    self.bottomLabel.text = [NSString stringWithFormat:@"%@",dict[@"desc"]];

    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
