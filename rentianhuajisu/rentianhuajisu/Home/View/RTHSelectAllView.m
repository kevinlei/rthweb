//
//  RTHSelectAllView.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHSelectAllView.h"

@implementation RTHSelectAllView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.payBtn.backgroundColor =[UIColor colorWithHexString:@"#24B6F6"];;
    self.payBtn.layer.masksToBounds = YES;
    self.payBtn.layer.cornerRadius = 25;
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.payBtn.titleLabel.textColor = [UIColor whiteColor] ;
    
    
    self.topLabel.textColor = [UIColor colorWithHexString:@"#666666"];;
    self.alterLabel.textColor = [UIColor colorWithHexString:@"#333333"];;
    
    self.priceLabel.textColor = [UIColor colorWithHexString:@"#24B6F6"];;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
