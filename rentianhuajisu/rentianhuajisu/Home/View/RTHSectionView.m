//
//  RTHSectionView.m
//  suixianghua
//
//  Created by 董学雷 on 2023/7/12.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHSectionView.h"

@implementation RTHSectionView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.lineView.backgroundColor = [UIColor colorWithHexString:@"#1FB4F6"];
    self.lineView.layer.masksToBounds  = YES;
    self.lineView.layer.cornerRadius = 2.5;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
