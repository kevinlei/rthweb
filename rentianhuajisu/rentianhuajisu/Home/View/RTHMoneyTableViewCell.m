//
//  RTHMoneyTableViewCell.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHMoneyTableViewCell.h"

@implementation RTHMoneyTableViewCell
+(instancetype)RTHMoneyTableViewCelllWithTableView:(UITableView *)tableview{
    static NSString *ID = @"RTHMoneyTableViewCell";
    RTHMoneyTableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RTHMoneyTableViewCell" owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectedBackgroundView = [UIView new];
    }
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.moenyLabel.textColor = [UIColor colorWithHexString:@"#666666"];
    self.dataLab.textColor = [UIColor colorWithHexString:@"#333333"];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.imgV.image = [UIImage imageNamed:@"CheckBox_select"];
    }else{
         self.imgV.image = [UIImage imageNamed:@"CheckBox_unselect"];
    }
    // Configure the view for the selected state
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}



@end
