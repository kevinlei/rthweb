//
//  RTHSelectOperationView.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/3.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "RTHSelectOperationView.h"

@implementation RTHSelectOperationView
-(void)awakeFromNib{
    [super awakeFromNib ];
    self.label.textColor = [UIColor colorWithHexString:@"#333333"];
    self.btn.titleLabel.textAlignment = NSTextAlignmentRight;
    [self.btn setTitleColor:[UIColor colorWithHexString:@"#26B6F6"] forState:UIControlStateNormal];
    self.btn.titleLabel.textColor  = [UIColor colorWithHexString:@"#26B6F6"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
