//
//  RTHVerifyIdView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/6/1.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHVerifyIdView : UIView
@property (weak, nonatomic) IBOutlet UIView *bg1;
@property (weak, nonatomic) IBOutlet UILabel *bg1lab1;

@property (weak, nonatomic) IBOutlet UILabel *bg1lab2;
@property (weak, nonatomic) IBOutlet UIView *bg2;
@property (weak, nonatomic) IBOutlet UILabel *bg2lab1;

@property (weak, nonatomic) IBOutlet UILabel *bg2lab2;

@property (weak, nonatomic) IBOutlet UILabel *lab5;

@property (weak, nonatomic) IBOutlet UILabel *lab6;

@property (weak, nonatomic) IBOutlet UILabel *lab7;

@property (weak, nonatomic) IBOutlet UILabel *lab8;


@property (weak, nonatomic) IBOutlet UIImageView *zhengmian;

@property (weak, nonatomic) IBOutlet UIImageView *fanmian;


@property (assign, nonatomic) BOOL isyhk;

@end

NS_ASSUME_NONNULL_END
