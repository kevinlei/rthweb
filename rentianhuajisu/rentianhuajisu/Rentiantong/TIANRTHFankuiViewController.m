//
//  RTHFankuiViewController.m
//  suixianghua
//
//  Created by 董学雷 on 2023/6/4.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import "TIANRTHFankuiViewController.h"
#import "HDFankuiView.h"
@interface TIANRTHFankuiViewController ()
@property(nonatomic,strong)  HDFankuiView *headerView;
@end

@implementation TIANRTHFankuiViewController

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//}
//-(void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"投诉反馈";
    self.view.backgroundColor = rgb(243, 246, 254);
//    UIView * topBG = [UIView new];
//    topBG.backgroundColor = self.view.backgroundColor;
//    [self.view addSubview:topBG];
//    topBG.userInteractionEnabled = YES;
//    [topBG mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top).offset(0);
//        make.left.mas_equalTo(self.view.mas_left).offset(0);
//        make.right.mas_equalTo(self.view.mas_right).offset(0);
//        make.height.mas_equalTo(80);
//    }];
//
//    UILabel * Label = [UILabel new];
//    Label.text = @"投诉反馈";
//    Label.textColor = [UIColor colorWithHexString:@"#000000"];
//    Label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
//    [topBG addSubview:Label];
//    [Label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(topBG.mas_top).offset(50);
//        make.centerX.mas_equalTo(topBG.mas_centerX).offset(0);
//        make.height.mas_equalTo(21);
//    }];
//    UIButton *qianbaoButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [qianbaoButton setImage:[UIImage imageNamed:@"返回 1"] forState:UIControlStateNormal];
//    [topBG addSubview:qianbaoButton];
//    [qianbaoButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(topBG.mas_left).offset(20);
//        make.centerY.mas_equalTo(Label.mas_centerY).offset(0);
//        make.height.mas_equalTo(20);
//        make.width.mas_equalTo(20);
//    }];
//    [qianbaoButton bk_whenTapped:^{
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
    
    
    NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"HDFankuiView"owner:self options:nil];
    HDFankuiView *headerView = [nibView objectAtIndex:0];
    headerView.backgroundColor = self.view.backgroundColor;
    headerView.TF.layer.cornerRadius = 8;
    headerView.TF.layer.masksToBounds = YES;
    headerView.TF.font = [UIFont systemFontOfSize:18];
    
    
    headerView.tf1.layer.cornerRadius = 8;
    headerView.tf1.layer.masksToBounds = YES;
    headerView.tf1.keyboardType  = UIKeyboardTypeNumberPad;
    headerView.tf1.font = [UIFont systemFontOfSize:18];
    
//    headerView.TF.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    headerView.TF.layer.borderWidth = 1;
    
    
    
//    headerView.submitBtn.backgroundColor = RGB(227, 105, 43);
    headerView.TF.placeholder = @"如您有任何反馈，请留言，我们一般会在24小时内与您电话联系，请保持手机畅通！";
    headerView.TF.placeholderFont = [UIFont systemFontOfSize:16];
    headerView.TF.placeholderColor = [UIColor grayColor];
    self.headerView = headerView;
    headerView.submitBtn.layer.cornerRadius = 3;
    headerView.submitBtn.layer.masksToBounds = YES;
    [headerView.submitBtn bk_whenTapped:^{
        [self submitAction];
    }];
    
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(Label.mas_bottom).offset(0);
        make.top.left.right.bottom.mas_equalTo(self.view);
    }];
    // Do any additional setup after loading the view.
}

-(void)submitAction{
    if (self.headerView.TF.text.length==0) {
        [SUIUtils makeShortToastAtCenter:@"请输入内容"];
        return;
    }
    if (self.headerView.tf1.text.length != 11) {
        [SUIUtils makeShortToastAtCenter:@"请输入正确手机号"];
        return;
    }
    [SVProgressHUD show];
    NSString *url = [HTTPInterface PublicForWard];
    NSDictionary *dict1 =@{@"parameter":@"saveIssuesInfo",@"content":self.headerView.TF.text};
    NSDictionary *param1 = @{@"json":[dict1 jsonStringEncoded],@"loginToken":USERTOKEN};
    [HttpRequestModel request:url withParamters:param1 success:^(id responseData) {
                    if ([responseData[@"code"] intValue]==1000) {
                        [SVProgressHUD dismiss];
                         [SUIUtils makeShortToastAtCenter:@"提交成功，我们将尽快与您反馈"];
                         [self.navigationController popViewControllerAnimated:YES];
                    }
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
                    [SUIUtils makeShortToastAtCenter:error.localizedDescription];
                    [SVProgressHUD dismiss];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
