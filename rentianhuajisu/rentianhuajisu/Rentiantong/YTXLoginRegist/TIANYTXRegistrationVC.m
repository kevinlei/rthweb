//
//  RegistrationVC.m
//
//  Created by dongxuelei on 16/4/20.
//  Copyright © 2016年 dongxuelei. All rights reserved.
//

#import "TIANYTXRegistrationVC.h"
//#import "UIViewExt.h"
#import "MBProgressHUD.h"
//#import "WXDataService.h"
//#import "SUIAppDelegate.h"
#import "JCCheckBox.h"
#import "RTHLoginCodeView.h"
#define KScreenWidth [UIScreen mainScreen].bounds.size.width
#define KScreenHeight [UIScreen mainScreen].bounds.size.height

@interface TIANYTXRegistrationVC ()<UITextFieldDelegate>
{
    UIButton *btn;
    NSTimer *timer;
    NSTimer *timer2;
    NSInteger timeCount;
    UIButton *registeBtn;
    BOOL isChecked;
    UIImageView * headericon ;
    BOOL isPsw;
    UIButton *swtichBtn;
    RTHLoginCodeView*sectionView1 ;

}
@end

@implementation TIANYTXRegistrationVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
 
    
    self.title = @"登录";
    isChecked = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = rgb(243, 246, 254);
    
    
    
        UIImageView * header = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"登录页-背景图"]];
        header.layer.masksToBounds = YES;
        header.layer.cornerRadius = 70/2;
        [self.view addSubview:header];
        header.userInteractionEnabled = YES;
        [header mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.view.mas_top).offset(0);
            make.right.mas_equalTo(self.view.mas_right).offset(0);
            make.left.mas_equalTo(self.view.mas_left).offset(0);
            make.height.mas_equalTo(275);
        }];
    headericon = header;
    
    UIButton *qianbaoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [qianbaoButton setImage:[UIImage imageNamed:@"navBack"] forState:UIControlStateNormal];
    [self.view addSubview:qianbaoButton];
    [qianbaoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(45);
        make.left.mas_equalTo(self.view.mas_left).offset(15);
        make.height.width.mas_equalTo(30);
    }];
    [qianbaoButton bk_whenTapped:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    

    
    
//    UILabel * Label = [UILabel new];
//    Label.text = @"仁天花";
//    Label.textAlignment = NSTextAlignmentLeft;
//    Label.textColor = [UIColor blackColor];
//    Label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
//    [self.view addSubview:Label];
//    [Label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.view.mas_top).offset(160);;
//        make.centerX.mas_equalTo(self.view.mas_centerX).offset(0);
//        make.height.mas_equalTo(21);
//        make.width.mas_equalTo(50);
//    }];
//
//
//    UILabel * Label1 = [UILabel new];
//    Label1.text = @"极速借贷，就上仁天花";
//    Label1.textAlignment = NSTextAlignmentLeft;
//    Label1.textColor = [UIColor darkGrayColor];
//    Label1.font = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
//    [self.view addSubview:Label1];
//    [Label1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(Label.mas_bottom).offset(10);;
//        make.left.mas_equalTo(Label.mas_left).offset(0);
//        make.height.mas_equalTo(21);
//        make.width.mas_equalTo(200);
//    }];
//
//
//
//        UIImageView * header = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mylogo"]];
//        header.layer.masksToBounds = YES;
//        header.layer.cornerRadius = 70/2;
//        [self.view addSubview:header];
//        header.userInteractionEnabled = YES;
//        [header mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(Label.mas_top).offset(-5);
//            make.right.mas_equalTo(Label.mas_left).offset(-10);
//            make.height.width.mas_equalTo(70);
//        }];
    [self creatRegistSubviews1];
    
    
    
    JCCheckBox *ovalCheckBox = [JCCheckBox checkBox]; // default size 24 by 24
//    self.ovalCheckBox =  ovalCheckBox;
    // SET STYLE
    ovalCheckBox.checkBoxStyle = JCCheckBoxStyleOval; // square is the default
    // CHANGE COLOR
    ovalCheckBox.innerColor = [UIColor colorWithHexString:@"#24B6F6"];
    ovalCheckBox.outerColor =  [UIColor colorWithHexString:@"#24B6F6"];
    // SET COMPLETION
    ovalCheckBox.checkedCompletion = ^{
        NSLog(@"Checked");
        self->isChecked = YES;
    };
    ovalCheckBox.uncheckedCompletion = ^{
        NSLog(@"Unchecked");
        self->isChecked = NO;
    };
    [self.view addSubview:ovalCheckBox];
    [ovalCheckBox mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(registeBtn.mas_left).offset(0);
        make.top.mas_equalTo(registeBtn.mas_bottom).offset(40);
        make.height.width.mas_equalTo(17);
    }];
    
    
    UILabel * Label3 = [UILabel new];
    Label3.text = @"我已阅读并仔细同意协议：";
    Label3.textAlignment = NSTextAlignmentCenter;
    Label3.textColor = [UIColor darkGrayColor];
    Label3.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
    [self.view addSubview:Label3];
    [Label3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(registeBtn.mas_bottom).offset(50);
        make.centerY.mas_equalTo(ovalCheckBox.mas_centerY).offset(0);
        make.left.mas_equalTo(ovalCheckBox.mas_right).offset(2);
        make.height.mas_equalTo(17);
    }];
    
    UILabel * Label31 = [UILabel new];
    Label31.text = @"未注册的手机号将自动创建账号";
    Label31.textAlignment = NSTextAlignmentLeft;
    Label31.textColor = [UIColor darkGrayColor];
    Label31.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
    [self.view addSubview:Label31];
    [Label31 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(registeBtn.mas_bottom).offset(50);
        make.top.mas_equalTo(Label3.mas_bottom).offset(0);
        make.left.mas_equalTo(ovalCheckBox.mas_right).offset(2);
        make.height.mas_equalTo(17);
    }];
    
    
    UILabel * Label5 = [UILabel new];
    Label5.text = @"《用户注册协议》";
    Label5.textAlignment = NSTextAlignmentCenter;
    Label5.textColor = [UIColor colorWithHexString:@"#24B6F6"];;
    Label5.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
    [self.view addSubview:Label5];
    [Label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(Label3.mas_centerY).offset(0);
        make.left.mas_equalTo(Label3.mas_right).offset(0);
        make.height.mas_equalTo(17);
    }];
    Label5.userInteractionEnabled = YES;
    [Label5 bk_whenTapped:^{
        SUIWKWebViewController *vc = [SUIWKWebViewController new];
        vc.url = @"http://www.mlwuyou.com:81/protocol/agreement.html?name=%E4%BB%81%E5%A4%A9%E8%8A%B1&corporate=%E9%87%8D%E5%BA%86%E4%B8%A4%E6%B1%9F%E6%96%B0%E5%8C%BA%E5%AE%9D%E5%8D%87%E5%B0%8F%E9%A2%9D%E8%B4%B7%E6%AC%BE%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8";
//        [self.navigationController pushViewController:vc animated:YES];
        [self presentViewController:vc animated:YES completion:^{
            
        }];
    }];
  
    
    UILabel * Label6 = [UILabel new];
    Label6.text = @"《隐私权政策服务》";
    Label6.textAlignment = NSTextAlignmentCenter;
    Label6.textColor = [UIColor colorWithHexString:@"#24B6F6"];;
    Label6.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
    [self.view addSubview:Label6];
    [Label6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(Label3.mas_centerY).offset(0);;
//        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-20);
        make.left.mas_equalTo(Label5.mas_right).offset(0);
        make.height.mas_equalTo(17);
    }];
    Label6.userInteractionEnabled = YES;
    [Label6 bk_whenTapped:^{
        SUIWKWebViewController *vc = [SUIWKWebViewController new];
        vc.url = @"http://www.mlwuyou.com:81/protocol/privacys.html?name=%E4%BB%81%E5%A4%A9%E8%8A%B1&corporate=%E9%87%8D%E5%BA%86%E4%B8%A4%E6%B1%9F%E6%96%B0%E5%8C%BA%E5%AE%9D%E5%8D%87%E5%B0%8F%E9%A2%9D%E8%B4%B7%E6%AC%BE%E8%82%A1%E4%BB%BD%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8";
//        [self.navigationController pushViewController:vc animated:YES];
        [self presentViewController:vc animated:YES completion:^{
            
        }];
    }];
    
    
    
    
   
    
}

-(void)swtichAction{
    isPsw = !isPsw;
    if(isPsw){
        btn.hidden = YES;
        sectionView1.codeLabel.placeholder = @"请输入密码";
        [swtichBtn setTitle:@"验证码登录" forState:UIControlStateNormal];
        [SUIUtils makeShortToastAtCenter:@"已切换到密码登录"];
    }else{
        [swtichBtn setTitle:@"密码登录" forState:UIControlStateNormal];
        btn.hidden = NO;
        sectionView1.codeLabel.placeholder = @"请输入验证码";
        [SUIUtils makeShortToastAtCenter:@"已切换到验证码登录"];
    }
}

-(void)creatRegistSubviews1{
    
    NSArray *arr = @[@"请输入手机号码",@"请输入验证码"];
//    for (int i=0; i<arr.count; i++) {
//
//        UITextField *textF = [[UITextField alloc]initWithFrame:CGRectMake(30, 300+i*65, KScreenWidth-60, 50)];
////        if (i==0) {
//            textF.keyboardType = UIKeyboardTypeNumberPad;
////        }
//        [self.view addSubview:textF];
//        textF.placeholder = arr[i];
//        textF.delegate = self;
//        textF.tag = 100+i;
//        textF.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textF.borderStyle = UITextBorderStyleRoundedRect;
////        textF.borderStyle = UITextBorderStyleLine;
////        textF.layer.borderWidth = 1;
////        textF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 50)];
//        textF.leftView = view;
//        textF.leftViewMode = UITextFieldViewModeAlways;
//
//        if (i == 1) {
//
//            btn = [[UIButton alloc]initWithFrame:CGRectMake(textF.right-130, textF.origin.y+7, 120, 36)];
//            btn.layer.cornerRadius = 18;
//            [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [btn addTarget:self action:@selector(btnAction)
//          forControlEvents:UIControlEventTouchUpInside];
//            btn.backgroundColor = [UIColor colorWithHexString:@"#24B6F6"];;
//            btn.titleLabel.font = [UIFont systemFontOfSize:15];
//            [self.view addSubview:btn];
//        }else if(i == 1){
//            textF.secureTextEntry = YES;
//        }
//    }
//
    
    RTHLoginCodeView*sectionView = [[[NSBundle mainBundle] loadNibNamed:@"RTHLoginCodeView" owner:nil options:nil] lastObject];
    sectionView.backgroundColor = [UIColor clearColor];
    sectionView.phoneLabel.tag = 100;
    sectionView.codeLabel.tag = 101;
    sectionView1 = sectionView;
    
    btn = sectionView.sendCodeBtn;
    btn.layer.masksToBounds= YES;
    btn.layer.cornerRadius = 13;
    [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction)
  forControlEvents:UIControlEventTouchUpInside];
    btn.backgroundColor = [UIColor colorWithHexString:@"#24B6F6"];;
    btn.titleLabel.font = [UIFont systemFontOfSize:10];
    [self.view addSubview:sectionView];
    [sectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headericon.mas_bottom).offset(100);
        make.left.mas_equalTo(self.view.mas_left).offset(35);
        make.right.mas_equalTo(self.view.mas_right).offset(-35);
        make.height.mas_equalTo(170);
    }];
    swtichBtn = sectionView.switchBtn;
//    swtichBtn.hidden = YES;
    sectionView.switchBtn.layer.masksToBounds= YES;
//    sectionView.switchBtn.layer.cornerRadius = 13;
    [sectionView.switchBtn setTitle:@"密码登录" forState:UIControlStateNormal];
    [sectionView.switchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sectionView.switchBtn addTarget:self action:@selector(swtichAction)
  forControlEvents:UIControlEventTouchUpInside];
    sectionView.switchBtn.backgroundColor = [UIColor colorWithHexString:@"#24B6F6"];;
    sectionView.switchBtn.titleLabel.font = [UIFont systemFontOfSize:14];

    
//    UITextField *textF = [self.view viewWithTag:100+arr.count-1];
    registeBtn = [[UIButton alloc]initWithFrame:CGRectZero];
    [registeBtn setTitle:@"登录" forState:UIControlStateNormal];
//    [registeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registeBtn.backgroundColor = [UIColor colorWithHexString:APP_tab_Color];
    
    [registeBtn setTitleColor:[UIColor whiteColor] forState:UIFontWeightBold];
    registeBtn.titleLabel.font =[UIFont systemFontOfSize:17 weight:UIFontWeightBold];
//    [registeBtn setBackgroundImage:[UIImage imageNamed:@"矩形"] forState:UIControlStateNormal];
    registeBtn.layer.masksToBounds = YES;
    registeBtn.layer.cornerRadius = 21;
    [registeBtn addTarget:self action:@selector(registeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registeBtn];
    [registeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(sectionView.mas_bottom).offset(10);
        make.left.mas_equalTo(self.view.mas_left).offset(35);
        make.right.mas_equalTo(self.view.mas_right).offset(-35);
        make.height.mas_equalTo(42);
    }];
    
}


-(void)creatRegistSubviews{
    
    NSArray *arr = @[@"请输入手机号码",@"请输入验证码"];
    for (int i=0; i<arr.count; i++) {
        
        UITextField *textF = [[UITextField alloc]initWithFrame:CGRectMake(30, 300+i*65, KScreenWidth-60, 50)];
//        if (i==0) {
            textF.keyboardType = UIKeyboardTypeNumberPad;
//        }
        [self.view addSubview:textF];
        textF.placeholder = arr[i];
        textF.delegate = self;
        textF.tag = 100+i;
        textF.clearButtonMode = UITextFieldViewModeWhileEditing;
        textF.borderStyle = UITextBorderStyleRoundedRect;
//        textF.borderStyle = UITextBorderStyleLine;
//        textF.layer.borderWidth = 1;
//        textF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 50)];
        textF.leftView = view;
        textF.leftViewMode = UITextFieldViewModeAlways;
        
        if (i == 1) {
            
            btn = [[UIButton alloc]initWithFrame:CGRectMake(textF.right-130, textF.origin.y+7, 120, 36)];
            btn.layer.cornerRadius = 18;
            [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnAction)
          forControlEvents:UIControlEventTouchUpInside];
            btn.backgroundColor = [UIColor colorWithHexString:@"#24B6F6"];;
            btn.titleLabel.font = [UIFont systemFontOfSize:15];
            [self.view addSubview:btn];
        }else if(i == 1){
            textF.secureTextEntry = YES;
        }
    }
    
    UITextField *textF = [self.view viewWithTag:100+arr.count-1];
    registeBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, textF.bottom+30, textF.width, 56)];
    [registeBtn setTitle:@"登录" forState:UIControlStateNormal];
//    [registeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    registeBtn.backgroundColor = [UIColor lightGrayColor];
    
    [registeBtn setTitleColor:[UIColor whiteColor] forState:UIFontWeightBold];
    registeBtn.titleLabel.font =[UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    [registeBtn setBackgroundImage:[UIImage imageNamed:@"矩形"] forState:UIControlStateNormal];
    registeBtn.layer.cornerRadius = 7;
    [registeBtn addTarget:self action:@selector(registeBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registeBtn];
    
    
}


-(void)pswLogin{
    //15933639058
    //12345678qaz#
    NSLog(@"注册Yes");
    UITextField *textF = [self.view viewWithTag:100];
    UITextField *textF2 = [self.view viewWithTag:101];
    NSLog(@"length:%lu",textF2.text.length);
    UITextField *textF3 = [self.view viewWithTag:102];
    UITextField *textF4 = [self.view viewWithTag:103];

    if(!isChecked){
        [SUIUtils makeShortToastAtCenter:@"请勾选用户协议哦"];
        return;
    }

    if (textF.text.length != 11) {
        [SUIUtils makeShortToastAtCenter:@"手机号码格式有误,请重新填写"];
        
    }else if(textF2.text.length == 0){
        [SUIUtils makeShortToastAtCenter:@"请输入密码"];
    }else{
        [SVProgressHUD show];
        
        NSString *url = [HTTPInterface PublicForWard];
        NSDictionary *dict =@{@"password":textF2.text,@"phone":textF.text,@"parameter":@"loginByPasswordFk"};
        NSDictionary *param = @{@"json":[dict jsonStringEncoded]};
        
        [HttpRequestModel request:url withParamters:param success:^(id responseData) {
            if ([responseData[@"code"] intValue]==1000) {
                NSDictionary *data = responseData[@"data"];
                [SUIUtils makeShortToastAtCenter:@"登录成功"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"id"]] forKey:@"USERID"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"phone"]] forKey:@"USERPHONE"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"password"]] forKey:@"USERPASSWORD"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"logintoken"]] forKey:@"USERTOKEN"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"type"]] forKey:@"USERCustomerType"];
                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"id"]] forKey:@"USERID"];
                [userDefault synchronize];
                
                [self.navigationController popViewControllerAnimated:YES];
                
                //                                SUIAppDelegate *app = [SUIAppDelegate shareAppDelegate];
                //                                [app changeLoginState:YES];
                
                
            }else{
                [SUIUtils makeShortToastAtCenter:responseData[@"data"][@"message"]];
            }
            [SVProgressHUD dismiss];
        } failure:^(NSError *error) {
            [SUIUtils makeShortToastAtCenter:error.localizedDescription];
            [SVProgressHUD dismiss];
        }];
    }
    
    
}

-(void)registeBtnAction{
    
    if(isPsw){
        [self pswLogin];
        return;
    }
    
//    if (registeBtn.backgroundColor != [UIColor lightGrayColor]) {
        NSLog(@"注册Yes");
        UITextField *textF = [self.view viewWithTag:100];
        UITextField *textF2 = [self.view viewWithTag:101];
        NSLog(@"length:%lu",textF2.text.length);
        UITextField *textF3 = [self.view viewWithTag:102];
        UITextField *textF4 = [self.view viewWithTag:103];
    
    if(!isChecked){
        [SUIUtils makeShortToastAtCenter:@"请勾选用户协议哦"];
        return;
    }
    
        if (textF.text.length != 11) {
            [SUIUtils makeShortToastAtCenter:@"手机号码格式有误,请重新填写"];
            
        }else if(textF2.text.length == 0){
            [SUIUtils makeShortToastAtCenter:@"请输入验证码"];
        }else{
            [SVProgressHUD show];
            
                        NSString *url = [HTTPInterface PublicForWard];
                        NSDictionary *dict =@{@"ideCode":textF2.text,@"phone":textF.text,@"parameter":@"loginFk"};
                        NSDictionary *param = @{@"json":[dict jsonStringEncoded]};
            
                        [HttpRequestModel request:url withParamters:param success:^(id responseData) {
                            if ([responseData[@"code"] intValue]==1000) {
                                NSDictionary *data = responseData[@"data"];
                                [SUIUtils makeShortToastAtCenter:@"登录成功"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"id"]] forKey:@"USERID"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"phone"]] forKey:@"USERPHONE"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"password"]] forKey:@"USERPASSWORD"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"logintoken"]] forKey:@"USERTOKEN"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"type"]] forKey:@"USERCustomerType"];
                                [userDefault setObject:[NSString stringWithFormat:@"%@",data[@"id"]] forKey:@"USERID"];
                                [userDefault synchronize];
                                
                                [self.navigationController popViewControllerAnimated:YES];
                                
//                                SUIAppDelegate *app = [SUIAppDelegate shareAppDelegate];
//                                [app changeLoginState:YES];
                                
                                
                            }else{
                                NSDictionary *data = responseData[@"data"];
                                [SUIUtils makeShortToastAtCenter:data[@"message"]];
                            }
                            [SVProgressHUD dismiss];
                        } failure:^(NSError *error) {
                                        [SUIUtils makeShortToastAtCenter:error.localizedDescription];
                                        [SVProgressHUD dismiss];
                        }];
            
        }
            
//    }else{
//        NSLog(@"注册NO");
//    }
}

//注册成功
-(void)registerSuccess{
    
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeText;
//    hud.labelText = @"注册成功!";
//    hud.detailsLabelText = @"正在为您登录...";
//    // 隐藏时候从父控件中移除
//    hud.removeFromSuperViewOnHide = YES;
//    // 1秒之后再消失
//    [hud hide:YES afterDelay:4];
    
    [self performSelector:@selector(delayMethod) withObject:nil afterDelay:1.0f];
}

-(void)delayMethod{
    NSDictionary *resultDiction = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"loginState", nil];
    [[NSUserDefaults standardUserDefaults] setObject:resultDiction forKey:@"ResultAuthData"];
    //保存数据，实现持久化存储
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)btnAction{
    __weak typeof(self) weakself = self;
    UITextField *textF = [self.view viewWithTag:100];
    if (textF.text.length != 11) {
        [SUIUtils makeShortToastAtCenter:@"手机号码格式有误,请重新填写"];
    }else{
        [SVProgressHUD show];
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(reduceTime:) userInfo:nil repeats:YES];
        timeCount = 60;
        
        
            NSString *url = [HTTPInterface PublicForWard];
            NSDictionary *dict =@{@"phone":textF.text,@"parameter":@"getFkMSCode"};
            NSDictionary *param = @{@"json":[dict jsonStringEncoded]};
        
            [HttpRequestModel request:url withParamters:param success:^(id responseData) {
                            if ([responseData[@"code"] intValue]==1000) {
                                UITextField *textF3 = [self.view viewWithTag:101];
//                                registeBtn.backgroundColor = [UIColor colorWithRed:0.002 green:0.774 blue:0.003 alpha:1.000];
                                [SUIUtils makeShortToastAtCenter:@"获取验证码成功！"];
                                [SVProgressHUD dismiss];
                            }
            } failure:^(NSError *error) {
                            [SUIUtils makeShortToastAtCenter:error.localizedDescription];
                            [SVProgressHUD dismiss];
            }];
    
    }
    
}


- (void)reduceTime:(NSTimer *)codeTimer {
    timeCount--;
    if (timeCount == 0) {
        [btn setTitle:@"重新获取验证码" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        UIButton *info = codeTimer.userInfo;
        info.enabled = YES;
        btn.userInteractionEnabled = YES;
        [timer invalidate];
    } else {
  
        NSString *str = [NSString stringWithFormat:@"%lu秒后重新获取", (long)timeCount];
        btn.titleLabel.text = str;
        [btn setTitle:str forState:UIControlStateNormal];
        btn.userInteractionEnabled = NO;
        
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    timer2 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(textFTime1) userInfo:nil repeats:YES];
    
}

-(void)textFTime1{
    
    UITextField *textF = [self.view viewWithTag:100];
    UITextField *textF2 = [self.view viewWithTag:101];
    UITextField *textF3 = [self.view viewWithTag:102];
    
    if (textF.text.length > 0 && textF2.text.length > 0 && textF3.text.length > 0) {
        
//        registeBtn.backgroundColor = [UIColor colorWithRed:0.002 green:0.774 blue:0.003 alpha:1.000];
        [timer2 invalidate];
        
    }else{
//        registeBtn.backgroundColor = [UIColor lightGrayColor];
    }
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if (self.isViewLoaded && !self.view.window) {
        
        self.view = nil;
    }
}



@end
