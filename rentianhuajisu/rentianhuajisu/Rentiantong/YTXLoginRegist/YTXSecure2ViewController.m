//
//  Secure2ViewController.m
//
//  Created by dongxuelei on 16/4/19.
//  Copyright © 2016年 dongxuelei. All rights reserved.
//

#import "YTXSecure2ViewController.h"



#define KScreenWidth [UIScreen mainScreen].bounds.size.width
#define KScreenHeight [UIScreen mainScreen].bounds.size.height

@interface YTXSecure2ViewController ()<UITextFieldDelegate>
{
    UIButton *btn;
    NSTimer *timer2;
    UIButton *confineBtn;
}
@end

@implementation YTXSecure2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self creatSubviews];
}

-(void)creatSubviews{
    
//    NSArray *arr = @[@"验证码",@"输入新密码",@"确认密码"];
     NSArray *arr = @[@"输入新密码",@"确认密码"];
    for (int i=0; i<arr.count; i++) {
        
        UITextField *textF = [[UITextField alloc]initWithFrame:CGRectMake(30, 100+i*70, KScreenWidth-60, 50)];
        [self.view addSubview:textF];
        textF.placeholder = arr[i];
        textF.delegate = self;
        textF.tag = 100+i;
        textF.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textF.secureTextEntry /= YES;
//        textF.borderStyle = UITextBorderStyleRoundedRect;
        textF.borderStyle = UITextBorderStyleNone;
        textF.layer.borderWidth = 1;
        textF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 50)];
        textF.leftView = view;
        textF.leftViewMode = UITextFieldViewModeAlways;
    }
    
    UITextField *textF = [self.view viewWithTag:100+arr.count-1];
    confineBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, textF.bottom+30, textF.width, 44)];
    [confineBtn setTitle:@"确认修改" forState:UIControlStateNormal];
    [confineBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confineBtn.backgroundColor = [UIColor lightGrayColor];
    confineBtn.layer.cornerRadius = 7;
    [confineBtn addTarget:self action:@selector(confineAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confineBtn];
    
}

-(void)confineAction{
    
    
    if (confineBtn.backgroundColor != [UIColor lightGrayColor]) {
        
        UITextField *textF1 = [self.view viewWithTag:100];
        UITextField *textF2 = [self.view viewWithTag:101];
//        UITextField *textF3 = [self.view viewWithTag:102];
#pragma mark - 密码
        if ([textF2.text isEqualToString:textF1.text]) {
            [SVProgressHUD show];
//            NSString *url = [YTXAPIService userSetNewPwd];
//            NSDictionary *param = @{@"mobile":self.phone,@"password":textF2.text,@"code":self.token};
//            [YTXNetWorking postWithUrl:url param:param method:@"POST" success:^(NSDictionary * _Nonnull response) {
//                [SVProgressHUD dismiss];
//                if ([response[@"code"] intValue]==0) {
//                    NSLog(@"loginresponse:%@",response);
//                    [SUIUtils makeShortToastAtCenter:@"修改成功"];
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }else{
//                    [SUIUtils makeShortToastAtCenter:response[@"msg"]];
//                }
//            } failure:^(NSError * _Nonnull error) {
//                [SUIUtils makeShortToastAtCenter:error.localizedDescription];
//                [SVProgressHUD dismiss];
//            }];
            
        }else{
            [SUIUtils makeShortToastAtCenter:@"两次密码输入不一致"];
        }
        
        
        NSLog(@"确认修改Yes");
    }else{
         NSLog(@"确认修改NO");
    }
}

-(void)delaySecureAction{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"6666");
    
    timer2 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(textFTime1) userInfo:nil repeats:YES];
   
}

-(void)textFTime1{
    
    UITextField *textF = [self.view viewWithTag:100];
    UITextField *textF2 = [self.view viewWithTag:101];
    
    if (textF.text.length > 0 && textF2.text.length > 0) {
        
        confineBtn.backgroundColor = [UIColor colorWithRed:0.002 green:0.774 blue:0.003 alpha:1.000];
        [timer2 invalidate];
        
    }else{
        confineBtn.backgroundColor = [UIColor lightGrayColor];
    }

}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if (self.isViewLoaded && !self.view.window) {
        
        self.view = nil;
    }
}

@end
