//
//  RTHLoginCodeView.h
//  suixianghua
//
//  Created by 董学雷 on 2023/7/15.
//  Copyright © 2023 suixianghua. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTHLoginCodeView : UIView
@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *codeLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *switchBtn;

@end

NS_ASSUME_NONNULL_END
