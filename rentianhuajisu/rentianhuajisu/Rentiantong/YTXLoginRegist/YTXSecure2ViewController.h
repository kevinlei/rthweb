//
//  Secure2ViewController.h
//
//  Created by dongxuelei on 16/4/19.
//  Copyright © 2016年 dongxuelei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YTXSecure2ViewController : UIViewController

@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *token;

@end
