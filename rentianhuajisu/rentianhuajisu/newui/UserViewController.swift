//
//  UserViewController.swift
//  leyihua
//
//  Created by xl12 on 2023/10/12.
//

import UIKit

class UserViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBOutlet var userLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "信息"
        var phoneStr: String = UserDefaults.standard.string(forKey: String.phone) ?? ""
 
        userLabel.text = phoneStr
    }


    @IBAction func delAction(_ sender: Any) {
        var phoneStr: String = UserDefaults.standard.string(forKey: String.phone) ?? ""

        if phoneStr == "15013098988" {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "测试账户不能删除！", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default) { UIAlertAction in
              
            }
           
            alertVC.addAction(defaultAction)
             self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        let alertVC:UIAlertController = UIAlertController (title: "提示", message: "确定删除该用户么?", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "确定", style: .default) { UIAlertAction in
            let defaults = UserDefaults.standard
            defaults.set("", forKey: String.phone)
            defaults.set("", forKey: String.loginSuccess)
            
            defaults.set("", forKey: String.phone + phoneStr)
            defaults.set("", forKey: String.password + phoneStr)
            
            defaults.synchronize()
            
            let window:UIWindow = (UIApplication.shared.delegate?.window)!!
            let vc = LoginViewController()
             window.rootViewController = vc
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default) { UIAlertAction in
           
        }
        alertVC.addAction(defaultAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
        
    }
    
    @IBAction func exitAction(_ sender: Any) {
   
        let defaults = UserDefaults.standard
        defaults.set("", forKey: String.phone)
        defaults.set("", forKey: String.loginSuccess)
        defaults.synchronize()

        let window:UIWindow = (UIApplication.shared.delegate?.window)!!
        let vc = LoginViewController()
         window.rootViewController = vc
        
    }
    
}
