//
//  BaseViewController.swift
//  leyihua
//
//  Created by xl12 on 2023/10/12.
//



import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if (navigationController?.viewControllers.count ?? 0 > 1) {
            navigationItem.hidesBackButton = true
            
            let btn = UIButton()
            btn.addTarget(self, action: #selector(backAction1), for: .touchUpInside)
            btn.setImage(UIImage(named: "back1"), for: .normal)
            let left = UIBarButtonItem(customView: btn)
            left.imageInsets = UIEdgeInsets(top: 0, left: -5.5, bottom: 0, right: 0)
            navigationItem.leftBarButtonItem = left
        }

        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [.font : UIFont.boldSystemFont(ofSize: 18), .foregroundColor : UIColor.black]
    
    }
    

    @objc func backAction1() {
        navigationController?.popViewController(animated: true)
    }

}
