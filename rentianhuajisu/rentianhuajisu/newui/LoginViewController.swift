//
//  LoginViewController.swift
//  leyihua
//
//  Created by LXie on 11/10/2023.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func loginAction(_ sender: Any) {

        let phone =  phoneField.text ?? ""
        let password = passwordField.text ?? ""
        if (phone.count == 0) {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "请输入手机号", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        if (password.count == 0) {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "请输入密码", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
    
        var phoneStr: String = UserDefaults.standard.string(forKey: String.phone + phone) ?? ""
        var passwordStr: String = UserDefaults.standard.string(forKey: String.password + phone) ?? ""
        
        
        if phoneStr.count == 0 {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "该账户未注册，请到注册页面注册！", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default) { UIAlertAction in
                let resignVC = RegistViewController()
                self.navigationController?.pushViewController(resignVC, animated: true)
            }
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
        }else{
            if phone == phoneStr && password == passwordStr {
                loginSuccess()
            }else if phone == phoneStr && password != passwordStr{
                let alertVC:UIAlertController = UIAlertController (title: "提示", message: "密码错误", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
                alertVC.addAction(defaultAction)
                self.present(alertVC, animated: true, completion: nil)
            }else{
                let alertVC:UIAlertController = UIAlertController (title: "提示", message: "该账户未注册，请到注册页面注册！", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
                alertVC.addAction(defaultAction)
                self.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
    func loginSuccess() {
        UserDefaults.standard.set("success", forKey: String.loginSuccess)
        UserDefaults.standard.synchronize()
        
        let defaults = UserDefaults.standard
        defaults.set(phoneField.text, forKey: String.phone + phoneField.text!)
        defaults.set(passwordField.text, forKey: String.password + phoneField.text!)
        
        defaults.set(phoneField.text, forKey: String.phone)

        defaults.synchronize()
        
        let window:UIWindow = (UIApplication.shared.delegate?.window)!!
        let vc = ViewController()
         window.rootViewController = vc
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let resignVC = RegistViewController()
        self.navigationController?.pushViewController(resignVC, animated: true)
    }
    
}
