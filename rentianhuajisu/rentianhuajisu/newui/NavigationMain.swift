//
//  NavigationMain.swift
//  leyihua
//
//  Created by LXie on 11/10/2023.
//

import UIKit

class NavigationMain: UINavigationController , UINavigationBarDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -60), for: .default)
        
//         customBarItem()
        
 
//        if (@available(iOS 15.0, *)) {
//        
//        if #available(iOS 13.0, *) {
//            let appearance = UINavigationBarAppearance()
//        } else {
//            // Fallback on earlier versions
//        }
//        appearance.configureWithTransparentBackground()
//        
//        let attrs: [NSAttributedString.Key: Any] = [
//            .foregroundColor: UIColor.black,
//            .font: UIFont.systemFont(ofSize: 18, weight: .black)
//        ]
// 
//        appearance.titleTextAttributes = attrs;
//        
//        appearance.backgroundImage = UIImage()
//        appearance.shadowColor = UIColor.clear
//        appearance.shadowImage = UIImage()
//        if #available(iOS 13.0, *) {
//            navigationBar.scrollEdgeAppearance = appearance
//        } else {
//            // Fallback on earlier versions
//        }
//        if #available(iOS 13.0, *) {
//            navigationBar.standardAppearance = appearance
//        } else {
//            // Fallback on earlier versions
//        }
        
//            UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
//            [appearance configureWithOpaqueBackground];
//            
//            NSDictionary *dict =  @{NSFontAttributeName:[DYSkinThemeManager fontWithType:DYSkinFontType_FONT_T1],
//                                    NSForegroundColorAttributeName:[DYSkinThemeManager colorWithType:DYSkinColorType_COLOR_T1]};
//            
//            appearance.titleTextAttributes = dict;
//            appearance.backgroundImage = [[UIImage alloc] init];
//            appearance.shadowImage = [[UIImage alloc] init];
//            appearance.shadowColor = [UIColor clearColor];
//            appearance.backgroundEffect = nil;
//     
//            self.navigationBar.scrollEdgeAppearance = appearance;
//            self.navigationBar.standardAppearance = appearance;

            
//        }else{
//            [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
//            [self.navigationBar setShadowImage:[[UIImage alloc] init]];
//            
//            self.navigationBar.tintColor=[UIColor darkGrayColor];
//            [self.navigationBar setBarTintColor:[UIColor whiteColor]];
//            
//            // * 标题文字/颜色 *
//            NSDictionary *dict =  @{NSFontAttributeName:[DYSkinThemeManager fontWithType:DYSkinFontType_FONT_T1],
//                                    NSForegroundColorAttributeName:[DYSkinThemeManager colorWithType:DYSkinColorType_COLOR_T1]};
//            [self.navigationBar setTitleTextAttributes:dict];
//        }
        
        customBarItem()
        
    }
    
    func navigationBar(_ navigationBar: UINavigationBar, shouldPush item: UINavigationItem) -> Bool {
        let dict = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15),
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        
        let appearance = UIBarButtonItem.appearance()
        appearance.setTitleTextAttributes(dict, for: .normal)
         return true
    }
    
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
    
     func customBarItem() {
        
//        let backBtns = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: nil, action: nil)
//        navigationItem.backBarButtonItem = backBtns
//        navigationController?.navigationBar.tintColor = .white
      
    }

}
